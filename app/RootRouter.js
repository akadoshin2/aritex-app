'use strict';

import React, { Component } from 'react';
import {
    Alert,
    BackAndroid 
} from 'react-native';
import { Router } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';

import configureStore from './Store';
import Wrapper from './Wrapper';
import Scenes from './Scenes';
import Languages from './Languages'


//Redux store setup
const RouterWithRedux = connect()(Router);

export default class RootRouter extends Component {
    constructor(props) {
        super(props);

        this.state = { 
            store: configureStore(() => this.setState({ isStoreLoading: false })),
            isStoreLoading: true,            
             
        };
    }

    render() {
        const { isStoreLoading, store } = this.state;
        // Exit confirm
        const onExitApp = () => {
            Alert.alert(
                Languages.Exit,
                Languages.ExitConfirm,
                [
                    { text: Languages.CANCEL, onPress: () => undefined },
                    { text: Languages.YES, onPress: () => BackAndroid.exitApp() },
                ]
            );
            return true;
        };  

        if (isStoreLoading) return null;

        //Set language at this phase is fine because RootRouter only render once.
        Languages.setLanguage(store.getState().Language);        

        return (
            <Provider store={store}>                           
                <Wrapper>
                    <RouterWithRedux
                        hideNavBar
                        onExitApp={onExitApp}
                        panHandlers={null}
                        scenes={Scenes}                     
                    />                                  
                </Wrapper>           
            </Provider>
        );
    }
}
