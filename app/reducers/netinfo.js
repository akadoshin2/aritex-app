import { NET_INFO_CHANGED } from 'react-native-redux-listener';

export default function netinfo(state = {}, action = {}) {
    switch (action.type) {
        case NET_INFO_CHANGED:
            // alert(`isConnected? ${action.isConnected}`);
            return {
                ...state,
                appIsOnline: (action.isConnected === true),
            };
        default:
            return state;
    }
}
