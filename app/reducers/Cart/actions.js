export const ASSIGN_AUTH = 'ASSIGN_AUTH_TOKEN';
export const UPDATE_CART = 'UPDATE_CART';
export const ADD_CART_ITEM = 'ADD_CART_ITEM';
export const DELETE_CART_ITEM = 'DELETE_CART_ITEM';
export const EMPTY_CART = 'EMPTY_CART';
export const SELECT_CLIENT = 'SELECT_CLIENT';
export const UNSELECT_CLIENT = 'UNSELECT_CLIENT';
export const ADD_OBSERVATION = 'ADD_OBSERVATION';

export function assignAuthToken(auth_token) {
    return {
        type: ASSIGN_AUTH,
        auth_token
    };
}

export function updateCart(cart) {
    return {
        type: UPDATE_CART,
        cart
    };
}

export function addCartItem(product, variations, amount, time) {
    return {
        type: ADD_CART_ITEM,
        product,
        variations,
        amount,
        time
    };
}

export function deleteCartItem(product, variations, quantity) {
    return {
        type: DELETE_CART_ITEM,
        product,
        variations,
        quantity
    };
}

export function emptyCart() {
    return {
        type: EMPTY_CART
    };
}

export function selectClient(client, clientId) {
    return {
        type: SELECT_CLIENT,
        client,
        clientId
    };
}

export function unselectClient() {
    return {
        type: UNSELECT_CLIENT
    };
}

export function addObservation(observation) {
    return {
        type: ADD_OBSERVATION,
        observation
    };
}
