import {
    ASSIGN_AUTH,
    UPDATE_CART,
    ADD_CART_ITEM,
    DELETE_CART_ITEM,
    EMPTY_CART,
    SELECT_CLIENT,
    UNSELECT_CLIENT,
    ADD_OBSERVATION,
} from './actions';

const INITIAL_STATE = { 
    auth_token: '',
    cartItems: [], 
    totalProducts: 0, 
    totalPrice: 0, 
    totalPriceIVA: 0, 
    client: '', 
    clientId: -1, 
    observation: ''
};

const compareCartItem = (cartItem, action) => {
        return cartItem.product.reference_id === action.product.reference_id;
};

const updateTotalProducts = (state, action) => {
    if (state.cartItems.length > 0) { // Si el carrito no esta vacio
        const index = state.cartItems.findIndex((cartItem) => compareCartItem(cartItem, action));
        if (index !== -1) return (state.totalProducts - state.cartItems[index].quantity) + action.amount; // Si el producto agregado ya existe
        return state.totalProducts + action.amount; // Si el producto agregado no existe en el carrito
    } 
    return action.amount; // Si el carrito está vacio
};

const updateTotalPrice = (state, action) => {
    if (state.cartItems.length > 0) { // Si el carrito no esta vacio
        const index = state.cartItems.findIndex((cartItem) => compareCartItem(cartItem, action));
        if (index !== -1) return (state.totalPrice - (state.cartItems[index].quantity * state.cartItems[index].product.price)) + (action.amount * action.product.price); // Si el producto agregado ya existe
        return state.totalPrice + (action.amount * action.product.price); // Si el producto agregado no existe en el carrito
    } 
    return action.amount * action.product.price; // Si el carrito está vacio
};

const cartItem = (state = { product: undefined, quantity: undefined, variations: undefined, time: 0 }, action) => {
    switch (action.type) {
        case ADD_CART_ITEM:
            console.log('CARDITEM', state);
            return state.product === undefined ? // Si es un nuevo producto
                Object.assign({}, { 
                    product: action.product, 
                    variations: action.variations, 
                    quantity: action.amount,
                    time: action.time
                }) : // Retorna el nuevo producto
                !compareCartItem(state, action) ? state : // Sino, revisa si son distintos y retorna state
                    Object.assign({}, state, { // Si son iguales borra lo anterior y agrega lo nuevo
                        variations: action.variations,
                        quantity: action.amount,
                        time: action.time
                    });
        default:
            return state;
    }
};

export default function Cart(state = INITIAL_STATE, action) {
    switch (action.type) {
        case ASSIGN_AUTH:
            return Object.assign({}, state, {
                auth_token: action.auth_token
            });
        case UPDATE_CART:
            return Object.assign({}, state, {
                ...action.cart
            });
        case ADD_CART_ITEM: {
            const isExisted = state.cartItems.some((cartItem) => compareCartItem(cartItem, action));
            const totalPrice = updateTotalPrice(state, action);
            return Object.assign({}, state, isExisted ?
                    { cartItems: state.cartItems.map(item => cartItem(item, action)) } : // Si existe actualiza
                    { cartItems: [...state.cartItems, cartItem(undefined, action)] }, // Sino agrega al carrito
                {
                    totalProducts: updateTotalProducts(state, action),
                    totalPrice,
                    totalPriceIVA: totalPrice * 1.19
                }
            );
        }
        case DELETE_CART_ITEM: {
            const index1 = state.cartItems.findIndex((cartItem) => compareCartItem(cartItem, action)); // check if existed
            const totalPrice = state.totalPrice - (action.quantity * action.product.price);
            return index1 === -1 ? state : //This should not happen, but catch anyway
                Object.assign({}, state, {
                    cartItems: state.cartItems.filter((cartItem) => !compareCartItem(cartItem, action)),
                    totalProducts: state.totalProducts - action.quantity,
                    totalPrice,
                    totalPriceIVA: totalPrice * 1.19
                });
        }
        case EMPTY_CART:
            return Object.assign({}, state, {
                cartItems: [],
                totalProducts: 0,
                totalPrice: 0,
                totalPriceIVA: 0,
                client: '',
                clientId: -1,
                observation: ''
            });
        case SELECT_CLIENT:
            return Object.assign({}, state, {
                client: action.client,
                clientId: action.clientId
            });
        case UNSELECT_CLIENT:
            return Object.assign({}, state, {
                client: '',
                clientId: -1
            });
        case ADD_OBSERVATION:
            return Object.assign({}, state, {
                observation: action.observation
            });
        default:
            return state;
    }
}
