import { combineReducers } from 'redux';
//Redux form
import { reducer as formReducer } from 'redux-form';

import routes from './routes';
import netinfo from './netinfo';
import Modal from './Modal/index';
import Language from './Language/index';
import Updater from './Updater/index';
import ActionQueue from './ActionQueue/index';
import User from './User/index';
import Store from './Store/index';
import Carousels from './Carousels/index';
import Cart from './Cart/index';

export default combineReducers({
    routes,
    netinfo,
    Modal,
    Language,
    Updater,
    ActionQueue,
    User,
    Store,
    Carousels,
    Cart,
    form: formReducer
});
