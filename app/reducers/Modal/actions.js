import AppEventEmitter from './../../utils/AppEventEmitter';
import Constants from './../../Constants';

export const SHOW_LOADING_SCREEN = 'SHOW_LOADING_SCREEN';
export const HIDE_LOADING_SCREEN = 'HIDE_LOADING_SCREEN';
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export function showLoadingScreen() {
    function action() {
        return {
            type: SHOW_LOADING_SCREEN
        };              
    }

    return (dispatch) => {
        dispatch(action());  
        AppEventEmitter.emit(Constants.EmitCode.OpenModal);           
    };
}

export function hideLoadingScreen() {
    function action() {
        return {
            type: HIDE_LOADING_SCREEN
        };              
    }

    return (dispatch) => {
        dispatch(action());   
        AppEventEmitter.emit(Constants.EmitCode.CloseModal);             
    };
}

export function openModal(modal) {
    function action() {
        return {
            type: OPEN_MODAL,
            modal
        };              
    }

    return (dispatch) => {
        dispatch(action());
        AppEventEmitter.emit(Constants.EmitCode.OpenModal);
    };
}

export function closeModal() {
    function action() {
        return {
            type: CLOSE_MODAL
        };              
    }

    return (dispatch) => {
        dispatch(action());   
        AppEventEmitter.emit(Constants.EmitCode.CloseModal);
    };
}
