import {
    SHOW_LOADING_SCREEN,
    HIDE_LOADING_SCREEN,
    OPEN_MODAL,
    CLOSE_MODAL
} from './actions';

const INITIAL_STATE = {
    modal: ''
};

export default function Modal(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SHOW_LOADING_SCREEN:
            return Object.assign({}, state, {
                modal: 'loading-screen',
            });

        case HIDE_LOADING_SCREEN:
            return INITIAL_STATE;

        case OPEN_MODAL:
            return Object.assign({}, state, {
                modal: action.modal,
            });

        case CLOSE_MODAL:
            return INITIAL_STATE;
            
        default:
            return state;
    }
}
