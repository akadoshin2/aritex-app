import TimerMixin from 'react-timer-mixin';

import { clearUpdater, updateData } from '../Updater/actions';
import { submitOrder } from '../User/actions';
import { showLoadingScreen } from '../Modal/actions';

export const ADD_TO_ACTION_QUEUE = 'ADD_TO_ACTION_QUEUE';
export const START_DISPATCHING_QUEUED_ACTIONS = 'START_DISPATCHING_QUEUED_ACTIONS';
export const REMOVE_FROM_ACTION_QUEUE = 'REMOVE_FROM_ACTION_QUEUE';
export const FINISH_DISPATCHING_QUEUED_ACTIONS = 'FINISH_DISPATCHING_QUEUED_ACTIONS';
export const CLEAR_ACTION_QUEUE = 'CLEAR_ACTION_QUEUE';

export function addToActionQueue(actionName, params) {
    function action(id) {
        return {
            type: ADD_TO_ACTION_QUEUE, 
            payload: {
                id,
                action: actionName,
                params
            }
        };              
    }

    return (dispatch, getState) => {
        const { ActionQueue } = getState();

        dispatch(action(ActionQueue.actionQueue.length));        
    };
}

export function startDispatchingQueuedActions() {
    function action() {
        return {
            type: START_DISPATCHING_QUEUED_ACTIONS
        };              
    }

    return (dispatch) => {
        dispatch(showLoadingScreen());
        dispatch(action());
    };
}

export function removeFromActionQueue(actionID) {
    function action() {
        return {
            type: REMOVE_FROM_ACTION_QUEUE, 
            actionID
        };              
    }

    return (dispatch, getState) => {
        const { ActionQueue } = getState();
        // sconsole.log(ActionQueue.actionQueue.length);
        dispatch(action());
        // console.log(ActionQueue.actionQueue.length);
        if ((ActionQueue.actionQueue.length - 1) === 0) {
            dispatch(finishDispatchingQueuedActions());   
        }
    };
}

export function finishDispatchingQueuedActions() {
    function action() {
        return {
            type: FINISH_DISPATCHING_QUEUED_ACTIONS
        };              
    }

    return (dispatch) => {
        // dispatch(hideLoadingScreen());        
        dispatch(action());
        dispatch(clearUpdater());
        TimerMixin.setTimeout(
            () => { dispatch(updateData()); }, 
            500
        );        
    };
}

export function clearActionQueue() {
    return {
        type: CLEAR_ACTION_QUEUE
    };
}

export function dispatchQueuedActions() {
    return (dispatch, getState) => {
        const { netinfo, ActionQueue } = getState();
        const { appIsOnline } = netinfo;
        // const { actionQueue } = ActionQueue;
        const actionQueue = [...ActionQueue.actionQueue];

        if (appIsOnline && actionQueue.length > 0) {
            dispatch(startDispatchingQueuedActions());
            actionQueue.forEach((actionQueued) => {
                const { id, action, params } = actionQueued;
                const callback = () => {
                    dispatch(removeFromActionQueue(id));
                };

                switch (action) {
                    case 'submitOrder':
                        dispatch(submitOrder(params, callback));
                        break;

                    default:
                        break;
                }
            });
        }
    };
}
