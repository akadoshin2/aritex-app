import {
    ADD_TO_ACTION_QUEUE,
    START_DISPATCHING_QUEUED_ACTIONS,
    REMOVE_FROM_ACTION_QUEUE,
    FINISH_DISPATCHING_QUEUED_ACTIONS,
    CLEAR_ACTION_QUEUE
} from './actions';

const INITIAL_STATE = {
    actionQueue: []
};

export default function ActionQueue(state = INITIAL_STATE, action) {
    switch (action.type) {
        case ADD_TO_ACTION_QUEUE:
            return Object.assign({}, state, {
                actionQueue: [...state.actionQueue, action.payload]
            });

        case START_DISPATCHING_QUEUED_ACTIONS:
            return state;

        case REMOVE_FROM_ACTION_QUEUE:
            return Object.assign({}, state, {
                actionQueue: state.actionQueue.filter((item) => {
                    return item.id !== action.actionID;
                })
            });

        case FINISH_DISPATCHING_QUEUED_ACTIONS:
            return state;

        case CLEAR_ACTION_QUEUE:
            return INITIAL_STATE;

        default:
            return state;
    }
}
