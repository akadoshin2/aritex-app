export const UPDATE_STORE_DATA = 'UPDATE_STORE_DATA';
export const SELECT_CATEGORY = 'SELECT_CATEGORY';
export const SELECT_PRODUCT = 'SELECT_PRODUCT';

export const TOGGLE_PRODUCT_VIEW_MODE = 'TOGGLE_PRODUCT_VIEW_MODE';

export const ProductViewMode = {
    LIST_VIEW: 'LIST_VIEW',
    GRID_VIEW: 'GRID_VIEW',
};

export function updateStoreData(categories, specialCategories) {
    return {
        type: UPDATE_STORE_DATA,
        categories,
        specialCategories
    };
}

export function selectCategory(selectedCategoryId) {
    return {
        type: SELECT_CATEGORY,
        selectedCategoryId
    };
}

export function selectProduct(selectedProduct) {
    return {
        type: SELECT_PRODUCT,
        selectedProduct
    };
}

export function toggleProductViewMode() {
    return { 
        type: TOGGLE_PRODUCT_VIEW_MODE 
    };
}

