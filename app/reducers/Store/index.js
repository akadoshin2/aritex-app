import {
    UPDATE_STORE_DATA,
    SELECT_CATEGORY,
    SELECT_PRODUCT,
    TOGGLE_PRODUCT_VIEW_MODE,
    ProductViewMode
} from './actions';

const { LIST_VIEW, GRID_VIEW } = ProductViewMode;

const INITIAL_STATE = {
    categories: [],
    specialCategories: [],
    selectedCategoryId: undefined,
    selectedProduct: undefined,
    viewMode: GRID_VIEW
};

export default function Store(state = INITIAL_STATE, action) {
    switch (action.type) {
        case UPDATE_STORE_DATA:
            return Object.assign({}, state, {
                categories: action.categories,
                specialCategories: action.specialCategories
            });
        case SELECT_CATEGORY:
            return Object.assign({}, state, {
                selectedCategoryId: action.selectedCategoryId
            });
        case SELECT_PRODUCT:
            return Object.assign({}, state, {
                selectedProduct: action.selectedProduct
            });
        case TOGGLE_PRODUCT_VIEW_MODE:
            return Object.assign({}, state, {
                viewMode: state.viewMode === LIST_VIEW ? GRID_VIEW : LIST_VIEW
            });
        default:
            return state;
    }
}
