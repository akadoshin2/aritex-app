export const SWITCH_LANGUAGE = 'SWITCH_LANGUAGE';

export function switchLanguage(language) {
    return {
        type: SWITCH_LANGUAGE,
        language
    };
}
