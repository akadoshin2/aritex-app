export const ASSIGN_AUTH_FIREBASE = 'ASSIGN_AUTH_FIREBASE';
export const ASSIGN_DATABASE_FIREBASE = 'ASSIGN_DATABASE_FIREBASE';

export function assignAuthFirebase(firebaseAuth) {
    return {
        type: ASSIGN_AUTH_FIREBASE,
        firebaseAuth
    };
}

export function assignDatabaseFirebase(firebaseDatabase) {
    return {
        type: ASSIGN_DATABASE_FIREBASE,
        firebaseDatabase
    };
}