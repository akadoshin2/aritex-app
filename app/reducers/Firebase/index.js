import {
    ASSIGN_AUTH_FIREBASE,
    ASSIGN_DATABASE_FIREBASE
} from './actions';

import Constants from './../Constants';
import 'firebase/auth';
import 'firebase/database';
import firebase from 'firebase';
var app = firebase.initializeApp(Constants.initializeAppFirebase);
var database = app.database();
var auth = firebase.auth();

const INITIAL_STATE = auth;

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ASSIGN_AUTH_FIREBASE:
            return auth;
        case ASSIGN_DATABASE_FIREBASE:
            return database;
    }
}