import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Languages from './../../Languages';
import APITexWorker from './../../services/APITexWorker';

import { assignAuthToken, updateCart, emptyCart } from '../Cart/actions';
import { clearUpdater, updateData } from '../Updater/actions';
import { addToActionQueue, clearActionQueue } from '../ActionQueue/actions';
import { showLoadingScreen, hideLoadingScreen } from '../Modal/actions';

export const USER_SIGNIN = 'USER_SIGNIN';
export const USER_SIGNOUT = 'USER_SIGNOUT';
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
export const UPDATE_CLIENTS_DATA = 'UPDATE_CLIENTS_DATA';
export const CHANGE_USER_PASSWORD = 'CHANGE_USER_PASSWORD';
export const UPDATE_USER_PROFILE = 'UPDATE_USER_PROFILE';

export function signIn(identification, password) {
    function action(data) {
        return {
            type: USER_SIGNIN,
            data
        };              
    }

    return (dispatch) => {
        const callback = (data) => {
            dispatch(action(data));
            dispatch(updateCart(data.user.cart));             
            dispatch(assignAuthToken(data.auth_token));
            dispatch(hideLoadingScreen());
            Actions.main();         
        };

        const errorCallback = () => {
            dispatch(hideLoadingScreen());
            Alert.alert(
                'Error',
                'Usuario o contraseña inválida',                
                [
                    {
                        text: Languages.OK, 
                        onPress: () => {}
                    },
                ],
                { cancelable: true }
            );
        };

        dispatch(showLoadingScreen());
        APITexWorker.signInUser(identification, password, callback, errorCallback);
    };
}

export function signOut() {
	function action() {
        return {
            type: USER_SIGNOUT
        };              
    }

    return (dispatch, getState) => {
        const { User, netinfo, Cart } = getState();
        const { auth_token, refData } = User;
        const { appIsOnline } = netinfo;        

        if (appIsOnline) {
            const callback = () => {
                dispatch(clearUpdater());
                dispatch(clearActionQueue());
                dispatch(action());
                Actions.authentication({ type: 'reset' });
                dispatch(hideLoadingScreen());
            };
            const errorCallback = () => {
                dispatch(hideLoadingScreen());
            };

            // dispatch(showLoadingScreen());
            APITexWorker.saveCart({auth_token, refData}, { cart: Cart }, callback, errorCallback);
        }
    };
}

export function updateUserData(data) {
    return {
        type: UPDATE_USER_DATA,
        data
    };
}

export function updateClientsData(data) {
    return {
        type: UPDATE_CLIENTS_DATA,
        data
    };
}

export function submitOrder(offlineParams = {}, offlineCallback = () => {}) {
    return (dispatch, getState) => {
        const offlineAction = Object.keys(offlineParams).length > 0 ? true : false;
        const { netinfo, User, Cart } = getState();
        const { appIsOnline } = netinfo;
        const { auth_token, is_client, settings } = User;
        const { 
            client, 
            cartItems, 
            totalPrice, 
            observation 
        } = offlineAction ? offlineParams : Cart;
        const { abon, abtarget, screenfinal, screenseller, testscreen } = settings;

        const ABTesting = () => {
            if (abon) {
                switch (abtarget) {
                    case 1:
                        if (is_client) return testscreen;
                        return screenseller;
                    case 2:
                        if (is_client) return screenfinal;
                        return testscreen;
                    default:
                        return testscreen;
                }
            } else if (is_client) return screenfinal;
            return screenseller;
        };

        const successfulOrder = (orderID) => {
            Alert.alert(
                Languages.OrderCompleted,
                `${Languages.OrderCompletedDesc} ${orderID}`,
                [
                    {
                        text: Languages.OK, 
                        onPress: () => {}
                    },
                ],
                { cancelable: true }
            );
        };

        const appIsOffline = () => {
            Alert.alert(
                'App sin conexión',
                'Su orden se enviará en cuanto se restablezca la conexión con el servidor',
                [
                    {
                        text: Languages.OK, 
                        onPress: () => {}
                    },
                ],
                { cancelable: true }
            );
        };

        const callback = (orderId) => {                       
            if (!offlineAction) {
                Actions.home({ type: 'reset' }); 
                successfulOrder(orderId);
                dispatch(emptyCart()); 
                dispatch(clearUpdater());
                dispatch(updateData());         
            } else {
                // successfulOrder(orderId);  
                offlineCallback();              
            }
            dispatch(hideLoadingScreen());      
        };

        const errorCallback = () => {
            alert('faltan datos en el pedido');
            setTimeout(()=>{
                dispatch(hideLoadingScreen());   
            }, 1000);
        };

        if (appIsOnline) {
            if (!offlineAction) dispatch(showLoadingScreen());
            APITexWorker.submitOrder(
                User, 
                client, 
                cartItems, 
                observation,
                totalPrice, 
                ABTesting(),
                callback, 
                errorCallback
            ); 
        } else if (!offlineAction) {
            dispatch(addToActionQueue('submitOrder', Cart));
            dispatch(emptyCart());
            appIsOffline();
        }        
    };  
}

export function changePassword(password, newPassword) {
    return {
        type: CHANGE_USER_PASSWORD,
        userPassword: {
            password,
            newPassword
        }
    };
}

export function updateUserProfile(password, newPassword, image) {
    function action() {
        return {
            type: UPDATE_USER_PROFILE
        };              
    }

    return (dispatch, getState) => {
        const { User, netinfo } = getState();
        const { auth_token } = User;
        const { appIsOnline } = netinfo;

        const successfulProfileUpdate = () => {
            Alert.alert(
                'Aviso',
                'Su perfil ha sido actualizado exitosamente.'
            );
        };

        const errorUpdatingProfile = (msj) => {
            Alert.alert(
                'Aviso',
                `Ha habido un problema al actualizar el perfil.\n${msj}`
            );
        };

        if (appIsOnline) {
            const callback = () => {
                successfulProfileUpdate();
                dispatch(clearUpdater());
                dispatch(updateData());                             
                // dispatch(hideLoadingScreen());
            };
            const errorCallback = (msj) => {
                errorUpdatingProfile(msj);
                dispatch(hideLoadingScreen());
            };

            dispatch(action());
            dispatch(showLoadingScreen());
            APITexWorker.updateUser(
                auth_token, 
                { password, newPassword, image }, 
                callback, 
                errorCallback
            );
        }
    };
}
