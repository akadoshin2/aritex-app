import {
    USER_SIGNIN,
    USER_SIGNOUT,
    UPDATE_USER_DATA,
    UPDATE_CLIENTS_DATA,
    CHANGE_USER_PASSWORD,
    UPDATE_USER_PROFILE
} from './actions';

const INITIAL_STATE = {
    auth_token: '', 
    refData: '',
    user: {}, 
    clients: {},
    orders: {},
    settings: {},
    userPassword: {}
};

export default function User(state = INITIAL_STATE, action) {
    switch (action.type) {
        case USER_SIGNIN:
            return Object.assign({}, state, {
                ...action.data
            });

        case USER_SIGNOUT:
            return INITIAL_STATE;

        case UPDATE_USER_DATA:
            return Object.assign({}, state, {
                ...action.data
            });
        
        case UPDATE_CLIENTS_DATA:
            let ordersNumber = [];
            if(state.orders){
                for(let number in state.orders){
                    // console.log(number);
                    ordersNumber.push(state.orders[number]);
                }
            }
            return Object.assign({}, state, {
                clients: action.data,
                // orders: ordersNumber
            });

        case CHANGE_USER_PASSWORD:
            return Object.assign({}, state, {
                userPassword: action.userPassword
            });

        case UPDATE_USER_PROFILE:
            return state;  
            
        default:
            return state;
    }
}
