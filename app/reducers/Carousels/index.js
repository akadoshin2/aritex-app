import {
    UPDATE_CAROUSELS_DATA
} from './actions';

const INITIAL_STATE = {
    carousels: []
};

export default function Carousels(state = INITIAL_STATE, action) {
    switch (action.type) {
        case UPDATE_CAROUSELS_DATA:
            return Object.assign({}, state, {
                carousels: action.data
            });
        default:
            return state;
    }
}
