export const UPDATE_CAROUSELS_DATA = 'UPDATE_CAROUSELS_DATA';

export function updateCarouselsData(data) {
    return {
        type: UPDATE_CAROUSELS_DATA,
        data
    };
}
