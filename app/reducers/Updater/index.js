import moment from 'moment';

import {
    START_UPDATE,
    RECEIVE_DATA,
    FINISH_UPDATE,
    UPDATE_FAILURE,
    CLEAR_UPDATER
} from './actions';

const INITIAL_STATE = {
    timestamp: moment('2017-01-01 00:00:00'),
    fetchingData: 0
};

export default function Updater(state = INITIAL_STATE, action) {
    switch (action.type) {
        case START_UPDATE:
            return Object.assign({}, state, {
                fetchingData: 3
            });
        case RECEIVE_DATA:
            return Object.assign({}, state, {
                fetchingData: action.fetchingData - 1
            });
        case FINISH_UPDATE:
            return Object.assign({}, state, {
                timestamp: action.timestamp,
                fetchingData: 0
            });
        case UPDATE_FAILURE:
            return Object.assign({}, state, {
                error: action.error
            });
        case CLEAR_UPDATER:
            return Object.assign({}, state, {
                ...INITIAL_STATE
            });
        default:
            return state;
    }
}
