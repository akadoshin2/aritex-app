import moment from 'moment';

import APITexWorker from '../../services/APITexWorker';

import { showLoadingScreen, hideLoadingScreen } from '../Modal/actions';
import { updateUserData } from '../User/actions';
import { updateClientsData } from '../User/actions';
import { updateStoreData } from '../Store/actions';
import { updateCarouselsData } from '../Carousels/actions';

export const START_UPDATE = 'START_UPDATE';
export const RECEIVE_DATA = 'RECEIVE_DATA';
export const FINISH_UPDATE = 'FINISH_UPDATE';
export const UPDATE_FAILURE = 'UPDATE_FAILURE';
export const CLEAR_UPDATER = 'CLEAR_UPDATER';

export function startUpdate() {
    function action() {
        return {
            type: START_UPDATE
        };              
    }

    return (dispatch) => {
        dispatch(showLoadingScreen());
        dispatch(action());
    };
}

export function receiveData(now) {
    function action(fetchingData) {
        return {
            type: RECEIVE_DATA,
            fetchingData
        };              
    }

    return (dispatch, getState) => {
        const { Updater } = getState();
        const { fetchingData } = Updater;
        // console.log(fetchingData);
        dispatch(action(fetchingData));
        // console.log(fetchingData);
        if ((fetchingData - 1) === 0) dispatch(finishUpdate(now));      
    };
}

export function finishUpdate(timestamp) {
    function action() {
        return {
            type: FINISH_UPDATE,
            timestamp
        };              
    }

    return (dispatch) => {
        dispatch(hideLoadingScreen());
        dispatch(action());
    };
}

export function updateFailure(error) {
    function action() {
        return {
            type: UPDATE_FAILURE,
            error
        };              
    }

    return (dispatch) => {
        dispatch(hideLoadingScreen());
        dispatch(action());
    };
}

export function clearUpdater() {
    return {
        type: CLEAR_UPDATER
    };
}

export function updateData() {
    const fetchData = (dispatch, auth_token, now) => {
        fetchUserData(dispatch, auth_token, now);
        // fetchClientsData(dispatch, auth_token, now);
        fetchStoreData(dispatch, now);
        fetchCarouselsData(dispatch, now);
    };

    const fetchUserData = (dispatch, auth_token, now) => {
        const callback = (data) => {
            dispatch(updateUserData(data));
            dispatch(receiveData(now));
        };
        const callbackClients = (data) => {
            dispatch(updateClientsData(data));
            dispatch(receiveData(now));
        };
        const errorCallback = (error) => dispatch(updateFailure(error));
        APITexWorker.fetchUserData(auth_token, callback, callbackClients, errorCallback);
    };

    const fetchClientsData = (dispatch, auth_token, now) => {
        const callback = (data) => {
            dispatch(updateClientsData(data));
            dispatch(receiveData(now));
        };
        const errorCallback = (error) => dispatch(updateFailure(error));
        APITexWorker.fetchClientsData(auth_token, callback, errorCallback);
    };

    const fetchStoreData = (dispatch, now) => {
        const callback = (data) => {
            const categories = data.filter((category) => {
                return category.shown === true&&category.special !== true;
            });
            const specialCategories = data.filter((category) => {
                // console.log("category", category);
                return category.shown === true&&category.home === true;
            });

            dispatch(updateStoreData(categories, specialCategories));
            dispatch(receiveData(now));
        };
        const errorCallback = (error) => dispatch(updateFailure(error));
        APITexWorker.fetchStoreData(callback, errorCallback);
    };

    const fetchCarouselsData = (dispatch, now) => {
        const callback = (data) => {
            dispatch(updateCarouselsData(data));
            dispatch(receiveData(now));
        };
        const errorCallback = (error) => dispatch(updateFailure(error));
        // APITexWorker.fetchCarouselsData(callback, errorCallback);
        APITexWorker.fetchSliderData('home', callback, errorCallback);
    };

    return (dispatch, getState) => {
        const { Updater, User, netinfo, ActionQueue } = getState();
        const { timestamp } = Updater;
        const { auth_token, refData } = User;
        const { appIsOnline } = netinfo;
        const now = moment();

        // if (appIsOnline && 
        //     now.diff(timestamp, 'hours') >= 24 && 
        //     ActionQueue.actionQueue.length === 0) {
        //     dispatch(startUpdate());
        //     fetchData(dispatch, auth_token, now);
        // }
        console.log("appIsOnline", appIsOnline);
        if (appIsOnline) {
            //dispatch(startUpdate());
            fetchData(dispatch, User, now);
        }
    };
}
