/**
 * Created by Luan on 10/26/2016.
 */
import LocalizedStrings from 'react-native-localization';

/* API
 setLanguage(languageCode) - to force manually a particular language
 getLanguage() - to get the current displayed language
 getInterfaceLanguage() - to get the current device interface language
 formatString() - to format the passed string replacing its placeholders with the other arguments strings
 */

const Languages = new LocalizedStrings({
    Spanish: {
        //Exit Confirm Dialog
        Exit: 'Salir',
        ExitConfirm: '¿Seguro que quieres salir de esta aplicación?',
        YES: 'SI',
        OK: 'OK',
        CANCEL: 'Cancelar',
        Confirm: 'Confirmar',

        //Scene's Titles
        Login: 'Inicio de sesión',
        Home: 'ARITEX',
        Intro: 'Intro',
        Catalog: 'Catálogo',
        Product: 'Producto',
        MyOrders: 'Mis Ordenes',
        Cart: 'Carrito',
        MyAccount: 'Mi cuenta',
        CreateClient: 'Registrar Cliente',
        MyClients: 'Mis clientes',

        //Login
        Password: 'Contraseña',
        ForgotPassword: '¿Olvidó su contraseña?',

        //TopBar
        ShowFilter: 'Sub Categorias',
        HideFilter: 'Esconder',
        Sort: 'Ordenar',

        //Carousel by category
        SeeMore: 'Ver más',

        //Category
        ThereIsNoMore: 'No hay más productos para mostrar',

        //Product
        AddtoCart: 'Agregar al carrito',
        ProductVariations: 'Variaciones',
        NoVariation: 'Este producto no tiene ninguna variación',
        AdditionalInformation: 'Descripción',
        NoProductDescription: 'Producto sin descripción',
        ProductReviews: 'Comentarios',
        NoReview: 'Este producto aún no tiene comentarios...',
        BUYNOW: 'HACER PEDIDO',
        ProductLimitWaring: 'No se pueden agregar más de 9999999 productos',

        //Cart
        Alert: 'Alerta',
        EditQuantity: 'Editar',
        NoCartItem: 'No hay productos en el carrito',
        Total: 'Total',
        EmptyCheckout: 'Lo sentimos, no puedes hacer una orden con el carrito vacío', // REVISAR
        EmptyClient: 'Lo sentimos, no puedes hacer una orden sin seleccionar algún cliente',
        RemoveCartItemConfirm: '¿Eliminar este producto del carrito?',

        //Sidemenu
        SignIn: 'Iniciar Sesión', // TRADUCIR
        SignOut: 'Log Out', // TRADUCIR
        GuestAccount: 'Guest Account', // TRADUCIR
        CantReactEmailError: 'We can\'t reach your email address, please try other login method', // TRADUCIR
        NoEmailError: 'Your account don\'t have valid email address', // TRADUCIR
        EmailIsNotVerifiedError: 'Your email address is not verified, we can\' trust you', // TRADUCIR

        //Checkout
        ConfirmOrderTitle: 'Confirmar orden',
        ConfirmOrder: 'Presione OK para enviar su orden',
        Checkout: 'Checkout',
        ProceedPayment: 'Proceder al pago',
        Purchase: 'Comprar',
        CashOnDelivery: 'Pagar al entregar',
        CreditCard: 'Tarjeta de crédito',
        PaymentMethod: 'Metodo de pago - No seleccionado',
        PaymentMethodError: 'Seleccione su método de pago',
        PayWithCoD: 'Su compra será pagada cuando los bienes sean entregados',
        PayWithPayPal: 'Su compra será pagada con PayPal',
        PayWithStripe: 'Su compra será pagada con Stripe',
        ApplyCoupon: 'Aplicar cupón',
        CouponPlaceholder: 'CÓDIGO PROMOCIONAL',
        APPLY: 'APLICAR',
        CardNamePlaceholder: 'Nombre escrito en la tarjeta',
        BackToHome: 'Back to Home', // TRADUCIR
        OrderCompleted: 'Su orden ha sido procesada',
        OrderCanceled: 'Su orden ha sido cancelada',
        OrderFailed: 'Algo salió mal...',
        OrderCompletedDesc: 'Su ID de orden es',
        OrderCanceledDesc: 'Ha cancelado la orden. La transacción no se ha completado',
        OrderFailedDesc: 'Hemos encontrado un error al procesar su orden. La transacción no se ha completado. Por favor, inténtelo de nuevo',
        OrderTip: 'Sugerencia: puede realizar un seguimiento del estado de su orden en la sección "Mis ordens" del menú lateral',
        Delivery: 'Entrega',
        Payment: 'Pago',
        Complete: 'Completo',

        //myorder
        MyOrder: 'My Order', // TRADUCIR
        NoOrder: 'You don\'t have any orders', // TRADUCIR
        OrderDate: 'Order Date: ', // TRADUCIR
        OrderStatus: 'Status: ', // TRADUCIR
        OrderPayment: 'Payment method: ', // TRADUCIR
        OrderTotal: 'Total: ', // TRADUCIR
        OrderDetails: 'Order details...', // TRADUCIR

        News: 'News', // TRADUCIR
        PostDetails: 'Post Details', // TRADUCIR
        FeatureArticles: 'Feature articles', // TRADUCIR
        MostViews: 'Most views', // TRADUCIR
        EditorChoice: 'Editor choice', // TRADUCIR

        //settings
        Settings: 'Mi cuenta',
        BASICSETTINGS: 'CONFIGURACIONES BASICAS',
        Language: 'Idioma',
        INFO: 'INFORMACION',
        About: 'Sobre nosotros',

        //language
        AvailableLanguages: 'Idiomas disponibles',
        SwitchLanguage: 'Cambiar idioma',
        SwitchLanguageConfirm: 'Cambiar el idioma requiere reiniciar el app. ¿Desea continuar?',

        //about us
        AppName: 'Aritex',
        AppDescription: 'Somos una organización productora y comercializadora de prendas de vestir en tejido de punto a nivel nacional, con moderna tecnología y un equipo humano comprometido que permite satisfacer las necesidades y expectativas de nuestros clientes.',
        AppContact: 'LÍNEA DE ATENCIÓN AL CLIENTE',
        AppPhone: ' (57) 2 390 4242 EXT. 311 / Movil 311 7497213',
        AppAddress: ' Carrera 39 # 11-81 Acopi, Yumbo / Valle del Cauca, Colombia',        
        AppEmail: ' Email: contacto@aritexdecolombia.co',
        AppCopyRights: '© Flamin Lab 2017',

        //form
        NotSelected: 'No seleccionado',
        EmptyError: 'Este campo está vacío',
        DeliveryInfo: 'Información de entrega',
        FirstName: 'Nombre',
        LastName: 'Apellido',
        Address: 'Dirección',
        State: 'Estado/Ciudad',
        NotSelectedError: 'Por favor elige uno',
        Postcode: 'Código postal',
        Country: 'País',
        Email: 'Email',
        Phone: 'Número de teléfono',
        Note: 'Nota',

        //search
        Search: 'Buscar',
        SearchPlaceHolder: 'Buscar producto por nombre',
        NoResultError: 'Su palabra clave de búsqueda no coincide con ningún producto.',
        Details: 'Detalles',

        //filter panel
        Categories: 'Categorias',
    },
    English: {
        //Exit Confirm Dialog
        Exit: 'Exit',
        ExitConfirm: 'Are you sure you want to exit this app',
        YES: 'YES',
        OK: 'OK',
        CANCEL: 'CANCEL',
        Confirm: 'Confirm',

        //Scene's Titles
        Login: 'Login',
        Home: 'Aritex',
        Intro: 'Intro',
        Catalog: 'Catalog',
        Product: 'Product',
        MyOrders: 'My Orders',
        Cart: 'Cart',
        MyAccount: 'My Account',
        CreateClient: 'Create client',
        MyClients: 'My clients',

        //Login
        Password: 'Password',
        ForgotPassword: 'Forgot password?',

        //TopBar
        ShowFilter: 'Sub Categories',
        HideFilter: 'Hide',
        Sort: 'Sort',

        //Carousel by category
        SeeMore: 'See More',

        //Category
        ThereIsNoMore: 'There is no more product to show',

        //Product
        AddtoCart: 'Add to Cart',
        ProductVariations: 'Variations',
        NoVariation: 'This product don\'t have any variation',
        AdditionalInformation: 'Description',
        NoProductDescription: 'No Product Description',
        ProductReviews: 'Reviews',
        NoReview: 'This product don\'t have any reviews ...yet',
        BUYNOW: 'MAKE ORDER',
        ProductLimitWaring: 'You can\'t add more than 9999999 product',

        //Cart
        Alert: 'Alert',
        EditQuantity: 'Edit',
        NoCartItem: 'There is no product in cart',
        Total: 'Total',
        EmptyCheckout: 'Sorry, you can\'t check out an empty cart',
        EmptyClient: 'Sorry, you must select a client',
        RemoveCartItemConfirm: 'Remove this product from cart?',

        //Sidemenu
        SignIn: 'SIGN IN',
        SignOut: 'Log Out',
        GuestAccount: 'Guest Account',
        CantReactEmailError: 'We can\'t reach your email address, please try other login method',
        NoEmailError: 'Your account don\'t have valid email address',
        EmailIsNotVerifiedError: 'Your email address is not verified, we can\' trust you',

        //Checkout
        ConfirmOrderTitle: 'Confirm order',
        ConfirmOrder: 'Press OK to submit your order',
        Checkout: 'Checkout',
        ProceedPayment: 'Proceed Payment',
        Purchase: 'Purchase',
        CashOnDelivery: 'Cash on Delivery',
        CreditCard: 'Credit Card',
        PaymentMethod: 'Payment Method - Not select',
        PaymentMethodError: 'Please select your payment method',
        PayWithCoD: 'Your purchase will be pay when goods were delivered',
        PayWithPayPal: 'Your purchase will be pay with PayPal',
        PayWithStripe: 'Your purchase will be pay with Stripe',
        ApplyCoupon: 'Apply Coupon',
        CouponPlaceholder: 'COUPON CODE',
        APPLY: 'APPLY',
        CardNamePlaceholder: 'Name written on card',
        BackToHome: 'Back to Home',
        OrderCompleted: 'Your order was completed',
        OrderCanceled: 'Your order was canceled',
        OrderFailed: 'Something went wrong...',
        OrderCompletedDesc: 'Your order id is ',
        OrderCanceledDesc: 'You have canceled the order. The transaction has not been completed',
        OrderFailedDesc: 'We have encountered an error while processing your order. The transaction has not been completed. Please try again',
        OrderTip: 'Tip: You could track your order status in "My Orders" section from side menu',
        Delivery: 'Delivery',
        Payment: 'Payment',
        Complete: 'Complete',

        //myorder
        MyOrder: 'My Order',
        NoOrder: 'You don\'t have any orders',
        OrderDate: 'Order Date: ',
        OrderStatus: 'Status: ',
        OrderPayment: 'Payment method: ',
        OrderTotal: 'Total: ',
        OrderDetails: 'Order details...',

        News: 'News',
        PostDetails: 'Post Details',
        FeatureArticles: 'Feature articles',
        MostViews: 'Most views',
        EditorChoice: 'Editor choice',

        //settings
        Settings: 'My account',
        BASICSETTINGS: 'BASIC SETTINGS',
        Language: 'Language',
        INFO: 'INFO',
        About: 'About us',

        //language
        AvailableLanguages: 'Available Languages',
        SwitchLanguage: 'Switch Language',
        SwitchLanguageConfirm: 'Switch language require an app reload, continue?',

        //about us
        AppName: 'Aritex',
        AppDescription: 'Somos una organización productora y comercializadora de prendas de vestir en tejido de punto a nivel nacional, con moderna tecnología y un equipo humano comprometido que permite satisfacer las necesidades y expectativas de nuestros clientes.',
        AppContact: 'LÍNEA DE ATENCIÓN AL CLIENTE',
        AppPhone: ' (57) 2 390 4242 EXT. 311 / Movil 311 7497213',
        AppAddress: ' Carrera 39 # 11-81 Acopi, Yumbo / Valle del Cauca, Colombia',        
        AppEmail: ' Email: contacto@aritexdecolombia.co',
        AppCopyRights: '© Flamin Lab 2017',

        //form
        NotSelected: 'Not selected',
        EmptyError: 'This field is empty',
        DeliveryInfo: 'Delivery Info',
        FirstName: 'First Name',
        LastName: 'Last Name',
        Address: 'Address',
        State: 'State/City',
        NotSelectedError: 'Please choose one',
        Postcode: 'Postcode',
        Country: 'Country',
        Email: 'Email',
        Phone: 'Phone Number',
        Note: 'Note',

        //search
        Search: 'Search',
        SearchPlaceHolder: 'Search product by name',
        NoResultError: 'Your search keyword did not match any products.',
        Details: 'Details',

        //filter panel
        Categories: 'Categories',
    },
    ///Put other languages here
});

export default Languages;
