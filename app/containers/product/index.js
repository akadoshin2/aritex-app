'use strict';

import React, { Component } from 'react';
import { Text, View, ScrollView, Image } from 'react-native';
import { Body1, Body2, Caption, RaisedButton } from 'carbon-ui';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';

import Constants from './../../Constants';
import Languages from './../../Languages';
import Toolbar from '../../components/Toolbar';

import AppEventEmitter from './../../utils/AppEventEmitter';
import { openModal } from '../../reducers/Modal/actions';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.styles = {
            container: { flex: 1 },
            container_row: {
                flexDirection: 'row',
            },
            card: {
                backgroundColor: 'white',
                marginBottom: 0,
                padding: Constants.Dimension.ScreenWidth(0.05),
            },
            label: {
                color: Constants.Color.TextDark,
                fontSize: 20,
                fontWeight: 'bold',
            },
        };

        this.swiperHeight = Constants.Dimension.ScreenWidth() * 1.1;
    }

    componentDidMount() {
        if (this.props.addProduct) {
            this.props.openModal('variations-panel'); 
        }
    }

    renderSwiper(product) {
        const renderImages = () => {
            return (
                product.images.map((image, index) =>
                    <Image
                        key={index}
                        source={{ uri: image.src }}
                        resizeMode='cover'
                        style={{ flex: 1 }}
                    />,
                    this
                )   
            );
        };

        return (
            <Swiper
                height={this.swiperHeight}
                dot={<View style={Constants.Swiper.swiper_dot} />}
                activeDot={<View style={Constants.Swiper.swiper_active_dot} />}
                paginationStyle={{ bottom: 20 }}
            >
                {renderImages()}
            </Swiper>
        );
    }

    renderTopInfo(product) {
        const styles = {
            name: {
                color: Constants.Color.TextDark,
                fontSize: 26,
                margin: 5,
                marginBottom: 0,
                textAlign: 'center'
            },
            price: {
                color: Constants.Color.ProductPrice,
                fontSize: 18,
                fontWeight: 'bold',
                margin: 5,
                marginRight: 0,
            }
        };
        return (
            <View style={this.styles.card}>
                <Text style={styles.name}>
                    {product.name}
                </Text>
                <Body1 
                    style={{
                        margin: 5,
                        marginBottom: 0,
                        textAlign: 'center'
                    }}
                >
                    {`Ref. ${product.reference}`}
                </Body1>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.price}>
                            {Constants.Formatter.currency(product.price) }
                        </Text>                        
                    </View>                    
                </View>
            </View>
        );
    }

    renderDescription(product) {
        return (
            <View style={this.styles.card}>
                <Text
                    style={[
                        this.styles.label,
                        { marginBottom: -10 }]
                    }
                >
                    Composición de la tela
                </Text>
                {
                    product.description === '' ?
                        <Text 
                            style={{
                                marginTop: 20,
                                color: Constants.Color.TextDark,
                                fontSize: 14
                            }}
                        >
                            {Languages.NoProductDescription}
                        </Text> :
                        <View style={{ marginTop: 10 }}>
                            <Text 
                                style={{
                                    marginTop: 10,
                                    color: Constants.Color.TextDark,
                                    fontSize: 14
                                }}
                            >
                                {product.description}
                            </Text> 
                        </View>
                }
            </View>
        );
    }

    renderVariations(product) {
        const renderSizeItems = () => {
            return product.variations.size.map((item) => 
                <View
                    key={item}
                    style={{
                        backgroundColor: '#F1F1F1',
                        width: 40,
                        height: 40,
                        borderRadius: 20,
                        marginRight: 12,
                        paddingTop: 2,
                        alignItems: 'center'
                    }}
                >
                    <Body2>
                        {item}
                    </Body2>
                </View>
            );
        };

        const renderColorItems = () => {
            return product.variations.color.map((item, colorIndex) => {
                const index = Constants.ReferenceColors.findIndex((color) => {
                    return item === color.color;
                });

                if (item === 'SURTIDO (999)') return undefined;

                return (
                    <View
                        key={item}
                        style={{
                            width: 100,
                            marginBottom: 12
                        }}
                    >
                        <View
                            style={{ 
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}
                        >
                            <View                            
                                style={{
                                    height: 20,
                                    width: 50,
                                    marginRight: 8,                              
                                    backgroundColor: index !== -1 ? 
                                        Constants.ReferenceColors[index].hex :
                                        'white',
                                    borderWidth: 1,
                                    borderColor: index !== -1 ?
                                        'black' :
                                        'red'
                                }}
                            />
                            <Caption>
                                {product.variations.colorId[colorIndex]}
                            </Caption>
                        </View>
                        <Caption>
                            {Constants.Formatter.capitalize(item)}
                        </Caption>
                    </View>
                );
            });
        };

        return (
            <View>
                <View style={this.styles.card}>
                    <Text style={this.styles.label}>
                        Tallas
                    </Text>
                    <View 
                        style={{ 
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            flexWrap: 'wrap',
                            marginTop: 20,
                        }}
                    >
                        {renderSizeItems()}
                    </View>
                </View>

                <View style={this.styles.card}>
                    <Text style={this.styles.label}>
                        Colores
                    </Text>
                    <View 
                        style={{ 
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'flex-start',
                            flexWrap: 'wrap',
                            marginTop: 20,
                        }}
                    >
                        {renderColorItems()}
                    </View>               
                </View>
            </View>
        );
    }

    renderAddCartButton() {
        const openVariationsPanel = () => {     
            this.props.openModal('variations-panel');
        };

        return (
            <View 
                style={{ 
                    flexDirection: 'row', 
                    justifyContent: 'center',
                    backgroundColor: 'yellow' 
                }}
            >
                <RaisedButton
                    onPress={() => openVariationsPanel()}
                    style={{
                        width: Constants.Dimension.ScreenWidth(1),
                        backgroundColor: '#1976D2'
                    }}
                >
                    Agregar al pedido
                </RaisedButton>
            </View>
        );
    }

    render() {
        const { product } = this.props;

        return (
            <View style={{ flex: 1 }}>
                <View
                    style={this.styles.container}
                >                    
                    <Toolbar {...this.props} />                                        
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={this.styles.container}
                    >
                        {this.renderSwiper(product)}
                        <View style={this.styles.container}>
                            {this.renderTopInfo(product)}
                            {this.renderDescription(product)}
                            {this.renderVariations(product)}
                        </View>
                    </ScrollView>
                    {this.renderAddCartButton()}
                </View>                
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        // cartItems: store.Cart.cartItems,
        is_client: store.User.is_client
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        openModal: (modal) => {
            dispatch(openModal(modal));
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Product);
