'use strict';

import React, { Component } from 'react';
import { 
    View, 
    Alert,
    ScrollView 
} from 'react-native';
import { Body2, Paper, RaisedButton } from 'carbon-ui';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Actions } from 'react-native-router-flux';
// import Icon from 'react-native-vector-icons/Ionicons';

import Constants from './../../Constants';
import LogoSpinner from './../../components/LogoSpinner';
import Toolbar from './../../components/Toolbar';
import APITexWorker from './../../services/APITexWorker';

import { signIn } from '../../reducers/User/actions';
import { hideLoadingScreen } from '../../reducers/Modal/actions';

const renderTextField = ({ 
    label,
    input,
    multiline = false,
    autoCorrect = false,
    keyboardType = 'default',
    returnKeyType = 'next',
    secureTextEntry = false,
    meta: { touched, error }, 
    ...custom 
}) => {
    const onFocusCallback = () => {

    };

    const onBlurCallback = (e) => {
        input.onBlur(e);
    };

    return (   
        <TextField
            fontSize={14}
            label={label}
            value={input.value} 
            animationDuration={150}
            multiline={multiline}
            autoCorrect={autoCorrect}
            keyboardType={keyboardType}
            returnKeyType={returnKeyType}
            secureTextEntry={secureTextEntry}
            error={touched && error ? error : null}
            onChangeText={(value) => input.onChange(value)}
            onFocus={() => onFocusCallback()}
            onBlur={(e) => onBlurCallback(e)}
            {...custom}      
        />
    );
};

const validate = (values) => {
    const errors = {};
    const requiredFields = [
        'name',
        'cc',
        'email',
        'password',        
        'address1',
        'address2',
        'city',
        'zipcode',
        'phone',
        'mobile'       
    ];

    requiredFields.forEach((field) => {
        if (!values[field]) {
            errors[field] = 'Campo requerido';
        }
    });

    return errors;
};

class SignUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        };
    }

    componentWillMount() {

    }

    renderPersonalDataInputs() {
        return (
            <Paper
                elevation={2}
                style={{
                    padding: 8
                }}
            >
                <Body2
                    style={{
                        alignSelf: 'flex-end'
                    }}
                >
                    Información Personal
                </Body2>
                <Field                    
                    name="name"
                    component={renderTextField}
                    label="Nombre"
                    icon="ios-mail"
                />    
                <Field                    
                    name="cc"
                    component={renderTextField}
                    label="Cédula de identidad"
                    icon="ios-mail"
                    keyboardType="numeric"
                />    
                <Field                    
                    name="email"
                    component={renderTextField}
                    label="Correo electrónico"
                    icon="ios-mail"
                    keyboardType="email-address"
                />
                <Field                    
                    name="password"
                    component={renderTextField}
                    label="Contraseña"
                    icon="ios-mail"
                    secureTextEntry
                />
                <Field                    
                    name="phone"
                    component={renderTextField}
                    label="Teléfono fijo"
                    icon="ios-mail"
                    keyboardType="numeric"
                />
                <Field                    
                    name="mobile"
                    component={renderTextField}
                    label="Teléfono movil"
                    icon="ios-mail"
                    keyboardType="numeric"
                    returnKeyType="done"
                />
            </Paper>
        );
    }

    renderAddressDataInputs() {
        return (
            <Paper
                elevation={2}
                style={{
                    marginTop: 20,
                    padding: 8
                }}
            >
                <Body2
                    style={{
                        alignSelf: 'flex-end'
                    }}
                >
                    Dirección de envío
                </Body2>
                <Field                    
                    name="address1"
                    component={renderTextField}
                    label="Dirección 1"
                    icon="ios-mail"
                    multiline
                    autoCorrect
                />
                <Field                    
                    name="address2"
                    component={renderTextField}
                    label="Dirección 2"
                    icon="ios-mail"
                    multiline
                    autoCorrect
                />
                <Field                    
                    name="city"
                    component={renderTextField}
                    label="Ciudad"
                    icon="ios-mail"
                />
                <Field                    
                    name="zipcode"
                    component={renderTextField}
                    label="Código Postal"
                    icon="ios-mail"
                />  
            </Paper>
        );
    }

    renderInputs() {
        return (   
            <ScrollView
                keyboardShouldPersistTaps="handled"
                style={{ 
                    // marginTop: 10,
                    marginHorizontal: 4
                    // paddingHorizontal: 15,
                    // paddingBottom: 10
                }}
            >
                {this.renderPersonalDataInputs()}
                {this.renderAddressDataInputs()}                
            </ScrollView>
        );
    }

    renderSubmitButton() {
        const { handleSubmit } = this.props;

        const signUp = (values) => {
            const { 
                name, 
                cc, 
                email, 
                password, 
                address1, 
                address2, 
                city, 
                zipcode, 
                phone, 
                mobile 
            } = values;
            const self = this;

            const callback = (data) => {
                self.setState({ isLoading: false }); 
                dispatch(hideLoadingScreen());
                self.props.signIn(data);
                Actions.main();
            };

            const errorCallback = () => {
                self.setState({ isLoading: false });
                Alert.alert(
                    'Error',
                    'Error',                
                    [
                        {
                            text: 'OK', 
                            onPress: () => {}
                        },
                    ],
                    { cancelable: true }
                );
            };

            this.setState({ isLoading: false });
            APITexWorker.signUpUser(
                {
                    name,
                    cc,
                    email,
                    password,        
                    address: address1.concat(' ', address2),
                    city,
                    zipcode,
                    phone,
                    mobile
                }, 
                ()=>{}, 
                errorCallback
            );
        };

        return (
            <View
                style={{
                    alignItems: 'center'
                }}
            >
                {/* <RaisedButton
                    onPress={handleSubmit(signUp)}
                    style={{ 
                        width: Constants.Dimension.ScreenWidth(1),
                        backgroundColor: '#1976D2'
                    }}
                >
                    REGISTRARSE
                </RaisedButton> */}
            </View>
        );
    }

    render() {
        const { isLoading } = this.state;

        if (isLoading) {
            return (
                <View style={{ flex: 1 }}>
                    <LogoSpinner fullStretch />
                </View>
            );
        }

        return (
            <View 
                style={{ 
                    flex: 1,
                    backgroundColor: 'white'
                }}
            >
                <Toolbar {...this.props} />
                {this.renderInputs()}
                {this.renderSubmitButton()}
            </View>
        );
    }
}

function mapStateToProps(store) {
    return {
   
    };
}

function mapDispatchToProps(dispatch) {
    return {
        signIn: (auth_token, user, clients) => {
            dispatch(signIn(auth_token, user, clients));
        }
    };
    //return bindActionCreators({ change, initialize }, dispatch);    
}

const withRedux = connect(mapStateToProps, mapDispatchToProps)(SignUp);

export default reduxForm({
    form: 'signUpForm',
    validate
})(withRedux);
