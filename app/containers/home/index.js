'use strict';

import React, { Component } from 'react';
import { View, Linking, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import Toolbar from './../../components/Toolbar';
import CarouselFull from './../../components/CarouselFullWidth';
import HomeCarousel from './../../components/HomeCarousel';
import LogoSpinner from './../../components/LogoSpinner';
import BottomBar from '../../components/BottomBar';

import { selectCategory, selectProduct } from '../../reducers/Store/actions';
import { updateData } from '../../reducers/Updater/actions';
import { dispatchQueuedActions } from '../../reducers/ActionQueue/actions';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        const { updateData, dispatchQueuedActions } = this.props;

        updateData();
        dispatchQueuedActions();                
    }

    renderCarouselFull() {
        const { selectCategory, imageCarouselData } = this.props;

        const goToProduct = (product) => {
            this.props.selectCategory(product.categories[0]);
            this.props.selectProduct(product);
            Actions.product({ product, addProduct: false }); 
        };

        const goToCategory = (category) => {
            this.props.selectCategory(category._id);
            Actions.category({
                initCategoryId: category._id,
                title: category.name
            });
        };

        const onPressCard = (card) => {
            console.log("card", card)
            switch (card.redirect) {
                case 1: // Redirecciona a un producto
                    goToProduct(card.data);
                    break;
                case 2: // Redirecciona a una categoría
                    goToCategory(card.data);
                    break;
                case 0: // Redirecciona a un URL
                    Linking.openURL(card.redirect_url);
                    break;
                default:
                    console.log('HELLO');
            }
        };

        return (
            <CarouselFull
                data={imageCarouselData}
                onPressCard={onPressCard}
            />
        );
    }

    renderHomeCarousels() {
        const { homeCarouselsData } = this.props;

        return homeCarouselsData.map((carousel, index) => {
            return (
                <HomeCarousel
                    key={`homeCarousel${index}`}
                    title={carousel.name}
                    products={carousel.products}
                />
            );
       });
    }

    renderContent() {
        const { isLoading } = this.props;

        if (isLoading) return <LogoSpinner fullStretch />;
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    {this.renderCarouselFull()}
                    {this.renderHomeCarousels()}
                </ScrollView>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Toolbar {...this.props} />
                {this.renderContent()}
                <BottomBar {...this.props} />
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    const { Updater, Carousels, Store } = store;
    // console.log(Store.specialCategories);

    return {
        isLoading: Updater.isLoading,
        imageCarouselData: Carousels.carousels.images,
        homeCarouselsData: Store.specialCategories
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateData: () => {
            dispatch(updateData());
        },
        dispatchQueuedActions: () => {
            dispatch(dispatchQueuedActions());
        },
        selectCategory: (selectedCategoryId, selectedCategoryName) => {
            dispatch(selectCategory(selectedCategoryId, selectedCategoryName));
        },
        selectProduct: (selectedProduct) => {
            dispatch(selectProduct(selectedProduct));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
