'use strict';

import React, { Component, PropTypes } from 'react';
import { Alert, Text, View, ListView } from 'react-native';
import { RaisedButton } from 'carbon-ui';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';

import Constants from './../../Constants';
import Languages from './../../Languages';
import CartItemRow from './CartItemRow';

import { openModal } from '../../reducers/Modal/actions';
import { submitOrder } from '../../reducers/User/actions';

class Cart extends Component {
    static propTypes = {
        Cart: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
        this.styles = {
            container: { 
                flex: 1 
            },
            totalText: {
                color: Constants.Color.TextLight,
                fontSize: 16,
                fontWeight: 'bold',
                marginRight: 10
            },
            totalPrice: {
                color: Constants.Color.ProductPrice,
                fontWeight: 'bold',
                marginRight: 5,
            }
        };

        this.state = {};        
    }

    componentDidMount() {
        const { Cart } = this.props;
        const { cartItems } = Cart;

        if (cartItems.length > 0) {
            TimerMixin.setTimeout(
                () => this.refs.listView.scrollToEnd({ animated: true }), 
                400
            );   
        }               
    }

    renderError(error) {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>
                    {error}
                </Text>
            </View>
        );
    }

    renderCartItems(data) {
        const dataSource = new ListView.DataSource({ rowHasChanged: () => true });
        const renderRow = (cartItem) => 
            <CartItemRow cartItem={cartItem} />;

        return (  
            <ListView
                ref="listView"
                dataSource={dataSource.cloneWithRows(data)}
                renderRow={renderRow}
                enableEmptySections
            />
        );
    }

    renderTotal() {
        return (
            <View 
                style={{
                    height: 50,
                    flexDirection: 'row',
                    paddingTop: 10,
                    borderTopWidth: 1,
                    borderTopColor: Constants.Color.ViewBorder,
                }}
            >
                <View 
                    style={{ 
                        flexDirection: 'row',
                        alignItems: 'center',
                        flex: 1,
                    }}
                >
                    <Text style={this.styles.totalText}>
                        Total productos
                    </Text>
                    <Text style={[this.styles.totalPrice, { marginTop: 2 }]}>
                        {this.props.Cart.totalProducts}
                    </Text> 
                </View>
                <View style={{ flex: 1 }}>
                    <View 
                        style={{ 
                            flexDirection: 'row',
                            alignItems: 'center',
                            flex: 1 
                        }}
                    >
                        <Text style={this.styles.totalText}>
                            Total
                        </Text>
                        <Text style={this.styles.totalPrice}>
                            {Constants.Formatter.currency(this.props.Cart.totalPrice)}
                        </Text>
                    </View>
                    {/* Los precios ya no tienen IVA */}
                    {/* <View 
                        style={{ 
                            flexDirection: 'row', 
                            alignItems: 'center',
                            flex: 1 
                        }}
                    >
                        <Text style={this.styles.totalText}>
                            Total + IVA
                        </Text>
                        <Text style={this.styles.totalPrice}>
                            {Constants.Formatter.currency(this.props.Cart.totalPriceIVA)}
                        </Text>
                    </View> */}
                </View>
            </View>
        );
    }

    renderSelectClientButton() {
        const { client } = this.props.Cart;
        const openSelectClientModal = () => {
            this.props.openModal('select-client-panel');
        };

        return (
            <RaisedButton 
                onPress={() => openSelectClientModal()}
                style={{
                    width: Constants.Dimension.ScreenWidth(0.46),
                    backgroundColor: '#1976D2'
                }}
            >
                { 
                    client ? 
                    `Cambiar: ${client.client_id}` : 
                    'Asociar cliente' 
                }
            </RaisedButton>
        );    
    }

    renderObservationButton() { 
        const openObservationPanel = () => {
            this.props.openModal('observation-panel');
        };

        return (
            <RaisedButton 
                onPress={() => openObservationPanel()}
                style={{ 
                    width: Constants.Dimension.ScreenWidth(0.46),
                    backgroundColor: '#1976D2'
                }}
            >
                Observaciones
            </RaisedButton>
        ); 
    }

    renderCheckoutButton() {
        const confirmOrder = () => {
            Alert.alert(
                Languages.ConfirmOrderTitle,
                Languages.ConfirmOrder,
                [            
                    {
                        text: Languages.CANCEL, 
                        onPress: () => {}, 
                        style: 'cancel'
                    },
                    {
                        text: Languages.OK, 
                        onPress: () => this.props.submitOrder()
                    },
                ],
                { cancelable: true }
            );
        };

        const emptyCheckout = () => {
            Alert.alert(
                Languages.Alert,
                Languages.EmptyCheckout,                
                [
                    {
                        text: Languages.OK, 
                        onPress: () => {}
                    },
                ],
                { cancelable: true }
            );
        };

        const emptyClient = () => {
            Alert.alert(
                Languages.Alert,
                Languages.EmptyClient,                
                [
                    {
                        text: Languages.OK, 
                        onPress: () => {}
                    },
                ],
                { cancelable: true }
            );
        };

        const cartCheckout = () => {
            const { is_client } = this.props;
            const { cartItems, client } = this.props.Cart;

            if (cartItems.length === 0) emptyCheckout();
            else if (client.length === 0 && !is_client) emptyClient();
            else confirmOrder();
        };

        return (
            <RaisedButton
                onPress={() => cartCheckout()}
                style={{ 
                    width: Constants.Dimension.ScreenWidth(1),
                    backgroundColor: '#1976D2'
                }}
            >
                Hacer Pedido
            </RaisedButton>
        );
    }

    render() {
        const { is_client } = this.props;

        return (
            <View style={{ flex: 1 }}>                
                <View 
                    style={{
                        flex: 1,
                        paddingHorizontal: 5
                    }}
                >
                    {
                        this.props.Cart.cartItems.length === 0 ?
                            this.renderError(Languages.NoCartItem) :
                            this.renderCartItems(this.props.Cart.cartItems)
                    }
                    {this.renderTotal()}
                </View> 
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                >
                    {/* {
                        !is_client ? 
                            <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    marginVertical: 8
                                }}
                            >   
                                {this.renderSelectClientButton()}
                                {this.renderObservationButton()}
                            </View> :
                            undefined
                    }                     */}
                     <View
                                style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    marginVertical: 8
                                }}
                     >   
                                {this.renderSelectClientButton()}
                                {this.renderObservationButton()}
                            </View> 
                    {this.renderCheckoutButton()}
                </View>
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        Cart: store.Cart,
        is_client: store.User.is_client
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        openModal: (modal) => {
            dispatch(openModal(modal));
        },
        submitOrder: () => {
            dispatch(submitOrder());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
