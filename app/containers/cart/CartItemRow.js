import React, { Component, PropTypes } from 'react';
import { Alert, Text, View, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

import Constants from './../../Constants';
import Languages from './../../Languages';
import VariationsPanel from '../../components/SelectVariationsPanel';
import { addCartItem, deleteCartItem } from '../../reducers/Cart/actions';

class CartItemRow extends Component {
    static propTypes = {
        cartItem: PropTypes.object.isRequired,
        addCartItem: PropTypes.func.isRequired,        
        deleteCartItem: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {};

        this.styles = {
            container: {
                height: undefined,
                width: undefined,
                borderColor: Constants.Color.ViewBorder,
                borderWidth: 1,
                flexDirection: 'row',
                marginTop: 10,
            },
            image: {
                width: Constants.Dimension.ScreenWidth(0.3),
                height: Constants.Dimension.ScreenWidth(0.3) * 1.2,
            },
            product_name: {
                padding: 10,
                paddingTop: 5,
                paddingBottom: 5,
                height: undefined,
                justifyContent: 'flex-start',
            },
            product_price: {
                padding: 10,
                paddingTop: 0,
                paddingBottom: 0,
                flex: 1,
                justifyContent: 'flex-start',
            },
            product_variation: {
                padding: 10,
                paddingTop: 5,
                paddingBottom: 5,
                flex: 1,
                justifyContent: 'center',
            },
            buttons_wrapper: {
                padding: 10,
                paddingTop: 5,
                paddingBottom: 5,
                flex: 1,
                justifyContent: 'flex-end',
            },
            itemWrapper: {
                justifyContent: 'center',
                padding: 0,
                marginLeft: 10,
            },
        };
    }

    renderProductImage() {
        const { images } = this.props.cartItem.product;

        return (
            <Image
                source={
                    images[0] !== undefined ?
                        { uri: images[0].src } :
                        Constants.Image.PlaceHolder
                }
                style={this.styles.image}
                resizeMode="cover"
            />
        );
    }

    renderProductName() {
        const { product } = this.props.cartItem;

        return (
            <Text
                ellipsizeMode={'tail'}
                numberOfLines={2}
                style={this.styles.product_name}
            >
                {product.name}
            </Text>
        );
    }

    renderProductReference() {
        const { product } = this.props.cartItem;

        return (
            <Text
                ellipsizeMode={'tail'}
                numberOfLines={2}
                style={this.styles.product_name}
            >
                {`Ref. ${product.reference}`}
            </Text>
        );
    }

    renderPrice() {
        const { product } = this.props.cartItem;
        const styles = {
            row: {
                flexDirection: 'row',
            },
            price: {
                color: Constants.Color.ProductPrice,
                fontSize: 14,
                fontWeight: 'bold',
                marginRight: 5,
            }
        };

        return (
            <View style={this.styles.product_price}>
                <View style={styles.row}>
                    <Text style={[styles.price]}>
                        {Constants.Formatter.currency(product.price) }
                    </Text>
                </View>
            </View>
        );
    }

    renderVariations() {
        const { cartItem } = this.props;

        return (
            <VariationsPanel
                productCart={cartItem.product}
                isModal={false}
            />
        );
    }

    renderDeleteButton() {
        const { cartItem, deleteCartItem } = this.props;
        const { product, variations, quantity } = cartItem;

        const onPressDelete = () => {
            Alert.alert(
                Languages.Confirm,
                Languages.RemoveCartItemConfirm,
                [
                    {
                        text: Languages.CANCEL,
                        onPress: () => undefined
                    },
                    {
                        text: Languages.YES,
                        onPress: () => deleteCartItem(product, variations, quantity)
                    },
                ]
            );
        };

        return (
            <View style={this.styles.buttons_wrapper}>  
                <TouchableOpacity
                    onPress={() => onPressDelete()}
                    style={this.styles.itemWrapper}
                >
                    <Icon
                        name={Constants.Icon.Delete}
                        size={25}
                        color={Constants.Color.ViewBorder}
                    />
                </TouchableOpacity> 
            </View>
        );
    }

    render() {
        return (
            <View
                style={{
                    height: undefined,
                    width: undefined,
                    borderColor: Constants.Color.ViewBorder,
                    borderWidth: 1,
                    marginTop: 10,
                }}
            >
                <View 
                    ref="datViewDAMN" 
                    style={{
                        height: undefined,
                        width: undefined,
                        flexDirection: 'row'
                    }}
                >
                    {this.renderProductImage()}
                    <View style={{ flex: 1 }}>
                        {this.renderProductName()}
                        {this.renderProductReference()}
                        {this.renderPrice()}                    
                        {this.renderDeleteButton()}
                    </View>
                </View>
                <View style={{ flex: 1 }}>
                    {this.renderVariations()}
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        Cart: state.Cart
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addCartItem: (product, variations, amount) => {
            dispatch(addCartItem(product, variations, amount));
        },
        deleteCartItem: (product, variations, quantity) => {
            dispatch(deleteCartItem(product, variations, quantity));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartItemRow);
