'use strict';

import React, { Component } from 'react';
import { View } from 'react-native';
import { Paper } from 'carbon-ui';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';

import Toolbar from './../../components/Toolbar';
import { addObservation } from '../../reducers/Cart/actions';

class ObservationPanel extends Component {
    constructor(props) {
        super(props);

        this.state = {};              
    }

    componentDidMount() {        
        TimerMixin.setTimeout(
            () => this.refs.observationInput !== undefined && this.refs.observationInput.focus(), 
            400
        );        
    }

    render() {
        const { observation, addObservation } = this.props;

        return (
            <View style={{ flex: 1 }}>
                <Toolbar 
                    title="Observaciones" 
                    back 
                    isModal 
                />
                <Paper 
                    style={{ 
                        flex: 1,
                        padding: 8
                    }}
                >
                    <TextField
                        ref="observationInput"
                        fontSize={18}
                        label="Observaciones"
                        value={observation}
                        animationDuration={150} 
                        multiline
                        onChangeText={(text) => addObservation(text)}                
                    />
                </Paper>    
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        observation: store.Cart.observation,
    };
};

const mapDispatchToProps = (dispatch) => {
  return {
        addObservation: (observation) => {
            dispatch(addObservation(observation));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ObservationPanel);
