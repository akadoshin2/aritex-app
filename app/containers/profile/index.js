'use strict';

import React, { Component } from 'react';
import { View, Alert, Image, ListView, TouchableOpacity } from 'react-native';
import { Subheading } from 'carbon-ui';
// import { TextField } from 'react-native-material-textfield';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
// import { Actions } from 'react-native-router-flux';
// import TimerMixin from 'react-timer-mixin';
// import DismissKeyboard from 'dismissKeyboard';

import Toolbar from './../../components/Toolbar';
import Constants from './../../Constants';
import { openModal } from '../../reducers/Modal/actions';
import { updateUserProfile } from '../../reducers/User/actions';

var RNFS = require('react-native-fs');

class Profile extends Component {
    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            password: '',
            newPassword: '',
            newImage: {},            
            dataSource: ds.cloneWithRows([
                {
                    iconLeft: 'md-contact',
                    iconRight: '',
                    data: Constants.Formatter.toTitleCase(`${this.props.name}`),
                    onPress: () => {}
                },
                {
                    iconLeft: 'md-mail',
                    iconRight: '',
                    data: this.props.mail,
                    onPress: () => {}
                },
                {
                    iconLeft: 'md-lock',
                    iconRight: 'md-create',
                    data: '******',
                    onPress: () => this.props.openModal('change-password-panel')
                },
            ])
        };                
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.password !== this.state.password ||
            nextProps.newPassword !== this.state.newPassword) {
            this.setState({
                password: nextProps.password,
                newPassword: nextProps.newPassword
            });  
        }               
    }

    handleImage() {
        ImagePicker.openPicker({
            cropping: true,
            width: 150,
            height: 150
        }).then(newImage => {            
            this.setState({ 
                newImage
            });
        });
    }

    renderUserPicture() {
        const { image } = this.props;
        const { newImage } = this.state;

        return (
            <View
                style={{                    
                    paddingVertical: 60,
                    alignItems: 'center'
                }}
            >
                <View>
                    <Image                                              
                        style={{ 
                            height: 150, 
                            width: 150,
                            borderWidth: 0.5,
                            borderColor: 'black',
                            borderRadius: 75
                        }} 
                        source={
                            Object.keys(newImage).length > 0 ?                                
                                { uri: newImage.path } :
                                { uri: image }
                        }
                    />
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => this.handleImage()}
                        style={{
                            height: 50, 
                            width: 50,
                            borderWidth: 0.5,
                            borderColor: '#0D47A1',
                            borderRadius: 25,
                            position: 'absolute',
                            right: 0,
                            bottom: 0,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#0D47A1'
                        }}
                    >
                        <Icon 
                            name="md-create" 
                            size={35}
                            color="white"                            
                        />
                    </TouchableOpacity>
                </View>
                
            </View>
        );
    }

    renderUserData() {
        const { dataSource } = this.state;

        const renderRow = (item) => {
            return (
                <TouchableOpacity
                    style={{ 
                        flex: 1,
                        height: 50,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        borderBottomWidth: 0.5,
                        borderColor: 'black',
                        paddingHorizontal: 15
                    }}
                    onPress={() => item.onPress()}
                >
                    <View 
                        style={{                            
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 5
                        }}
                    >      
                        <View 
                            style={{ 
                                width: 30,
                                alignItems: 'center'
                            }}
                        >                  
                            <Icon 
                                name={item.iconLeft} 
                                size={30}
                                color="black"
                            />
                        </View>
                        <Subheading style={{ marginLeft: 15 }}>
                            {item.data}
                        </Subheading>
                    </View>
                    <View 
                        style={{ 
                            width: 30,
                            alignItems: 'center'
                        }}
                    >
                        {
                            item.iconRight !== '' ?                        
                            <Icon 
                                name={item.iconRight} 
                                size={30}
                                color="black"
                            /> :
                            <View />
                        }
                    </View>
                </TouchableOpacity>
            );
        };

        return (
            <ListView
                dataSource={dataSource}
                renderRow={renderRow}
            />
        );
    }

    renderContent() {
        return (
            <View style={{ flex: 1 }}>
                {this.renderUserPicture()}
                {this.renderUserData()}
            </View>
        );
    }

    render() {
        const { updateUserProfile } = this.props;
        const { password, newPassword, newImage } = this.state;

        const prepareImage = () => {
            if (Object.keys(newImage).length > 0) {                
                RNFS.readFile(newImage.path, 'base64')
                .then((file) => {
                    updateUserProfile(
                        password,
                        newPassword,
                        file                        
                    );
                });                
            } else {
                updateUserProfile(
                    password,
                    newPassword,
                    newImage                    
                );
            }
        };

        const confirmSubmit = () => {
            Alert.alert(
                'Confirmar actualización de datos',
                'Presione OK para actualizar sus datos',
                [            
                    {
                        text: 'CANCEL', 
                        onPress: () => {}, 
                        style: 'cancel'
                    },
                    {
                        text: 'OK', 
                        onPress: () => prepareImage(),
                    },
                ],
                { cancelable: true }
            );
        };

        return (
            <View style={{ flex: 1 }}>           
                <Toolbar 
                    {...this.props} 
                    check={
                        Object.keys(newImage).length > 0 || newPassword !== '' ?
                            true :
                            false
                    }
                    rightCallback={() => confirmSubmit()}
                />                                       
                {this.renderContent()}
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    const { user, userPassword } = store.User;
    const { name, seller_code } = user.seller;
    const { password, newPassword } = userPassword;

    return {
        name,
        mail: user.email,
        //mail: seller_code,
        image: user.profile,
        password,
        newPassword        
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        openModal: (modal) => {
            dispatch(openModal(modal));
        },
        updateUserProfile: (password, newPassword, image) => {
            dispatch(updateUserProfile(password, newPassword, image));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
