'use strict';

import React, { PureComponent } from 'react';
import { Animated, Keyboard } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
import { connect } from 'react-redux';

import Cart from '../cart';
import Orders from '../orders';
import Toolbar from './../../components/Toolbar';
import BottomBar from '../../components/BottomBar';
import LogoSpinner from './../../components/LogoSpinner';
import { updateData } from '../../reducers/Updater/actions';
import { dispatchQueuedActions } from '../../reducers/ActionQueue/actions';

class MyOrders extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            index: 0,
            routes: [
                { key: '1', title: 'Carrito' },
                { key: '2', title: 'Ordenes' },
            ], 
            navbarHeight: new Animated.Value(110),
            bottombarHeight: new Animated.Value(56)
        };        
    }

    componentWillMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow', 
            this._keyboardDidShow.bind(this)
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide', 
            this._keyboardDidHide.bind(this)
        );

        const { updateData, dispatchQueuedActions } = this.props;

        updateData();
        dispatchQueuedActions();
    }

    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }    

    setNavbarAnimation(isKeyboardHide) {
        const { navbarHeight } = this.state;

        Animated.timing(navbarHeight, {
            duration: 1,
            toValue: !isKeyboardHide ? 0 : 110
        }).start();
    }

    setBottombarAnimation(isKeyboardHide) {
        const { bottombarHeight } = this.state;

        Animated.timing(bottombarHeight, {
            duration: 1,
            toValue: !isKeyboardHide ? 0 : 56
        }).start();
    }

    _keyboardDidShow() {
        this.setNavbarAnimation(false);
        this.setBottombarAnimation(false);
    }

    _keyboardDidHide() {
        this.setNavbarAnimation(true);
        this.setBottombarAnimation(true);
    }

    renderScene() {
        const FirstRoute = () => <Cart />;
        const SecondRoute = () => <Orders />;

        return SceneMap({
            1: FirstRoute,
            2: SecondRoute,
        });
    }

    render() {
        const { isLoading } = this.props;
        
        const handleChangeTab = index => this.setState({ index });

        const renderHeader = props => {
            const { navbarHeight } = this.state;

            return (
                <Animated.View 
                    style={{ 
                        backgroundColor: '#0D47A1', 
                        elevation: 5,
                        height: navbarHeight
                    }}
                >
                    <Toolbar {...this.props} />                
                    <TabBar 
                        style={{ 
                            backgroundColor: '#0D47A1', 
                            elevation: 0 
                        }} 
                        indicatorStyle={{ backgroundColor: '#63a4ff' }}
                        {...props} 
                    />                
                </Animated.View>
            );
        };

        const renderFooter = () => {
            const { bottombarHeight } = this.state;

            return (
                <Animated.View 
                    style={{ height: bottombarHeight }}
                >
                    <BottomBar {...this.props} />
                </Animated.View>
            );
        };

        if (isLoading) return <LogoSpinner fullStretch />;
        return (
            <TabViewAnimated
                style={{ flex: 1 }}
                navigationState={this.state}
                renderScene={this.renderScene()}
                renderHeader={(props) => renderHeader(props)}
                renderFooter={() => renderFooter()}
                onRequestChangeTab={(index) => handleChangeTab(index)}
            />
        );
    }
}

const mapStateToProps = (store) => {
    return {
        isLoading: store.Updater.isLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateData: () => {
            dispatch(updateData());
        },
        dispatchQueuedActions: () => {
            dispatch(dispatchQueuedActions());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);
