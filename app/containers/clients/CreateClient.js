import React, { Component } from 'react';
import {
    Text,
    View,
    Image,
    Modal, 
    Alert,
    Picker,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import { RaisedButton, FlatButton } from 'carbon-ui';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { Field, reduxForm, change, initialize } from 'redux-form';
import ImagePicker from 'react-native-image-crop-picker';
import { TextField } from 'react-native-material-textfield';
import { Actions } from 'react-native-router-flux';

import { bindActionCreators } from 'redux';

import Constants from './../../Constants';
import Toolbar from './../../components/Toolbar';
import LogoSpinner from '../../components/LogoSpinner';
import APITexWorker from '../../services/APITexWorker';
import  {Dimensions} from 'react-native';
let ScreenHeight = Dimensions.get("window").height;

var RNFS = require('react-native-fs');

const renderTextField = ({ 
    input,
    label, 
    index,
    scrollRef,
    icon, 
    secondStep,
    prefix,
    autoCorrect = false,
    keyboardType = 'default',
    meta: { touched, error }, 
    ...custom 
}) => {
    const height = Constants.Dimension.ScreenHeight(0.15);

    const onFocusCallback = () => {
        scrollRef.scrollTo({
            y: index * height, 
            x: 0,
            animated: true 
        });
    };

    const onBlurCallback = (e) => {
        scrollRef.scrollTo({
            y: (index - 1) * height, 
            x: 0,
            animated: true 
        });
        input.onBlur(e);
    };

    return (   
        <TextField
            fontSize={14}
            label={label}
            value={input.value}
            prefix={prefix} 
            autoCorrect={autoCorrect}
            keyboardType={keyboardType}
            animationDuration={150}
            error={touched && error ? error : null}
            onChangeText={(value) => input.onChange(value)}
            onFocus={() => onFocusCallback()}
            onBlur={(e) => onBlurCallback(e)}
            {...custom}      
        />
    );
};

const validate = values => {
    const errors = {};
    const requiredFields = [
        'name',
        'email',
        'cc',
        'dv',
        'act_code',
        'address',
        'city',
        'phone',
        'mobile',
        'treasure_dept',
        'assigned',
        'flete'
    ];

    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Este campo es requerido';
        }
    });

    return errors;
};

class CreateClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            images: [],
            merchs: [],
            showCR: true,
            time: 0,
            user_type: 1,
            isLoading: false,
            inputFocus: false
        };
    }

    componentWillMount = () => {
        this.props.change('createClientForm', 'user_type', 1);
        this.props.change('createClientForm', 'time', 0);
    }

    handleSelectImage = () => {
        ImagePicker.openPicker({
            multiple: true
        }).then(images => {
            let imgs = this.state.images;
            images.map(image => {
                return imgs.push({ uri: image.path }); // OJO return
            });
            this.setState({ images: imgs });
        });
    }

    handleSelectMerch = () => {
        ImagePicker.openPicker({
            multiple: true
        }).then(images => {
            let imgs = this.state.merchs;
            images.map(image => {
                return imgs.push({ uri: image.path });  // OJO return
            });
            this.setState({ merch: imgs });
        });
    }

    handleImageCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            let images = this.state.images;
            images.push({ uri: image.path });
            this.setState({ images });
        });
    }

    handleMerchCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            let images = this.state.merchs;
            images.push({ uri: image.path });
            this.setState({ merchs: images });
        });
    }  

    uploadClient = (data) => {
        const self = this;
        const { auth_token } = this.props;

        const callback = () => {
            self.setState({ isLoading: false });
            // Alert.alert(
            //     'Cliente creado con exito',
            //     'Ahora puedes asociar el cliente a cualquier pedido'
            // );
            Actions.pop();
        };

        const errorCallback = () => {
            Alert.alert(
                'Rayos',
                'Ha habido un problema creando el cliente, por favor vuelvelo a intentar'
            );
            Actions.pop();
        };

        this.setState({ isLoading: true });
        APITexWorker.createClient(auth_token, data, callback, errorCallback);
    }

    handleCreateClient = (values) => {
        let ccs = [];
        let merchs = [];

        
        const input = {
            ccs,
            merchs,
            ...values
        };
        this.uploadClient(input);

        // if (this.state.images.length > 0) {
        //     this.state.images.map((image, i) => {
        //         RNFS.readFile(image.uri, 'base64')
        //         .then((file) => {
        //             ccs.push(file);
        //             if (i == this.state.images.length - 1) {
        //                 if (this.state.showCR) {
        //                     if (this.state.merchs.length > 0) {
        //                         this.state.merchs.map((image, j) => {
        //                             RNFS.readFile(image.uri, 'base64')
        //                             .then(file => {
        //                                 merchs.push(file);
        //                                 if (j == this.state.merchs.length - 1) {
        //                                     const input = {
        //                                         ccs,
        //                                         merchs,
        //                                         ...values
        //                                     };
        //                                     this.uploadClient(input);
        //                                 }
        //                             });
        //                         });
        //                     } else {
        //                         Alert.alert(
        //                             'Rayos',
        //                             'Debes adjuntar al menos una foto de tu registro mercantil'
        //                         );
        //                     }
        //                 } else {
        //                     const input = {
        //                         ccs,
        //                         merchs,
        //                         ...values
        //                     };
        //                     this.uploadClient(input);
        //                 }
        //             }
        //         });
        //     });
        // } else {
        //     Alert.alert(
        //         'Rayos',
        //         'Debes adjuntar al menos una foto de tu CC o NIT'
        //     );
        // }
    } 

    renderCCImages() {
        const deleteImage = (imageIndex) => {
            this.setState({
                images: this.state.images.filter((image, index) => {
                    return index !== imageIndex;
                })
            });
        };

        if (this.state.images.length > 0) {
            return (
                <View 
                    style={{ 
                        height: 200,
                        marginVertical: 15
                    }}
                >
                    <ScrollView horizontal>
                        {
                            this.state.images.map((image, i) => {
                                return (
                                    <View key={i}>
                                        <Image                                              
                                            style={{ 
                                                height: 150, 
                                                width: 150,
                                                borderWidth: 0.5,
                                                borderColor: 'black',
                                                marginHorizontal: 5
                                            }} 
                                            source={image} 
                                        />
                                        <TouchableOpacity 
                                            onPress={() => deleteImage(i)}
                                            style={{
                                                position: 'absolute',
                                                top: 0,
                                                right: 15

                                            }}
                                        >
                                            <Icon 
                                                name="md-close"
                                                size={25}
                                                color="black"
                                            />
                                        </TouchableOpacity>
                                    </View>
                                );
                            })
                        }                    
                    </ScrollView>
                </View>
            );
        }
        return <View style={{ marginBottom: 20 }} />;
    }    

    renderMercantilImages() {
        const deleteMerch = (imageIndex) => {
            this.setState({
                merchs: this.state.merchs.filter((image, index) => {
                    return index !== imageIndex;
                })
            });
        };

        if (this.state.merchs.length > 0) {
            return (
                <View 
                    style={{ 
                        height: 150,
                        marginVertical: 15
                    }}
                >
                    <ScrollView horizontal>
                        {
                            this.state.merchs.map((image, i) => {
                                return (
                                    <View key={i}>
                                        <Image                                              
                                            style={{ 
                                                height: 150, 
                                                width: 150,
                                                borderWidth: 0.5,
                                                borderColor: 'black',
                                                marginHorizontal: 5
                                            }} 
                                            source={image} 
                                        />
                                        <TouchableOpacity 
                                            onPress={() => deleteMerch(i)}
                                            style={{
                                                position: 'absolute',
                                                top: 0,
                                                right: 15

                                            }}
                                        >
                                            <Icon 
                                                name="md-close"
                                                size={25}
                                                color="black"
                                            />
                                        </TouchableOpacity>
                                    </View>
                                );
                            })
                        }                    
                    </ScrollView>
                </View>
            );
        }
        return <View style={{ marginBottom: 20 }} />;
    } 

    renderModal() {
        return (
            <Modal
                animationType="slide"
                transparent
                visible={this.state.isLoading}
                onRequestClose={() => {}}
            >
                <View
                    style={{
                        flex: 1, 
                        justifyContent: 'center', 
                        alignItems: 'center', 
                        backgroundColor: 'white'
                    }}
                >
                    <LogoSpinner fullStretch />
                </View>
            </Modal>
        );
    }

    renderSwiper() {
        const renderPrevButton = () => {
            if (this.state.index > 0) {
                return (
                    <FlatButton 
                        onPress={() => { 
                            this.setState({ index: this.state.index - 1 }); 
                            this._swiper.scrollBy(-1); 
                        }}
                    >
                        <View 
                            style={{ 
                                flexDirection: 'row', 
                                justifyContent: 'flex-start', 
                                alignItems: 'center'
                            }}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={25}
                                color="#6F6F6F"
                            />
                            <Text 
                                style={{ 
                                    color: 'black', 
                                    fontSize: 12, 
                                    marginLeft: 10 
                                }}
                            >
                                ATRAS
                            </Text>
                        </View>
                    </FlatButton>
                );
            }
        };

        const renderNextButton = () => {
            return (
                <FlatButton 
                    onPress={() => { 
                        this.setState({ index: this.state.index + 1 }); 
                        this._swiper.scrollBy(1); 
                    }}
                >
                    <View 
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'center'
                        }}
                    >
                        <Text 
                            style={{ 
                                color: 'black', 
                                fontSize: 12, 
                                marginRight: 10 
                            }}
                        >
                            SIGUIENTE
                        </Text>
                        <Icon
                            name="ios-arrow-forward"
                            size={25}
                            color="#6F6F6F"
                        />
                    </View>
                </FlatButton>
            );
        };

        const renderFirstView = () => {
            return (
                <ScrollView ref="scrollViewFirstView" style={{
                    flex: 1}}>
                    <View 
                        style={{ 
                            backgroundColor: 'white', 
                            height: 900,
                            paddingTop: 10,
                            paddingHorizontal: 15,
                        }} 
                    >                    
                        <Text 
                            style={{ 
                                color: '#1976D2', 
                                fontWeight: 'bold'
                            }}
                        >
                            INFORMACIÓN PRINCIPAL
                        </Text>                    
                        <Text 
                            style={{ 
                                color: 'black',
                                marginTop: 10,
                            }}
                        >
                            Tipo de cliente
                        </Text>
                        <Field
                            name="user_type"
                            component={({
                                input, 
                                label, 
                                icon, 
                                secondStep, 
                                meta: { touched, error }, 
                                ...custom
                            }) => 
                                <Picker
                                    selectedValue={input.value}
                                    onValueChange={(itemValue) => {
                                        this.setState({ user_type: itemValue });
                                        if (itemValue === 2) {
                                            this.setState({ showCR: true, user_type: 2 });
                                        } else {
                                            this.setState({ showCR: false, user_type: 1 });
                                        }
                                        input.onChange(itemValue);
                                    }}
                                >
                                    <Picker.Item label="Régimen Simplificado" value={1} />
                                    <Picker.Item label="Régimen Común" value={2} />
                                </Picker>                                        
                            }
                        />
                        <Field
                            secondStep
                            name="name"
                            component={renderTextField}
                            label="Nombre o Razón Social"
                            icon="ios-mail"
                            index={0}
                            scrollRef={this.refs.scrollViewFirstView}
                        />
                        <Field
                            secondStep
                            name="email"
                            component={renderTextField}
                            label="Correo electrónico"
                            icon="ios-mail"
                            index={1}
                            scrollRef={this.refs.scrollViewFirstView}
                            keyboardType="email-address"
                        />
                       
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 2, }}>
                                <Field
                                    name="cc"
                                    component={renderTextField}
                                    label="CC o NIT"
                                    index={2}
                                    scrollRef={this.refs.scrollViewFirstView}
                                    keyboardType='numeric'
                                />
                            </View>
                            <View style={{ flex: 1, marginLeft: 15 }}>
                                <Field
                                    name="dv"
                                    component={renderTextField}
                                    label="DV"
                                    index={2}
                                    scrollRef={this.refs.scrollViewFirstView}
                                    keyboardType='numeric'
                                />
                            </View>
                        </View>
                        
                        <Field
                            name="act_code"
                            component={renderTextField}
                            label="Código de Actividad"
                            index={3}
                            scrollRef={this.refs.scrollViewFirstView}
                            keyboardType='numeric'
                        />
                        <Field
                            name="address"
                            component={renderTextField}
                            label="Domicilio Principal"
                            index={4}
                            scrollRef={this.refs.scrollViewFirstView}
                        />
                        <Field
                            name="city"
                            component={renderTextField}
                            label="Ciudad"
                            index={5}
                            scrollRef={this.refs.scrollViewFirstView}
                        />                        
                    </View>
                </ScrollView>
            );
        };

        const renderSecondView = () => {
            return (
                <ScrollView ref="scrollViewSecondView">
                    <View 
                        style={{ 
                            backgroundColor: 'white',
                            height: Constants.Dimension.ScreenHeight(1),
                            paddingTop: 10,
                            paddingHorizontal: 15
                        }}
                    >   
                        <Field
                            name="phone"
                            component={renderTextField}
                            label="Teléfono Fijo"
                            index={0}
                            scrollRef={this.refs.scrollViewFirstView}
                        />                
                        <Field
                            name="mobile"
                            component={renderTextField}
                            label="Teléfono Movil"
                            index={0}
                            scrollRef={this.refs.scrollViewSecondView}
                        />
                        <Field
                            name="treasure_dept"
                            component={renderTextField}
                            label="Tesorerí­a"
                            index={1}
                            scrollRef={this.refs.scrollViewSecondView}
                        />  
                        <Text 
                            style={{ 
                                color: '#1976D2', 
                                fontWeight: 'bold',
                                marginTop: 10 
                            }}
                        >
                            CONDICIONES DE VENTA
                        </Text>                    
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 2 }}>
                                <Field
                                    name="assigned"
                                    component={renderTextField}
                                    label="Cupo asignado"
                                    keyboardType='numeric'
                                    index={2}
                                    scrollRef={this.refs.scrollViewSecondView}
                                    prefix="$"
                                />
                            </View>
                            <View style={{ flex: 1, marginLeft: 15 }}>
                                <Field
                                    name="flete"
                                    component={renderTextField}
                                    label="Flete"
                                    keyboardType='numeric'
                                    index={2}
                                    scrollRef={this.refs.scrollViewSecondView}
                                    prefix="%"
                                />
                            </View>
                        </View>                        
                        <Text style={{ color: 'black', marginTop: 24 }}>
                            Plazo de pago
                        </Text>
                        <Field
                            name="time"
                            component={({
                                    input, 
                                    label, 
                                    icon, 
                                    secondStep, 
                                    meta: { touched, error }, 
                                    ...custom
                                }) => 
                                    <Picker
                                        selectedValue={input.value}
                                        onValueChange={(itemValue) => input.onChange(itemValue)}
                                    >
                                        <Picker.Item label="Contado" value={0} />
                                        <Picker.Item label="30 dias" value={30} />
                                        <Picker.Item label="45 dias" value={45} />
                                        <Picker.Item label="60 dias" value={60} />
                                    </Picker>                                
                                }
                        />
                    </View>
                </ScrollView>
            );
        };

        const renderThirdView = () => {
            return (
                <ScrollView>
                    <View 
                        style={{ 
                            backgroundColor: 'white', 
                            //flex: 1,
                            height: Constants.Dimension.ScreenHeight(1),
                            paddingTop: 10,
                            paddingHorizontal: 15
                        }}
                    > 
                        <Text 
                            style={{ 
                                color: '#1976D2', 
                                fontWeight: 'bold' 
                            }}
                        >
                            ADJUNTAR DOCUMENTOS
                        </Text>                        
                        <Text 
                            style={{ 
                                color: 'black',
                                marginTop: 10,
                            }}
                        >
                            Adjuntar CC o NIT
                        </Text>
                        
                        {this.renderCCImages()}

                        <View 
                            style={{ 
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                                alignItems: 'center'                            
                            }}
                        >
                            <RaisedButton 
                                onPress={this.handleImageCamera}
                                style={{ backgroundColor: '#1976D2' }}
                            >
                                CAMARA
                            </RaisedButton>
                            <RaisedButton 
                                onPress={this.handleSelectImage}
                                style={{ backgroundColor: '#1976D2' }}
                            >
                                GALERIA
                            </RaisedButton>
                        </View>                    
                        {
                            this.state.showCR ? (
                                <View>
                                    <Text 
                                        style={{ 
                                            color: 'black',
                                            marginTop: 10,
                                        }}
                                    >
                                        Adjuntar Registro Mercantil
                                    </Text>                                        
                                        
                                    {this.renderMercantilImages()}

                                    <View 
                                        style={{ 
                                            flexDirection: 'row',
                                            justifyContent: 'space-around',
                                            alignItems: 'center'                            
                                        }}
                                    >
                                        <RaisedButton 
                                            onPress={this.handleMerchCamera}
                                            style={{ backgroundColor: '#1976D2' }}
                                        >
                                            CAMARA
                                        </RaisedButton>
                                        <RaisedButton 
                                            onPress={this.handleSelectMerch}
                                            style={{ backgroundColor: '#1976D2' }}
                                        >
                                            GALERIA
                                        </RaisedButton>
                                    </View>
                                </View>
                            ) : undefined
                        }
                    </View>
                </ScrollView>
            );
        };

        return (
            <View style={{height: ScreenHeight}}>
                <Swiper      
                    width={'100%'}       
                    ref={(swiper) => { this._swiper = swiper; }}
                    loop={false}
                    index={this.state.index}
                    showsButtons
                    scrollEnabled={false}
                    showsPagination
                    buttonWrapperStyle={{
                        position: 'relative',
                        flex: null,
                        height: 135,
                        alignItems: 'flex-start'
                    }}
                    dotStyle={{
                        marginBottom: 60,
                        marginLeft: 3,
                        marginRight: 3
                    }}
                    activeDotStyle={{
                        marginBottom: 60,
                        marginLeft: 3,
                        marginRight: 3
                    }}
                    nextButton={renderNextButton()}
                    prevButton={renderPrevButton()}
                    paginationStyle={{ paddingBottom: 15 }}
                >
                    {renderFirstView()}
                    {renderSecondView()}
                    {/* {renderThirdView()}   */}
                </Swiper>
            </View>
        );
    }

    render() {
        const { handleSubmit } = this.props;

        const confirmSubmit = () => {
            Alert.alert(
                'Confirmar creación de Cliente',
                'Presione OK para enviar la solicitud y crear un nuevo Cliente',
                [            
                    {
                        text: 'CANCEL', 
                        onPress: () => {}, 
                        style: 'cancel'
                    },
                    {
                        text: 'OK', 
                        onPress: handleSubmit(this.handleCreateClient)
                    },
                ],
                { cancelable: true }
            );
        };

        return (
            <View 
                style={{ backgroundColor: '#EEEEEE', }}
            >
                {this.renderModal()}
                <Toolbar 
                    {...this.props}        
                    check={
                        this.state.index === 1 ? 
                            true : 
                            false
                    }
                    rightCallback={
                        this.state.index === 1 ?
                            () => confirmSubmit() :
                            undefined
                    }
                />
                {this.renderSwiper()}
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ change, initialize }, dispatch);
};

const mapStateToProps = (store) => {
    return {
        auth_token: store.User.auth_token
    };
};

const withRedux = connect(mapStateToProps, mapDispatchToProps)(CreateClient);

export default reduxForm({
    form: 'createClientForm',
    validate
})(withRedux);
