'use strict';

import React, { Component } from 'react';
import { 
    View,
    TextInput, 
    TouchableOpacity 
} from 'react-native';
import {
    List, 
    Body1,
    Paper,
    Caption,
    Divider,
    ListItem    
} from 'carbon-ui';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import TimerMixin from 'react-timer-mixin';

import Constants from './../../Constants';
import Toolbar from './../../components/Toolbar';

class SelectClientPanel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            query: '',
            data: this.props.clients
        };       
        console.log("clients", this.props.clients);       
    }

    componentDidMount() {        
        TimerMixin.setTimeout(
            () => this.refs.textInput !== undefined && this.refs.textInput.focus(), 
            400
        );        
    }

    renderSearchBar() {
        const searchClient = () => {
            const { clients } = this.props;
            const { query } = this.state;

            const data = clients.filter((client) => {
                return (client.name.toLowerCase().indexOf(query.toLowerCase()) !== -1 ||
                        client.client_id.toString().indexOf(query) !== -1
                );
            });

            this.setState({ data });
        };

        return (
            <Paper
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    height: 50
                }}
            >
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',                     
                        width: Constants.Dimension.ScreenWidth(0.8),
                    }}
                >
                    <View 
                        style={{
                            justifyContent: 'center',
                            padding: 10,
                            marginHorizontal: 10,
                            width: 40
                        }}
                    >                        
                        <Icon 
                            name="md-search" 
                            size={25}
                        />                        
                    </View>
                    <TextInput
                        ref="textInput"
                        placeholder="Buscar por código o nombre"
                        value={this.state.query}
                        onChangeText={(query) => this.setState({ query })}  
                        underlineColorAndroid='transparent'  
                        onSubmitEditing={() => searchClient()}            
                        style={{
                            flex: 1,
                            fontSize: 16
                        }}
                    />
                </View>               
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-end',              
                        width: Constants.Dimension.ScreenWidth(0.2),
                    }}
                >                    
                    <TouchableOpacity
                        onPress={() => this.setState({ 
                            query: '',
                            data: this.props.clients
                        })}
                        style={{
                            justifyContent: 'center',
                            padding: 10,
                            marginHorizontal: 10
                        }}       
                    >
                        <Icon 
                            name="md-close"
                            size={25}
                        />
                    </TouchableOpacity>
                </View>
            </Paper>
        );
    }

    renderClientList() {
        const renderTimeStamp = (length, userId) => {
            return (
                <Paper
                    key="timestamp"
                    elevation={0}
                    style={{ 
                        alignItems: 'flex-end',
                        marginBottom: 10,
                        marginRight: 15
                    }}                        
                > 
                    <Caption style={{ color: 'black' }}>
                        {`${length} Clientes - Asesor ${userId}`}
                    </Caption>
                </Paper>
            );
        };

        const renderLetterDivider = (letter) => {
            return (
                <Paper 
                    key={`letterdivider${letter}`}
                    elevation={0}
                    style={{ marginLeft: 15, marginBottom: 10 }}                        
                > 
                    <Body1 style={{ color: '#1976D2' }}>
                        {letter}
                    </Body1>
                </Paper>
            );
        };

        const renderClient = (client) => {
            return (
                <ListItem  
                    key={`client${client.client_id}`}
                    // onPress={() => onPressClient(client.client_id, client.client_id)}                               
                    primaryText={Constants.Formatter.toTitleCase(client.name)}
                    secondaryText={`Código: ${client.client_id}\nCali, Colombia`}
                    secondaryTextLines={2}
                    rightIcon={
                        <Icon 
                            name="md-information-circle"
                            size={25}
                            color="gray"
                            style={{ marginTop: 15 }}
                        />
                    }
                    style={{ width: Constants.Dimension.ScreenWidth(0.9) }}
                />
            );
        };

        const renderDivider = (letter) => {
            return (
                <Divider 
                    key={`divider${letter}`}
                    style={{ marginVertical: 10 }} 
                />
            );
        };

        const renderList = () => {
            const { userId } = this.props;
            const { data } = this.state;
            let array = [];

            array = array.concat(renderTimeStamp(data.length, userId));

            data.forEach((client, indexClient) => {
                if (indexClient === 0) {
                    array = array.concat(renderLetterDivider(client.name[0]));
                } else if (client.name[0] !== data[indexClient - 1].name[0]) {
                    array = array.concat(renderLetterDivider(client.name[0])); 
                }

                array = array.concat(renderClient(client)); 
                    
               
                if (indexClient === data.length - 1) {
                    array = array.concat(renderDivider(client.name[0]));
                } else if (client.name[0] !== data[indexClient + 1].name[0]) {
                    array = array.concat(renderDivider(client.name[0]));
                }
            });

            if (array.length > 0) return array;
            return <ListItem primaryText="Cliente no encontrado" />;
        };
       
        if (this.props.clients.length > 0) { 
            return (
                <List>
                    {renderList()}
                </List>
            );
        }

        return (
            <View 
                style={{ 
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Body1>
                    No posee ningún cliente asociado a su cuenta
                </Body1>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Toolbar {...this.props} />
                {this.renderSearchBar()}
                {this.renderClientList()}            
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        userId: store.User.user.seller.seller_code,
        clients: store.User.clients
    };
};

export default connect(mapStateToProps)(SelectClientPanel);
