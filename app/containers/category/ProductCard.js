import React, { Component, PropTypes } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
// import PureRenderMixin from 'react-addons-pure-render-mixin'

import Constants from './../../Constants';
import { ProductViewMode } from '../../reducers/Store/actions';

const { LIST_VIEW } = ProductViewMode;

export default class ProductCard extends Component {
    static propTypes = {
        viewMode: PropTypes.string.isRequired,
        product: PropTypes.object.isRequired,
        onPress: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.styles = {
            container: {
                height: undefined,
                borderColor: Constants.Color.ViewBorder,
                borderWidth: 1,
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
                paddingBottom: 5,
            },
            image: {
                marginBottom: 8,
            },
            price_wrapper: {
                flexDirection: 'row',
            },
            text: {
                color: 'black',
                marginBottom: 8,
            },
            sale_price: {
                textDecorationLine: 'line-through',
                color: Constants.Color.TextLight,
                marginLeft: 0,
                marginRight: 0
            },
            price: {
                fontWeight: '600',
                color: Constants.Color.ProductPrice,
            },
            sale_off: {
                color: Constants.Color.TextLight,
            }
        };
    }

    shouldComponentUpdate(nextProps) {
        return this.props.viewMode !== nextProps.viewMode;
    }

    render() {
        const { viewMode, product, onPress } = this.props;

        const isListMode = viewMode === LIST_VIEW;
        this.styles = Object.assign({}, this.styles, {
            container: isListMode ? {
                    ...this.styles.container,
                    ...Constants.ProductCard.ListMode.container,
                } : {
                    ...this.styles.container,
                    ...Constants.ProductCard.GridMode.container,
                },
            image: isListMode ? {
                    ...this.styles.image,
                    ...Constants.ProductCard.ListMode.image,
                } : {
                    ...this.styles.image,
                    ...Constants.ProductCard.GridMode.image,
                },
            text: isListMode ? {
                    ...this.styles.text,
                    ...Constants.ProductCard.ListMode.text,
                } : {
                    ...this.styles.text,
                    ...Constants.ProductCard.GridMode.text,
                },
        });

        const productImage = (
            <Image
                source={{ 
                    uri: product.images[0].src
                }}
                style={this.styles.image}
                resizeMode="cover"
            />
        );

        const productReference = (
            <Text
                ellipsizeMode={'tail'}
                numberOfLines={1}
                style={this.styles.text}
            >
                {`Ref. ${product.reference}`}
            </Text>
        );

        const productPrice = (
            <View style={this.styles.price_wrapper}>
                <Text style={[this.styles.text, this.styles.price]}>
                    {Constants.Formatter.currency(product.price)}
                </Text>
            </View>
        );

        return (
            <TouchableOpacity
                style={this.styles.container}
                onPress={() => onPress(product)}
            >
                {productImage}
                {productReference}
                {productPrice}
            </TouchableOpacity>
        );
    }
}
