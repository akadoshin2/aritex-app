'use strict';

import React, { Component } from 'react';
import { Text, View, ListView } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import Constants from './../../Constants';
import Languages from './../../Languages';
import Toolbar from './../../components/Toolbar';
import { 
    selectCategory, 
    selectProduct, 
    toggleProductViewMode 
} from '../../reducers/Store/actions';

import ProductCard from './ProductCard';

class Category extends Component {
    constructor(props) {
        super(props);
        this.styles = {
            container: { flex: 1 },
            baseModalConfig: {
                backButtonClose: true,
                swipeToClose: false,
                backdropOpacity: 0.7,
                style: {
                    height: undefined,
                    width: Constants.Dimension.ScreenWidth(0.9),
                    justifyContent: 'center',
                    alignItems: 'center'
                }
            }
        };
        this.currentCategoryId = this.props.initCategoryId;
        this.currentViewMode = this.props.Store.viewMode;
    }


    componentWillReceiveProps(nextProps) {
        const { Store } = nextProps;
        const { viewMode } = Store;

        if (this.currentViewMode !== viewMode) {
            this.currentViewMode = viewMode;
            if (this.refs._listView !== undefined) {
                this.refs._listView.scrollTo({ x: 5, y: 5, animated: true });//Scroll to top when view mode change
            }
        }
    }

    renderProducts(data) {
        const dataSource = new ListView.DataSource({ rowHasChanged: () => true });

        const goToProduct = (product) => {
            this.props.selectCategory(product.categories[0]);
            this.props.selectProduct(product);
            Actions.product({ product, addProduct: false }); 
        };

        const renderRow = (product) => (
            <ProductCard
                onPress={goToProduct}
                viewMode={this.props.Store.viewMode}
                product={product}
            />
        );

        const renderFooter = () => {
            return (
                <View
                    style={{
                        height: 60,
                        width: Constants.Dimension.ScreenWidth(),
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Text
                        style={{
                            color: Constants.Color.TextDark,
                            fontSize: 16
                        }}
                    >
                        {Languages.ThereIsNoMore}
                    </Text>
                </View>
            );
        };

        return (
            <ListView
                ref="_listView"
                contentContainerStyle={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    alignItems: 'flex-start'
                }}
                dataSource={dataSource.cloneWithRows(data)}
                renderRow={renderRow}                
                renderFooter={renderFooter}
                enableEmptySections                
            />
        );
    }

    render() {
        const {
            title,
            initCategoryId,
            Store,
        } = this.props;
        const { selectedCategoryId, categories, viewMode } = Store;
        const titleWithCategory = title + (selectedCategoryId === initCategoryId
                ? '' : ` - ${categories.find((category) => category.id === selectedCategoryId).name}`); // EX: Men - Shoes

        return (
            <View style={this.styles.container}>
                <Toolbar {...this.props} title={titleWithCategory} />              
                {this.renderProducts(categories.find((category) => category.id === selectedCategoryId).products)}
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        Store: store.Store,
        is_client: store.User.is_client
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleProductViewMode: () => {
            dispatch(toggleProductViewMode());
        },
        selectCategory: (selectedCategoryId) => {
            dispatch(selectCategory(selectedCategoryId));
        },
        selectProduct: (selectedProduct) => {
            dispatch(selectProduct(selectedProduct));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Category);
