'use strict';

import React, { Component } from 'react';
import { View, ListView } from 'react-native';
import { connect } from 'react-redux';

import Constants from './../../Constants';
import Toolbar from './../../components/Toolbar';
import CarouselByCategory from './../../components/CarouselByCategory';
import LogoSpinner from './../../components/LogoSpinner';
import BottomBar from '../../components/BottomBar';
import { updateData } from '../../reducers/Updater/actions';
import { dispatchQueuedActions } from '../../reducers/ActionQueue/actions';

class Catalog extends Component {
    constructor(props) {
        super(props);        

        this.state = {};

        this.styles = {
            container: { flex: 1 },
            imageCard: {
                width: Constants.Dimension.ScreenWidth(1),
                height: 200,
            },
            mainCategoryText: {
                color: 'white',
                fontSize: 30
            },
            numberOfProductsText: {
                color: 'white',
                fontSize: 15
            },
            hideComponent: {
                position: 'absolute', 
                left: Constants.Dimension.ScreenWidth(1)  
            }
        };
    }

    componentWillMount() {
        const { updateData, dispatchQueuedActions } = this.props;

        updateData();
        dispatchQueuedActions();
    }

    renderCategories() {
        const { categories } = this.props.Store;    
        const dataSource = new ListView.DataSource({ rowHasChanged: () => true });

        const renderRow = (category) => {
            return (
                <CarouselByCategory
                    categoryId={category.id}                    
                    categoryName={category.name}
                    products={category.products}
                />
            );
        };

        return (
            <View style={{ flex: 1 }}>
                <ListView
                    dataSource={dataSource.cloneWithRows(categories)}
                    renderRow={renderRow}
                    enableEmptySections
                />
            </View>
        );
    }

    renderContent() {
        const { isLoading } = this.props;

        if (isLoading) return <LogoSpinner fullStretch />;
        return this.renderCategories();
    }

    render() {
        return (
            <View style={this.styles.container}>
                <Toolbar {...this.props} />
                {this.renderContent()}
                <BottomBar {...this.props} />
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        isLoading: store.Updater.isLoading,
        Store: store.Store
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateData: () => {
            dispatch(updateData());
        },
        dispatchQueuedActions: () => {
            dispatch(dispatchQueuedActions());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);
