'use strict';

import React, { Component } from 'react';
import { 
    View,
    ScrollView,
    TouchableOpacity, 
} from 'react-native';
import { Caption, RaisedButton } from 'carbon-ui';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';

import Languages from './../../Languages';
import Carousel from './../../components/CarouselFullWidth';
import APITexWorker from './../../services/APITexWorker';

import { signIn } from '../../reducers/User/actions';
import { openModal } from '../../reducers/Modal/actions';

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // identification: '46',
            // password: 'aritex1234',
            // identification: 'mbolivar@gmail.com',
            // password: '1234',
            identification: '',
            password: '',
            imageCarouselData: [
                { url: require('../../images/2.png') },
                { url: require('../../images/3.png') },
                { url: require('../../images/1.png') }
            ],
            isLoading: true
        };
    }

    componentWillMount() {
        const fetchSliderData = () => {
            const callback = (data) => {
                this.setState({
                    imageCarouselData: data.images,
                    isLoading: false
                });
            };
            APITexWorker.fetchSliderData('login', callback);
        };
        // APITexWorker.onAuthStateChanged(user => {
        //     if (user) {
        //         Actions.main();
        //     }else{
        //         fetchSliderData();
        //     }
        // });
        if (this.props.User.auth_token === '') fetchSliderData();
        else Actions.main();
    }

    renderInputs() {
        return (
            <View
                style={{
                    alignItems: 'center',
                    paddingLeft: 4,
                    paddingRight: 16
                }}
            >
                <View style={{ flexDirection: 'row' }}>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                            paddingBottom: 10,
                            paddingRight: 10
                        }}
                    >
                        <Icon
                            name="md-contact"
                            size={25}
                            color="black"
                        />
                    </View>
                    <View style={{ flex: 11 }}>
                        <TextField
                            ref="identification"
                            fontSize={16}
                            label="Correo electrónico"
                            value={this.state.identification}
                            animationDuration={150} 
                            keyboardType="email-address"
                            onChangeText={(identification) => 
                                this.setState({ identification })
                            }                      
                        />
                    </View>
                </View>
               
                <View style={{ flexDirection: 'row' }}>
                    <View
                        style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                            paddingBottom: 10,
                            paddingRight: 10
                        }}
                    >
                        <Icon
                            name="md-lock"
                            size={25}
                            color="black"
                        />
                    </View>
                    <View style={{ flex: 11 }}>
                        <TextField
                            ref="password"
                            fontSize={16}
                            label={Languages.Password}
                            value={this.state.password}
                            animationDuration={150}
                            secureTextEntry
                            onChangeText={(password) => this.setState({ password })}
                        />
                    </View>
                </View>
            </View>
        );
    }

    renderForgotPasswordButton() {
        return (
            <View
                style={{
                    alignItems: 'flex-end',
                    paddingTop: 15,
                    paddingRight: 10
                }}
            >
                <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {}}
                >
                    <Caption>
                        {Languages.ForgotPassword}
                    </Caption>
                </TouchableOpacity>
            </View>
        );
    }

    renderSignInButton() {
        const { signIn } = this.props;
        const { identification, password } = this.state;

        return (
            <View
                style={{
                    marginTop: 20,
                    justifyContent: 'center',
                    paddingHorizontal: 5,
                }}
            >
                <RaisedButton
                    onPress={() => signIn(identification, password)}
                    style={{ 
                        backgroundColor: '#1976D2',
                        elevation: 0
                    }}
                >
                    {Languages.SignIn}
                </RaisedButton>
            </View>
        );
    }

    renderSignUpButton() {
        const onPressSignUp = () => {
            Actions.signup();
        };

        return (
            <View
                style={{
                    marginTop: 10,
                    justifyContent: 'center',
                    paddingHorizontal: 5,
                }}
            >
                <RaisedButton
                    onPress={() => onPressSignUp()}
                    style={{ 
                        backgroundColor: '#1976D2',
                        elevation: 0
                    }}
                >
                    Registrate
                </RaisedButton>
            </View>
        );
    }

    render() {
        const { imageCarouselData, isLoading } = this.state;
        
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>
                    <Carousel 
                        data={imageCarouselData} 
                        isLocal={isLoading} 
                    />
                </View>  
                <View style={{ flex: 1, backgroundColor: 'white' }}>        
                    {this.renderInputs()}
                    
                    {this.renderSignInButton()}
                    {/* {this.renderSignUpButton()} */}
                </View>
            </ScrollView>
        );
    }
}

function mapStateToProps(store) {
    return {
        User: store.User    
    };
}

function mapDispatchToProps(dispatch) {
    return {
        signIn: (identification, password) => {
            dispatch(signIn(identification, password));
        },
        openModal: (modal) => {
            dispatch(openModal(modal));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
