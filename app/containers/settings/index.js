'use strict';

import React, { Component } from 'react';
import { ListView, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';

import Constants from './../../Constants';
import Languages from './../../Languages';
import Toolbar from './../../components/Toolbar';
import LogoSpinner from './../../components/LogoSpinner';
import BottomBar from '../../components/BottomBar';
import { LanguageModal, AboutModal } from './Modals';

import { signOut } from '../../reducers/User/actions';
import { updateData } from '../../reducers/Updater/actions';
import { dispatchQueuedActions } from '../../reducers/ActionQueue/actions';

class Settings extends Component {
    constructor(props) {
        super(props);
        const open = (modal) => modal && modal.open();
        const settingsArray = !this.props.is_client ? [
            // {
            //     section: Languages.BASICSETTINGS, 
            //     name: Languages.Language, 
            //     onPress: (refs) => open(refs.languageModal)
            // },
            {
                section: Languages.INFO, 
                name: Languages.About, 
                onPress: (refs) => open(refs.AboutModal)
            },
            {
                section: 'CLIENTES', 
                name: 'Crear cliente', 
                onPress: () => Actions.createclient()
            },
            {
                section: 'CLIENTES', 
                name: 'Mis clientes', 
                onPress: () => Actions.listclients()
            },
            {
                section: 'USUARIO', 
                name: 'Perfil', 
                onPress: () => Actions.profile()
            },
            {
                section: 'USUARIO', 
                name: 'Cerrar sesión', 
                onPress: () => { this.props.signOut(); }
            }
        ] : 
        [
            {
                section: Languages.INFO, 
                name: Languages.About, 
                onPress: (refs) => open(refs.AboutModal)
            },
            {
                section: 'USUARIO', 
                name: 'Perfil', 
                onPress: () => Actions.profile()
            },
            {
                section: 'USUARIO', 
                name: 'Cerrar sesión', 
                onPress: () => { this.props.signOut(); }
            }
        ];

        const dataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
            sectionHeaderHasChanged: (s1, s2) => s1 !== s2
        });

        const convertArrayToMap = (array) => {
            let result = {}; // Create the blank map
            array.forEach((item) => {
                if (!result[item.section]) result[item.section] = [];
                result[item.section].push(item);
            });
            return result;
        };

        this.state = {
            dataSource: dataSource.cloneWithRowsAndSections(convertArrayToMap(settingsArray))
        };
    }

    componentWillMount() {
        const { updateData, dispatchQueuedActions } = this.props;

        updateData();
        dispatchQueuedActions();
    }

    renderListView(dataSource) {
        const renderRow = (item) => {
            return (
                <TouchableOpacity onPress={() => item.onPress(this.refs)}>
                    <View 
                        style={{
                            height: 50, 
                            backgroundColor: Constants.Color.Background,
                            justifyContent: 'space-between', 
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}
                    >
                        <Text style={{ marginLeft: 15 }}>
                            {item.name}
                        </Text>
                        <Icon 
                            name={Constants.Icon.Forward} 
                            size={25} 
                            style={{ marginRight: 15 }}
                        />
                    </View>
                </TouchableOpacity>
            );
        };

        const renderSectionHeader = (sectionData, section) => {
            return (
                <Text 
                    style={{
                        padding: 5, 
                        marginLeft: 10, 
                        fontSize: 12
                    }}
                >
                    {section}
                </Text>
            );
        };

        const renderSeparator = (sectionID, rowID) => {
            return (
                <View 
                    key={sectionID + rowID}
                    style={{
                        height: 1, 
                        width: null, 
                        borderTopColor: Constants.Color.DirtyBackground, 
                        borderTopWidth: 1
                    }}
                />
            );
        };

        return (
            <ListView
                dataSource={dataSource}
                renderRow={renderRow}
                renderSectionHeader={renderSectionHeader}
                renderSeparator={renderSeparator}
            />
        );
    }

    render() {
        return (
            <View 
                style={{
                    flex: 1, 
                    backgroundColor: Constants.Color.DirtyBackground
                }}
            >
                <Toolbar {...this.props} />
                {
                    this.props.isLoading ? 
                        <LogoSpinner fullStretch /> :
                        <View style={{ flex: 1 }}>
                            {this.renderListView(this.state.dataSource)}
                        </View>
                }
                <BottomBar {...this.props} />
                {LanguageModal()}
                {AboutModal(this.props.buildVersion)}
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        isLoading: store.Updater.isLoading,
        is_client: store.User.is_client,
        buildVersion: store.User.settings.buildVersion
    };
};

function mapDispatchToProps(dispatch) {
    return {
        updateData: () => {
            dispatch(updateData());
        },
        dispatchQueuedActions: () => {
            dispatch(dispatchQueuedActions());
        },
        signOut: () => {
            dispatch(signOut());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
