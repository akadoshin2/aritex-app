'use strict';

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
    List, 
    Caption,
    Body2,
    Divider,
    ListItem    
} from 'carbon-ui';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import Constants from './../../Constants';

class Orders extends Component {
    constructor(props) {
        super(props);

        this.state = {
            expandedOrders: []
        };
    }

    componentWillMount() {
        const { orders } = this.props;

        const setExpandedOrders = () => {
            const setExpandedProducts = (order) => {
                return order.products.map(() => false);
            };

            return orders.map((order) => {
                return {
                    expanded: false,
                    expandedProducts: setExpandedProducts(order)
                };
            });
        };

        if (orders.length > 0) {
            this.setState({
                expandedOrders: setExpandedOrders() 
            });
        }
    }

    renderOrders() {
        const { is_client, orders } = this.props;
        const { expandedOrders } = this.state;

        const countUnits = (order) => 
            order.products.reduce((total, product) => total + product.quantity, 0);

        const onPressOrder = (orderIndex) => {
            let array = expandedOrders.slice();

            if (array[orderIndex].expanded) array[orderIndex].expanded = false;
            else array[orderIndex].expanded = true;
            this.setState({ expandedOrders: array });
        };

        const onPressProduct = (orderIndex, productIndex) => {
            let array = expandedOrders.slice();
            
            if (array[orderIndex].expandedProducts[productIndex]) array[orderIndex].expandedProducts[productIndex] = false;
            else array[orderIndex].expandedProducts[productIndex] = true;
            this.setState({ expandedOrders: array });
        };

        const renderStatus = (orderIndex, statusID) => {
            const status = [
                'Indefinido',
                'Procesado',
                'Entregado',
                'Cancelado',
                'Por enviar'
            ];

            return (
                <View 
                    style={{
                        flexDirection: 'row',
                        marginTop: 12
                    }}
                >
                    <Body2 
                        style={{ 
                            color: 'gray', 
                            marginRight: 20
                        }}
                    >
                        {status[statusID]}
                    </Body2>
                    <Icon 
                        name={!expandedOrders[orderIndex].expanded ?
                            'ios-arrow-down' :
                            'ios-arrow-up'
                        }
                        size={25}
                        color="gray"
                        style={{ marginTop: 5 }}
                    />
                </View>
            );
        };

        const renderVariations = (orderIndex, productIndex) => {
            return orders[orderIndex].products[productIndex].variations.map((variation, variationIndex) => {
                return (
                    <ListItem     
                        key={`variation${variationIndex}`} 
                        secondaryText={`Color: ${Constants.Formatter.capitalize(variation.color)} - Talla: ${variation.size}\nCantidad: ${variation.quantity}`}
                        secondaryTextLines={2}
                        style={{ paddingLeft: 50, backgroundColor: '#F1F1F1' }}
                    />
                );
            });
        };

        const renderProducts = (orderIndex) => {
            return orders[orderIndex].products.map((product, productIndex) => {
                return (
                    <ListItem     
                        key={`product${productIndex}`}                   
                        onPress={() => onPressProduct(orderIndex, productIndex)}
                        expanded={expandedOrders[orderIndex].expandedProducts[productIndex]}                            
                        primaryText={`Ref. ${product.product.reference}`}
                        secondaryText={`Precio ${Constants.Formatter.currency(product.product.price)} - ${product.quantity} unidad(es)`}
                        style={{ paddingLeft: 30, backgroundColor: '#F1F1F1' }}
                    >
                        {renderVariations(orderIndex, productIndex)}
                    </ListItem>
                );
            });
        };        

        return orders.map((order, orderIndex) => {
            let _data = new Date(order.createdDate);
            return (
                <View
                    key={`order${orderIndex}`}
                >
                    <ListItem                        
                        onPress={() => onPressOrder(orderIndex)}
                        expanded={expandedOrders[orderIndex].expanded}                            
                        primaryText={`Orden #${order._id} - ${_data.getDay()}/${_data.getMonth()}/${_data.getFullYear()}`}
                        secondaryText={`${!is_client ? `${order.client.name}\n` : ''}${order.products.length} item(s), ${countUnits(order)} unidad(es)\n${Constants.Formatter.currency(order.total * 1.19)}`}
                        rightIcon={renderStatus(orderIndex, order.status)}
                        secondaryTextLines={3}
                    >   
                        <Caption style={{ marginLeft: 18 }}>
                            {`Observaciones: \n\n${order.observation}`}
                        </Caption>
                        {renderProducts(orderIndex)}
                    </ListItem>
                    
                    <Divider />
                </View>
            );
        });
    }

    renderError() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}
            >
                <Text>
                    No posee ninguna orden
                </Text>
            </View>
        );
    }

    render() {
        const { orders } = this.props;

        if (orders.length > 0) {
            return (
                <List>
                    {this.renderOrders()}
                </List>
            );
        }
        return this.renderError();
    }
}

const mapStateToProps = (store) => {
    return {
        orders: store.User.orders,
        is_client: store.User.is_client
    };
};

export default connect(mapStateToProps)(Orders);
