import React, { Component } from 'react';
import { 
    View,
    StatusBar 
} from 'react-native';
import FCM, {
    FCMEvent, 
    RemoteNotificationResult, 
    WillPresentNotificationResult, 
    NotificationType
} from 'react-native-fcm';
import { connect } from 'react-redux';
import Modal from 'react-native-modalbox';
import TimerMixin from 'react-timer-mixin';

import SplashScreen from './components/SplashScreen';
import Constants from './Constants';
import AppEventEmitter from './utils/AppEventEmitter';

// Modal scenes
import LogoSpinner from './components/LogoSpinner';
import SearchPanel from './components/SearchPanel';
import VariationsPanel from './components/SelectVariationsPanel'; // PANEL VIEJO
import VariationsGrid from './components/SelectVariationsGrid';
import SelectClientPanel from './containers/cart/SelectClientPanel';
import ObservationPanel from './containers/cart/ObservationPanel';
import ChangePasswordPanel from './components/ChangePasswordPanel';

class Wrapper extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true
        };
    }

    componentWillMount() {
        //Short methods
        this.openModal = () => {
            this.refs.modal.open();
        };
        this.closeModal = () => {
            this.refs.modal.close();
        };
        TimerMixin.setTimeout(
            () => this.setState({ isLoading: false }),
            Constants.SplashScreen.Duration
        );
    }

    componentDidMount() {
        // Register Firebase Cloud Message Topic
        FCM.requestPermissions(); // iOS only
        FCM.getFCMToken().then(token => console.log(`Firebase token: ${token}`));
        FCM.getInitialNotification().then(notif => console.log(notif));
        FCM.subscribeToTopic('/topics/notificaciones');
        this.notificationListener = FCM.on(FCMEvent.Notification, async (notif) => {
            console.lof(notif);
            if (typeof notif.custom_notification !== 'undefined' && 
                notif.custom_notification != null &&
                this.props.auth_token != '') {
                const notify = JSON.parse(notif.custom_notification);
                const { segmentation } = notify;
                const { is_client } = this.props;
                if(segmentation === 1 && is_client){
                    FCM.unsubscribeFromTopic('/topics/asesores');
                    FCM.subscribeToTopic('/topics/clientes');
                }else if(segmentation === 2 && !is_client){
                    FCM.unsubscribeFromTopic('/topics/clientes');
                    FCM.subscribeToTopic('/topics/asesores');
                }
                if (segmentation === 3 ||                   // Todos los usuarios
                    (segmentation === 1 && is_client) ||    // Usuarios finales
                    (segmentation === 2 && !is_client)) {   // Asesores
                    FCM.presentLocalNotification({
                        id: notify.id,
                        title: notify.title,
                        body: notify.body,
                        sound: 'default',
                        priority: 'high',
                        show_in_foreground: true,
                        color: 'blue',
                        big_text: notify.big_text,
                        sub_text: notify.sub_text,
                        ticker: notify.ticker,
                        lights: true
                        //click_action: this.goToAnother()
                    });
                }                
            }
        });

        //Subscribe listeners
        this.modalOpenListener = AppEventEmitter.addListener(
            Constants.EmitCode.OpenModal, 
            this.openModal.bind(this)
        );
        this.modalCloseListener = AppEventEmitter.addListener(
            Constants.EmitCode.CloseModal, 
            this.closeModal.bind(this)
        );
    }

    componentWillUnmount() {
        //Unsubscribe listeners        
        this.modalOpenListener.remove();
        this.modalCloseListener.remove();
    }

    // goToAnother = () => {
    //   Actions.settings();
    // }

    renderModalChildren() {
        const { is_client, settings, modal } = this.props;
        const { abon, abtarget, screenfinal, screenseller, testscreen } = settings;

         const ABTesting = () => {
            if (abon) {
                switch (abtarget) {
                    case 1:
                        if (is_client) return testscreen;
                        return screenseller;
                    case 2:
                        if (is_client) return screenfinal;
                        return testscreen;
                    default:
                        return testscreen;
                }
            } else if (is_client) return screenfinal;
            return screenseller;
        };

        switch (modal) {
            case 'loading-screen':
                return <LogoSpinner fullStretch />;
            case 'search-panel':
                return <SearchPanel />;
            case 'variations-panel':
                // if (ABTesting()) return <VariationsGrid isModal />; 
                if (!is_client) return <VariationsGrid isModal />; 
                return <VariationsPanel isModal />;
            case 'select-client-panel':
                return <SelectClientPanel />;
            case 'observation-panel':
                return <ObservationPanel />;
            case 'change-password-panel':
                return <ChangePasswordPanel />;
            default:
                return <View />;
        }
    }

    render() {
        const { children } = this.props;
        const { isLoading } = this.state;

        //Return splash screen if the app is still loading
        if (isLoading) return <SplashScreen />;        

        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    backgroundColor="#002171"
                    barStyle="light-content"
                />
                {children}
                <Modal
                    ref='modal'
                    backdropPressToClose={false}
                    // backButtonClose
                    swipeToClose={false}
                    entry="top"
                    style={{ flex: 1 }}
                >
                    {this.renderModalChildren()} 
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {
        auth_token: store.User.auth_token,
        is_client: store.User.is_client,
        settings: store.User.settings,
        modal: store.Modal.modal
    };
};

export default connect(mapStateToProps)(Wrapper);
