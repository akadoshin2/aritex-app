/**
 * Created by luanp on 12/10/2016.
 */
'use strict';
import { Dimensions } from 'react-native';

const ThemeColor1 = '#406ab3';
const ThemeColor2 = '#00aef0';
//const ThemeColor3 = '#77a464'

const Constants = {
    initializeAppFirebase: {
        apiKey: "AIzaSyDIGxDx4xJPlanUapazmBlz109RxvPB560",
        authDomain: "reacttestaritex.firebaseapp.com",
        databaseURL: "https://reacttestaritex.firebaseio.com",
        projectId: "reacttestaritex",
        storageBucket: "reacttestaritex.appspot.com",
        messagingSenderId: "181436959165"
    },
    ApiTex: {
        url: 'https://aritex-admin-test.herokuapp.com/api/',
    },
    Firebase: {
        apiKey: 'AIzaSyDIGxDx4xJPlanUapazmBlz109RxvPB560',
        CloudMessage: {
            TOPIC: '/topics/G_NOTIFICATION',
        }
    },
    EmitCode: {
        OpenModal: 'open-modal',
        CloseModal: 'close-modal'      
    },
    Dimension: {
        ScreenWidth(percent = 1) {
            return Dimensions.get('window').width * percent;
        },
        ScreenHeight(percent = 1) {
            return Dimensions.get('window').height * percent;
        },
    },
    Image: {
        Logo: require('./images/logo.png'),
        SplashScreen: require('./images/splash_screen.png'),
        PlaceHolder: require('./images/placeholderImage.png'),
    },
    Icon: { //App's icons. Checkout http://fontawesome.io/icons/ for more icons.
        Menu: 'ios-menu',
        Home: 'ios-home',
        Back: 'ios-arrow-back',
        Forward: 'ios-arrow-forward',
        Config: 'ios-settings',
        More: 'ios-more',
        SignIn: 'ios-log-in',
        SignOut: 'ios-log-out',
        ShoppingCart: 'ios-cart',
        ShoppingCartEmpty: 'ios-cart-outline',
        Sort: 'ios-funnel',
        Filter: 'ios-funnel-outline',
        ShowItem: 'ios-arrow-dropright',
        HideItem: 'ios-arrow-dropup',
        ListMode: 'ios-list-box',
        GridMode: 'ios-grid',
        RatingFull: 'ios-star',
        RatingEmpty: 'ios-star-outline',
        Delete: 'ios-trash',
        AddToCart: 'ios-cart',
        MyOrder: 'ios-cube',
        News: 'ios-paper',
        Mail: 'ios-mail-outline',
        RatioOff: 'ios-radio-button-off',
        RatioOn: 'ios-radio-button-on',
        Search: 'ios-search',
        Close: 'ios-close',
        HappyFace: 'ios-happy-outline',
        SadFace: 'ios-sad-outline',
        Lock: 'ios-lock-outline',
        Phone: 'ios-call-outline',
        Pencil: 'md-create',
        Alert: 'ios-alert-outline',
        Checkmark: 'md-checkmark'
    },
    Color: {
        Background: '#FFFFFF',
        DirtyBackground: '#F0F0F0',
        Error: '#ff0033',

        //Toolbar
        Toolbar: 'white',
        ToolbarText: '#283747',
        ToolbarIcon: '#283747',

        ToolbarTrans: 'transparent',
        ToolbarTextTrans: 'black',
        ToolbarIconTrans: 'black',

        TopBar: 'white',
        TopBarIcon: '#283747',

        //Button
        ButtonBackground: '#00aef0',
        ButtonText: 'white',
        ButtonBorder: '#bcbebb',

        //Product tabs
        TabActive: '#00aef0',
        TabDeActive: 'white',
        TabActiveText: 'white',
        TabDeActiveText: 'black',
        BuyNowButton: '#FF9522',

        ViewBorder: '#bcbebb',

        //Spinner
        Spinner: ThemeColor1,

        //Rating
        Rating: ThemeColor2,

        //Text
        TextNormal: '#77a464',
        TextLight: 'darkgray',
        TextDark: '#000000',
        ProductPrice: ThemeColor2,

        //sidemenu
        SideMenu: '#34BC99',
        SideMenuText: 'white',
        SideMenuIcon: 'white,'
    }
};

/*  We can't reference to outer object in constructor,
 *  therefore we need to add those property after Constants was created
 */
Constants.SplashScreen = {
    Duration: 1500, //Splash screen display duration (millisecond).
    Image: Constants.Image.SplashScreen
};
Constants.ProductCard = {
    ListMode: {
        container: {
            width: Constants.Dimension.ScreenWidth(0.9),
            marginLeft: Constants.Dimension.ScreenWidth(0.05),
            marginRight: Constants.Dimension.ScreenWidth(0.1 / 3),
            marginTop: Constants.Dimension.ScreenWidth(0.05),
        },
        image: {
            width: Constants.Dimension.ScreenWidth(0.9) - 2,
            height: 1.2 * Constants.Dimension.ScreenWidth(0.9),
        },
        text: {
            color: 'black',
            fontSize: 16,
            marginLeft: 15,
            marginRight: 15,
        }
    },
    GridMode: {
        container: {
            width: Constants.Dimension.ScreenWidth(0.9) / 2,
            marginLeft: Constants.Dimension.ScreenWidth(0.1 / 3),
            marginRight: 0,
            marginTop: Constants.Dimension.ScreenWidth(0.1 / 3),
        },
        image: {
            width: (Constants.Dimension.ScreenWidth(0.9) / 2) - 2,
            height: 1.2 * (Constants.Dimension.ScreenWidth(0.9) / 2),
        },
        text: {
            fontSize: 14,
            marginLeft: 10,
            marginRight: 10,
        }
    }
};
Constants.Formatter = {
    currency(value) {
        return `$ ${String(Math.round(value)).replace(/(.)(?=(\d{3})+$)/g, '$1.')}`;
    },
    capitalize(string) {
        return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
    },
    toTitleCase(string) {
        return string.replace(
            /\w\S*/g, 
            (txt) => {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            }
        );
    }
};
Constants.Swiper = {
    swiper_dot: {
        backgroundColor: 'rgba(0,0,0,.2)',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 4,
        marginRight: 4
    },
    swiper_active_dot: {
        backgroundColor: '#000',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 4,
        marginRight: 4
    },
};
Constants.ReferenceColors = [
    {
        color: 'AMARILLO CLARO',
        hex: '#FFF48C'
    },
    {
        color: 'AZUL NAVY',
        hex: '#002F5E'
    },
    {
        color: 'BLANCO',
        hex: '#FFFFFF'
    },
    {
        color: 'CORAL OSCURO',
        hex: '#BA403D'
    },
    {
        color: 'FUCSIA',
        hex: '#C34B90'
    },
    {
        color: 'FUCSIA OSCURO',
        hex: '#A40061'
    },
    {
        color: 'GRIS JASPÉ',
        hex: '#B5B5B5'
    },
    {
        color: 'MENTA CLARO',
        hex: '#A0C9C9'
    },    
    {
        color: 'NEGRO',
        hex: '#333333'
    },
    {
        color: 'ROJO',
        hex: '#C20016'
    },
    {
        color: 'TURQUEZA',
        hex: '#0098C0'
    },
    {
        color: 'VERDE JADE',
        hex: '#319E8F'
    },
    {
        color: 'AMARILLO ORO',
        hex: '#E6BF00'
    },
    {
        color: 'GRIS GRAFITO',
        hex: '#9B9B9B'
    },
    {
        color: 'CORAL',
        hex: '#CB524A'
    },
    {
        color: 'VDE MILITAR',
        hex: '#334D26'
    },
    {
        color: 'AGUA',
        hex: '#B6D9F3'
    },
    {
        color: 'AMARILLO',
        hex: '#FFF044'
    },
    {
        color: 'AZUL REY',
        hex: '#144491'
    },
    {
        color: 'GRIS',
        hex: '#A5A6A7'
    },
    {
        color: 'KAKY ARENA',
        hex: '#D0C29A'
    },
    {
        color: 'NARANJA',
        hex: '#DD9300'
    },
    {
        color: 'ROJO MALBORO',
        hex: '#9D0A17'
    },
    {
        color: 'ROSADO INTENSO',
        hex: '#9D0A17'
    },
    {
        color: 'VERDE CALI',
        hex: '#429A54'
    },
    {
        color: 'VINOTINTO',
        hex: '#602021'
    },
    {
        color: 'PETROLEO',
        hex: '#195071'
    },
    {
        color: 'AZUL FLASH',
        hex: '#009ADF'
    },
    {
        color: 'VERDE FLASH',
        hex: '#95BA4C'
    },
    {
        color: 'MENTA',
        hex: '#96C2B5'
    },
    {
        color: 'MORADO',
        hex: '#95BA4C'
    },
    {
        color: 'VINO',
        hex: '#95BA4C'
    },
    {
        color: 'UVA',
        hex: '#95BA4C'
    },
    {
        color: 'ROSADO CLARO',
        hex: '#95BA4C'
    },
    {
        color: 'KAKI',
        hex: '#CEBE97'
    },
    {
        color: 'MOSTAZA',
        hex: '#CFA943' 
    },
    {
        color: 'OCRE',
        hex: '#C85634'
    },
    {
        color: 'VERDE LIMON',
        hex: '#B7C72C'
    }
];
export default Constants;
