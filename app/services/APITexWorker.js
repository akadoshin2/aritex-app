/* eslint-disable space-infix-ops */
'use strict';

import APITex from './APITex';
import Constants from './../Constants';

// import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import firebase from 'firebase';
import email from 'react-native-email';
const app = firebase.initializeApp(Constants.initializeAppFirebase);
const database = app.database();
const auth = firebase.auth();

// var statusConnect = database.ref(".info/connected");

// statusConnect.on("value", snapshot => {
//     if (snapshot.val() === true) {
//       console.log("connected");
//     } else {
//       console.log("not connected");
//     }
//   });

const Api = new APITex({
    url: Constants.ApiTex.url,
});

const APITexWorker = {
    // User
    signInUser: (identification, password, callback, errorCallback) => {
        auth.signInWithEmailAndPassword(identification, password)
        .then((responseData) => {
            database.ref(`/ThemeApp/users/${responseData.uid}`).once('value', snapshotUser => {
                const user = snapshotUser.val();
                user.auth_token = auth;
                user.refData = database.ref(`/ThemeApp/users/${responseData.uid}`);
                callback(user);
            });
        }).catch((error) => {
            console.log('error', error);
            errorCallback();
        });
        // Api.post(null, 'signin', { 
        //     identification: 'mbolivar@gmail.com',
        //     password: '1234',
        // }).then((responseData) => {
        //     const { status } = responseData;
        //     if (status === 200) console.log("responseData", responseData);
        //     else errorCallback();
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    onAuthStateChanged: (callback) => {
        auth.onAuthStateChanged(user => {
            console.log('user change');
            callback(user);
        });
    },
    signUpUser: (user, callback, errorCallback) => {
        /** Pendiente */
        console.log('signUpUser', auth.currentUser);
        user.auth_token.signOut().then(() => {
            callback({});
        });
        // Api.post(null, 'finalsellers', { 
        //     email: user.email,
        //     password: user.password,
        //     name: user.name,
        //     cc: user.cc,
        //     country: 'Colombia',
        //     city: user.city,
        //     zone: user.zipcode,
        //     phone1: user.phone,
        //     phone2: user.mobile,
        //     address: user.address, 
        // }).then((responseData) => {
        //     const { status } = responseData;
        //     if (status === 200){
        //         auth.signOut().then(()=>{
        //             callback({});
        //         });
        //     }
        //     else errorCallback();
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    fetchUserData: (refData, callback, callbackClient, errorCallback) => {
        /** Terminado */
        console.log('fetchUserData');
        if ((refData.refData === '')) {
            refData.once('value', snapshotUser => {
                let user = snapshotUser.val();
                callback(user);
            });
        }
        console.log(refData.user.clientes_list);
        callbackClient(refData.user.clientes_list);
        // Api.get(auth_token, 'update_user').then((responseData) => {
        //     const { status } = responseData;
        //     console.log(responseData);
        //     if (status === 200) callback(responseData);
        //     else errorCallback('Error');
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    fetchClientsData: (refData, callbackClient, errorCallback) => {
        /** Terminado */
        console.log('fetchClientsData', refData);
        refData.once('value', snapshotUser => {
            let user = snapshotUser.val();
            console.log(user);
            for(let i in user){
                console.log(i);
            }
            callbackClient(user.clientes_list||[]);
        });
        // database.ref('/ThemeApp/data/clientes').once('value', snapshotProducts => {
        //     if (snapshotProducts.val()) {
        //         callback(snapshotProducts.val());
        //     } else {
        //         errorCallback('Error');
        //     }
        // });
        // if(refData === ''){
        //     refData.once("value", snapshotUser => {
        //         callback(snapshotUser.val());
        //     });
        // }
    },
    submitOrder: (user, clientId, products, observation, total, screen, callback, errorCallback) => {
        /** Pendiente */
        console.log(typeof clientId != 'object');
        if(typeof clientId == 'string'){
            errorCallback();
            return;
        }
        const key_ = database.ref('/ThemeApp/data/orders').push().key;
        database.ref('/ThemeApp/data/orders').child(key_).set({
                client: clientId,
                client_id: clientId.cc,
                client_name: clientId.name,
                sucursal_id: clientId.id_sucursal[0],
                date: new Date().toDateString(),
                products,
                observation,
                total,
                screen: screen || ''
            }, order => {
                database.ref(`/ThemeApp/users/${user.auth_token.currentUser.uid}/orders`).child(key_).set({
                        client: clientId,
                        user,
                        date: new Date().toDateString(),
                        products,
                        observation,
                        total,
                        screen: screen || ''
                    }, order => {
                    callback(key_);
                });
        });

        // Api.post(auth_token, 'orders', {
            // client_id: clientId,
            // products,
            // observation,
            // total,
            // screen
        // }).then((responseData) => {
        //     const { status, order } = responseData;
        //     if (status === 200) callback(order._id);
        //     else errorCallback();
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    createClient: (auth_token, client, callback, errorCallback) => {
        /** Pendiente */
        console.log('createClient', client);
        const to = []; // string or array of email addresses
        email(to, {
            // Optional additional arguments
            subject: 'Solicitud para crear cliente, Aritex',
            body: `
Datos del cliente:

- Nombre o Razón Social: ${client.name},
- Correo electronico: ${client.email},

- CC o NIT: ${client.cc},
- DV: ${client.dv},

- Código de Actividad: ${client.act_code},
- Domicilio Principal: ${client.address},
- Ciudad: ${client.city},

- Teléfono Fijo: ${client.phone},
- Teléfono Movil: ${client.mobile},

- Tesorería: ${client.treasure_dept},
- Cupo Asignado: ${client.assigned},
- Flete: ${client.flete},
- Plazo de pago: ${((client.time) ? `${client.time} dias`:'Contado')}

** Adjunte Registro Mercantil y CC o NIT **
            `
        }).then(callback).catch(errorCallback);
        // Api.post(auth_token, 'clients/create', client).then((responseData) => {
        //     const { status } = responseData;
        //     if (status === 200) callback();
        //     else errorCallback();
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    saveCart: ({ auth_token, refData }, cart, callback, errorCallback) => {
        /** Pendiente */
        console.log('saveCart', refData);
        callback();
        // Api.post(auth_token, 'update_cart', cart).then((responseData) => {
        //     const { status } = responseData;
        //     if (status === 200) callback();
        //     else errorCallback();
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    updateUser: (auth_token, data, callback, errorCallback) => {
        /** Pendiente */
        console.log('updateUser');
        // Api.post(auth_token, 'update_user', {
        //     old_password: data.password, 
        //     password: data.newPassword, 
        //     confirm_password: data.newPassword,
        //     profile: data.image
        // }).then((responseData) => {
        //     const { status, message } = responseData;
        //     if (status === 200) callback();
        //     else errorCallback(message);
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    // updateClients: (auth_token, callback, errorCallback) => {
    //     console.log("updateClients");
        // Api.get(auth_token, 'clients').then((responseData) => {
        //     const { status, clients } = responseData;
        //     if (status === 200) callback(clients);
        //     else errorCallback();
        // }).catch((error) => {
        //     console.log(error);
        // });
    // },
    // fetchOrders: (auth_token, callback, errorCallback) => {
    //     Api.get(auth_token, 'orders').then((responseData) => {
    //         const { status, orders } = responseData;
    //         if (status === 200) callback(orders);
    //         else errorCallback();
    //     }).catch((error) => {
    //         console.log(error);
    //     });
    // },

    // Categories / Products
    fetchStoreData: (callback, errorCallback) => {
        /** Dummy data */
        console.log('fetchStoreData');
        database.ref('/ThemeApp/data/products').once('value', snapshotProducts => {
            if (snapshotProducts.val()) {
                callback(snapshotProducts.val());
            }
        });
        
        // Api.get(null, 'update_categories').then((responseData) => {
        //     // const { status } = responseData;
        //     // if (status === 200) callback(responseData);
        //     // else errorCallback('Error');
        //     console.log(responseData);
        //     callback(responseData);
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    // fetchHomeCarousels: (callback, errorCallback) => {
    //     Api.get(null, 'best/categories').then((responseData) => {
    //         // const { status, carousels } = responseData;
    //         // if (status === 200) callback(carousels);
    //         // else errorCallback();
    //         callback(responseData);
    //     }).catch((error) => {
    //         console.log(error);
    //     });
    // },
    // categories: (callback, error = (e) => console.log(e)) => {
    //     Api.get(null, 'categories', {
    //         hide_empty: true,
    //         per_page: 100,
    //         order: 'desc',
    //         orderby: 'count'
    //     }).then((json) => {
    //         if (json.message === undefined) callback(json);
    //         else error(JSON.stringify(json.message));
    //     }).catch(error);
    // },
    // productById: (callback, error = (e) => console.log(e), id, is_client) => {
    //     Api.get(null, `products/${id}`, {
    //             is_client
    //         }).then((json) => {
    //         if (json.message === undefined) callback(json);
    //         else error(JSON.stringify(json.message));
    //     }).catch(error);
    // },
    // productsByCategoryId: (callback, error = (e) => console.log(e), id, is_client, per_page, page) => {        
    //     Api.get(null, 'products', {
    //             category: id,
    //             is_client, 
    //             per_page, 
    //             page
    //         }).then((json) => {
    //         if (json.message === undefined) callback(json);
    //         else error(JSON.stringify(json.message));
    //     }).catch(error);
    // },
    // productsByName: (callback, error = (e) => console.log(e), name, is_client, per_page, page) => {
    //     Api.get(null, 'products/find', {
    //             search: name, 
    //             is_client,
    //             per_page, 
    //             page
    //         }).then((json) => {
    //         if (json.message === undefined) callback(json);
    //         else error(JSON.stringify(json.message));
    //     }).catch(error);
    // },

    // Carousels
    fetchSliderData: (id, callback, errorCallback) => {
        /** Pendiente */
        console.log('fetchSliderData', id);
        database.ref('/ThemeApp/carousels').once('value', snapshotCarousels => {
            if (snapshotCarousels.val()) {
                const data = snapshotCarousels.val();
                let layout_ = 2;
                if (id === 'login') layout_ = 1;
                for (const key in data) {
                    if (data[key].layout === layout_) {
                        callback(data[key]);
                    }
                }
            }
        });
        // Api.get(null, `carousels/find/${id}`).then((responseData) => {
        //     const { status, carousel } = responseData;
        //     if (status === 200) callback(carousel);
        //     else errorCallback();
        // }).catch((error) => {
        //     console.log(error);
        // });
    },
    fetchCarouselsData: (callback, errorCallback) => {
        /** Pendiente */
        console.log('fetchCarouselsData');
        database.ref('/ThemeApp/carousels/carousels').once('value', snapshotCarousels => {
            if (snapshotCarousels.val()) {
                callback(snapshotCarousels.val());
            }
        });
        // Api.get(null, 'update_carousels').then((responseData) => {
        //     const { status, carousels } = responseData;
        //     if (status === 200) callback(carousels);
        //     else errorCallback('Error');
        // }).catch((error) => {
        //     console.log(error);
        // });
    }
};

export default APITexWorker;
