'use strict';

export default APITex;

/**
 * Aritex REST API wrapper
 *
 * @param {Object} opt
 */
function APITex(opt) {
    if (!(this instanceof APITex)) {
        return new APITex(opt);
    }

    opt = opt || {};

    if (!(opt.url)) {       
        throw new Error('url is required');
    }

    this.setDefaultsOptions(opt);
}

/**
 * Set default options
 *
 * @param {Object} opt
 */
APITex.prototype.setDefaultsOptions = function (opt) {
    this.url = opt.url;
};

/**
 * Get URL
 *
 * @param  {String} endpoint
 *
 * @return {String}
 */
APITex.prototype.getUrl = function (endpoint) {
    let url = this.url;

    url = `${url}${endpoint}`;

    return url;
};

/**
 * Join key object value to string by separator
 */
APITex.prototype.join = function (obj, separator) {
    let arr = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            arr.push(key + '=' + obj[key]);
        }
    }
    return arr.join(separator);
};

/**
 * Do requests
 *
 * @param  {String}   method
 * @param  {String}   endpoint
 * @param  {Object}   data
 * @param  {Function} callback
 *
 * @return {Object}
 */
APITex.prototype.request = function (method, auth_token, endpoint, data, callback) {
    let url = this.getUrl(endpoint);
    let headers;
    let body;

    if (method === 'GET') {
        headers = { 
            'Cache-Control': 'no-cache',
            Authorization: auth_token !== null ? auth_token : ''
        };
        if (data !== undefined) url = `${url}?${this.join(data, '&')}`;
    } else if (method === 'POST') {
        headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: auth_token !== null ? auth_token : ''
        };
        body = JSON.stringify(data);
    }
    console.log(url);
    return (
        fetch(url, { method, headers, body })
        .then((response) => response.json())
        .then((responseData) => {
            if (typeof callback == 'function') callback();
            // console.log(responseData);
            return responseData;
        }).catch((error) => console.warn(error))
    );
};

/**
 * GET requests
 *
 * @param  {String}   endpoint
 * @param  {Function} callback
 *
 * @return {Object}
 */
APITex.prototype.get = function (auth_token, endpoint, data, callback) {
    return this.request('GET', auth_token, endpoint, data, callback);
};

/**
 * POST requests
 *
 * @param  {String}   endpoint
 * @param  {Object}   data
 * @param  {Function} callback
 *
 * @return {Object}
 */
APITex.prototype.post = function (auth_token, endpoint, data, callback) {
    return this.request('POST', auth_token, endpoint, data, callback);
};

/**
 * PUT requests
 *
 * @param  {String}   endpoint
 * @param  {Object}   data
 * @param  {Function} callback
 *
 * @return {Object}
 */
APITex.prototype.put = function (auth_token, endpoint, data, callback) {
    return this.request('PUT', auth_token, endpoint, data, callback);
};

/**
 * DELETE requests
 *
 * @param  {String}   endpoint
 * @param  {Function} callback
 *
 * @return {Object}
 */
APITex.prototype.delete = function (auth_token, endpoint, callback) {
    return this.request('DELETE', auth_token, endpoint, null, callback);
};

/**
 * OPTIONS requests
 *
 * @param  {String}   endpoint
 * @param  {Function} callback
 *
 * @return {Object}
 */
APITex.prototype.options = function (auth_token, endpoint, callback) {
    return this.request('OPTIONS', auth_token, endpoint, null, callback);
};
