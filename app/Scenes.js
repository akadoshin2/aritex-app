'use strict';

import React from 'react';
import { Scene, Actions, ActionConst } from 'react-native-router-flux';

import Languages from './Languages';

//Scenes
import Home from './containers/home';
import Catalog from './containers/catalog';
import Category from './containers/category';
import Product from './containers/product';
import MyOrders from './containers/myorders';
import Settings from './containers/settings';
import Login from './containers/login';
import SignUp from './containers/signup';
import CreateClient from './containers/clients/CreateClient';
import ListClients from './containers/clients/ListClients';
import Profile from './containers/profile';

const Scenes = Actions.create(
    <Scene 
        key="scene"
    >                
        <Scene 
            key="authentication"
            initial
        >
            <Scene 
                key="login" 
                component={Login} 
                title={Languages.Login} 
            />
            <Scene 
                key="signup" 
                component={SignUp} 
                title="Registro"
                back
            />                  
        </Scene>

        <Scene 
            key="main" 
            type={ActionConst.RESET} 
        >          
            <Scene 
                key="home" 
                component={Home} 
                title={Languages.Home}
                search                 
            />
            <Scene 
                key="category" 
                component={Category}
                search
                back
            />
            <Scene
                key="product" 
                component={Product} 
                title={Languages.Product}
                back
            />    
            <Scene 
                key="catalog"
                component={Catalog} 
                title={Languages.Catalog}
                search
            />               
            <Scene 
                key="myorders" 
                component={MyOrders} 
                title={Languages.MyOrders}
                elevation={false} 
            />              
            <Scene 
                key="settings" 
                component={Settings} 
                title={Languages.Settings}
            />
            <Scene 
                key="createclient" 
                component={CreateClient} 
                title={Languages.CreateClient}
                back
            />
            <Scene 
                key="listclients" 
                component={ListClients} 
                title={Languages.MyClients}
                back
            /> 
            <Scene
                key="profile"
                component={Profile}
                title="Perfil"  
                back 
            />     
        </Scene>
    </Scene>
);

export default Scenes;
