'use strict';

import React, { Component } from 'react';
import {
    Text,
    View,
    ListView,
    ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import Constants from './../../Constants';
import ProductCard from './ProductCard';

import { selectCategory, selectProduct } from '../../reducers/Store/actions';

/*
*   title
*   products
*/
class HomeCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderActivityIndicator() {
        return (
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: Constants.Dimension.ScreenWidth(0.36)
                }}
            >
                <ActivityIndicator
                    size="large"
                />
            </View>
        );
    }

    renderProducts(data) {
        const dataSource = new ListView.DataSource({ rowHasChanged: () => true });

        const renderRow = (product) => {
            const goToProduct = (product) => {
                this.props.selectCategory(product.categories[0]);
                this.props.selectProduct(product);
                Actions.product({ product, addProduct: false }); 
            };
            return (
                <ProductCard
                    onPress={goToProduct}
                    product={product}
                />
            );
        };

        return (
            <ListView
                ref="_listView"
                style={{
                    marginBottom: 8
                }}
                dataSource={dataSource.cloneWithRows(data)}
                renderRow={renderRow}
                showsHorizontalScrollIndicator={false}
                enableEmptySections
                horizontal
            />
        );
    }

    renderCarousel(title, products) {
        return (
            <View>
                <View
                    style={{
                        flexDirection: 'row',
                        // justifyContent: 'space-between',
                        alignItems: 'center'
                    }}
                >
                    <Text
                        style={{
                            fontSize: 12,
                            color: 'gray',
                            margin: 8
                        }}
                    >
                        {title.toUpperCase()}
                    </Text>
                </View>
                {this.renderProducts(products)}
            </View>
        );
    }

    render() {
        const { title, products } = this.props;

        return (
            <View style={{ flex: 1 }}>
                {
                    products.length <= 0 ?
                        <View /> :
                        this.renderCarousel(title, products)
                }
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        selectCategory: (selectedCategoryId, selectedCategoryName) => {
            dispatch(selectCategory(selectedCategoryId, selectedCategoryName));
        },
        selectProduct: (selectedProduct) => {
            dispatch(selectProduct(selectedProduct));
        }
    };
};

export default connect(null, mapDispatchToProps)(HomeCarousel);
