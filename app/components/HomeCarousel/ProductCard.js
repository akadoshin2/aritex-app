
import React, { Component, PropTypes } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';

import Constants from './../../Constants';

export default class ProductCard extends Component {
    static propTypes = {
        product: PropTypes.object.isRequired,
        onPress: PropTypes.func.isRequired,
    };

    renderProductImage(product) {
        return (
            <Image
                source={{
                    uri: product.images[0].src
                }}
                style={{
                    width: Constants.Dimension.ScreenWidth(0.27),
                    height: Constants.Dimension.ScreenWidth(0.36)
                }}
                resizeMode="cover"
            />
        );
    }

    renderProductReference(product) {
        return (
            <Text
                ellipsizeMode={'tail'}
                numberOfLines={1}
                style={{
                    fontSize: 12,
                    color: 'black',
                    marginBottom: 0,
                    marginLeft: 10,
                    marginRight: 10
                }}
            >
                {`Ref. ${product.reference}`}
            </Text>
        );
    }

    renderProductPrice(product) {
        return (
            <View style={{ flexDirection: 'column' }}>
                <Text
                    style={{
                        fontSize: 12,
                        fontWeight: '600',
                        color: Constants.Color.ProductPrice,
                        marginBottom: 0,
                        marginLeft: 10,
                        marginRight: 10
                    }}
                >
                    {Constants.Formatter.currency(product.price)}
                </Text>
            </View>
        );
    }

    render() {
        const { product, onPress } = this.props;

        return (
            <TouchableOpacity
                style={{
                    marginLeft: 8,
                    width: Constants.Dimension.ScreenWidth(0.27)
                }}
                onPress={() => onPress(product)}
            >
                {this.renderProductImage(product)}
                {this.renderProductReference(product)}
                {this.renderProductPrice(product)}
            </TouchableOpacity>
        );
    }
}
