'use strict';

import React, { Component } from 'react';
import { View, Alert, ScrollView } from 'react-native';
import { Body2, Caption, Divider } from 'carbon-ui';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import TimerMixin from 'react-timer-mixin';
import DismissKeyboard from 'dismissKeyboard';

import Toolbar from './../../components/Toolbar';
import Constants from './../../Constants';

import { addCartItem } from '../../reducers/Cart/actions';
import { closeModal } from '../../reducers/Modal/actions';

/*
    product -> reducer
    productCart
    isModal
*/

class VariationsGrid extends Component {
    constructor(props) {
        super(props);

        this.cartItemIndex = this.props.cartItems.findIndex((cartItem) => {
            return this.props.product.reference_id === cartItem.product.reference_id;
        });  

        this.setVariations = () => {
            const { product, cartItems } = this.props;
            const { color, size } = product.variations;

            let variations = Array(color.length).fill().map(
                () => Array(size.length).fill('0'));

            if (this.cartItemIndex !== -1) {
                cartItems[this.cartItemIndex].variations.forEach((variation) => {
                    const colorIndex = color.indexOf(variation.color);
                    const sizeIndex = size.indexOf(variation.size);
                    const quantity = variation.quantity.toString();

                    variations[colorIndex][sizeIndex] = quantity;
                });   
            }

            return variations;
        };

        this.reduceVariations = (variations) => {
            return variations.reduce((total, row) => {
                return total + row.reduce((rowTotal, col) => {
                    return rowTotal + parseInt(col, 10);
                }, 0);
            }, 0);
        };

        this.addToCart = (variations) => {
            const { product } = this.props;
            const { color, size } = product.variations;
            const { time } = this.state;

            const now = moment();
            let productSelectedVariations = [];

            variations.forEach((row, rowIndex) => {
                row.forEach((col, colIndex) => {
                    if (parseInt(col, 10) > 0) {
                        productSelectedVariations.push({
                            color: color[rowIndex],
                            size: size[colIndex],
                            quantity: parseInt(col, 10),
                            input: col,
                            inputWarning: false   
                        });
                    }
                });
            });

            this.props.addCartItem(
                product, 
                productSelectedVariations, 
                this.reduceVariations(variations),
                now.diff(time, 'seconds')
            );
            
            Actions.myorders({ type: 'reset' });
            DismissKeyboard();
            TimerMixin.setTimeout(
                () => this.props.closeModal(), 
                400
            );
        };

        this.state = {
            product: this.props.product,
            variations: this.setVariations(),
            time: moment()
        };                
    }

    renderSizeHeader() {
        const { variations } = this.props.product;

        return (
            <View style={{ flexDirection: 'row' }}>
                <View 
                    style={{
                        alignItems: 'center',
                        width: 80
                    }}
                >
                    <Body2 style={{ color: '#0D47A1' }}>
                        COLOR
                    </Body2>
                </View>
                <View
                    style={{   
                        flex: 1,                 
                        flexDirection: 'row',
                        justifyContent: 'space-around'
                    }}
                >                
                    {variations.size.map((size, sizeIndex) => 
                        
                            <Body2 
                                key={`sizeHeader${sizeIndex}`}
                                style={{ color: '#0D47A1' }}
                            >
                                {size}
                            </Body2>
                    )}
                </View>
                <View 
                    style={{
                        alignItems: 'center',
                        width: 80
                    }}
                >
                    <Body2 style={{ color: '#0D47A1' }}>
                        TOTAL
                    </Body2>
                </View>
            </View>
        );
    }

    renderColorHeader() {
        const { variations } = this.props.product;

        return (
            <View
                style={{
                    marginHorizontal: 10,
                    justifyContent: 'space-around',
                    alignItems: 'center'
                }}
            >
                {variations.color.map((color, colorIndex) => {
                    const index = Constants.ReferenceColors.findIndex((item) => {
                        return color === item.color;
                    });

                    if (color === 'SURTIDO (999)') return undefined;

                    return (
                        <View
                            key={`colorHeader${colorIndex}`}
                            style={{ marginTop: 25 }}
                        >
                            <View                            
                                style={{
                                    height: 20,
                                    width: 50,                             
                                    backgroundColor: index !== -1 ? 
                                        Constants.ReferenceColors[index].hex :
                                        'white',
                                    borderWidth: 1,
                                    borderColor: index !== -1 ?
                                        'black' :
                                        'red'
                                }}
                            />
                            <Caption>
                                {variations.colorId[colorIndex]}
                            </Caption>
                        </View>
                    );
                })}
            </View>
        );
    }

    renderGrid() {
        const { variations } = this.state;

        const onValueChange = (value, rowIndex, colIndex) => {
            const regex = /^[0-9]+$/;
            let grid = variations.slice();

            const setValue = (value) => {
                grid[rowIndex][colIndex] = value;
                this.setState({
                    variations: grid
                });
            };

            if (value.length === 0) {  
                setValue('0');
            } else if (regex.test(value)) {
                setValue(value);
            } else {
                setValue(value.length === 1 ? 
                    '0' :
                    value.substr(0, value.length - 1)
                );
            }
        };

        return (
            <View>
            {variations.map((row, rowIndex) => {
                return (
                    <View 
                        key={rowIndex}
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}
                    >
                        {row.map((col, colIndex) =>
                            <View
                                key={colIndex}
                                style={{
                                    width: (Constants.Dimension.ScreenWidth() - 150) / 4,
                                    marginBottom: 15,
                                    marginHorizontal: 10
                                }}
                            >
                                <TextField                                    
                                    label=""
                                    placeholder="0"
                                    value={col > 0 ? col : ''}
                                    onChangeText={(value) => onValueChange(value, rowIndex, colIndex)}                                    
                                    animationDuration={150} 
                                    keyboardType="numeric"
                                />
                            </View>
                        )}
                    </View>
                );
            })}
            </View>
        );
    }

    renderSizeFooter() {
        const { product } = this.props;
        const { variations } = this.state;
        const sizeFooter = Array(product.variations.size.length).fill(0);

        variations.forEach((row) => {
            row.forEach((col, colIndex) => {
                sizeFooter[colIndex] += parseInt(col, 10);
            });
        });

        return (
            <View style={{ flexDirection: 'row' }}>
                <View 
                    style={{
                        alignItems: 'center',
                        width: 70
                    }}
                >
                    <Body2 style={{ color: '#0D47A1' }}>
                        TOTAL
                    </Body2>
                </View>
                <View
                    style={{
                        flex: 1,               
                        flexDirection: 'row',
                        justifyContent: 'space-between',                       
                        marginRight: 80,
                        paddingVertical: 10
                    }}
                >
                    {product.variations.size.map((size, sizeIndex) =>                     
                        <View
                            key={`sizeFooter${sizeIndex}`} 
                            style={{ 
                                width: (Constants.Dimension.ScreenWidth() - 150) / 4,
                                alignItems: 'center',
                                marginHorizontal: 10
                            }}
                        >
                            <Caption>
                                {sizeFooter[sizeIndex]}
                            </Caption>
                        </View>
                    )}
                </View>
            </View>
        );
    }

    renderColorFooter() {
        const { variations } = this.state;

        return (
            <View
                style={{                
                    justifyContent: 'space-between',  
                    alignItems: 'center' 
                }}
            >                
                {variations.map((color, colorIndex) => {
                    return (
                        <View
                            key={`colorFooter${colorIndex}`}
                            style={{
                                width: 80,
                                marginTop: 40,
                                marginBottom: 22.5,
                                alignItems: 'center'
                            }}
                        >
                            <Caption>
                                {color.reduce((total, value) => {
                                    return total + parseInt(value, 10);
                                }, 0)}
                            </Caption>
                            <Divider 
                                style={{ 
                                    width: 80,
                                    marginTop: 5
                                }}
                            />
                        </View>
                    );
                })}
            </View>
        );
    }

    renderContent() {
        const scrollToBegin = () => {
            this.refs.horizontalScrollView.scrollTo({ 
                x: 0, 
                y: 0, 
                animated: true 
            });
        };

        return (
            <View style={{ flex: 1 }}>                
                <ScrollView 
                    ref="horizontalScrollView"
                    horizontal
                    onScrollEndDrag={() => scrollToBegin()}
                    contentContainerStyle={{ flexDirection: 'column' }}
                >
                    {this.renderSizeHeader()}
                    <Divider style={{ marginVertical: 2 }} />
                    <ScrollView>                        
                        <View style={{ flexDirection: 'row' }}>
                            {this.renderColorHeader()}                                      
                            {this.renderGrid()}                    
                            {this.renderColorFooter()}
                        </View>                        
                    </ScrollView>
                    <Divider style={{ marginVertical: 2 }} />                
                    {this.renderSizeFooter()}            
                </ScrollView>
            </View>
        );
    }

    render() { 
        const { variations } = this.state;

        const submitToCart = () => {
            Alert.alert(
                'Confirmar',
                'Presione OK para añadir al carrito',                
                [            
                    {
                        text: 'CANCEL', 
                        onPress: () => {}, 
                        style: 'cancel'
                    },
                    {
                        text: 'OK', 
                        onPress: () => this.addToCart(variations)
                    },
                ],
                { cancelable: true }
            );             
        };

        return (
            <View style={{ flex: 1 }}>           
                <Toolbar 
                    title="Variaciones"  
                    back 
                    isModal     
                    check={this.reduceVariations(variations) > 0 ? true : false}
                    rightCallback={() => submitToCart()}
                />                                       
                {this.renderContent()}
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return { 
        product: store.Store.selectedProduct,
        cartItems: store.Cart.cartItems
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addCartItem: (product, variations, amount, time) => {
            dispatch(addCartItem(product, variations, amount, time));
        },
        closeModal: () => {
            dispatch(closeModal());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(VariationsGrid);
