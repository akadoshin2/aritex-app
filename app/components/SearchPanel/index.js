'use strict';

import React, { Component } from 'react';
import {
    Text, 
    TextInput, 
    View, 
    TouchableOpacity, 
    ListView
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import TimerMixin from 'react-timer-mixin';
import DismissKeyboard from 'dismissKeyboard';
import { Actions } from 'react-native-router-flux';

import Constants from './../../Constants';
import Languages from './../../Languages';
import ItemRow from './ItemRow';

import { selectCategory, selectProduct } from '../../reducers/Store/actions';
import { closeModal } from '../../reducers/Modal/actions';

class SearchPanel extends Component {
    constructor(props) {
        super(props);

        this.state = {
            query: '',
            search: false,
            data: [],
            dataSource: new ListView.DataSource({ rowHasChanged: () => true })
        };
        this.getDataSource = (products) => this.state.dataSource.cloneWithRows(products);

        this.closeModal = () => {
            DismissKeyboard(); // There is a serious bug occurs whenever the keyboard close AFTER modal
            TimerMixin.setTimeout(
                () => this.props.closeModal(), 
                400
            );
        };
    }

    componentDidMount() {
        TimerMixin.setTimeout(
            () => this.refs.textInput != undefined && this.refs.textInput.focus(), 
            400
        );
    }

    startNewSearch() {
        const { products } = this.props;
        const { query } = this.state;

        const data = products.filter((product) => {
            return (product.name.toLowerCase().indexOf(query.toLowerCase()) !== -1 ||
                    product.reference.indexOf(query) !== -1
            );
        });

        this.setState({
            data,
            dataSource: this.getDataSource(data),
            search: true
        });
    }

    renderResultList() {
        const openProduct = (product) => {
            this.props.selectCategory(product.categories[0]);
            this.props.selectProduct(product);            
            Actions.product({ product, addProduct: false });
            this.closeModal();
        };
        const addToCart = (product) => {
            this.props.selectCategory(product.categories[0]);
            this.props.selectProduct(product);
            // this.closeModal();
            Actions.product({ product, addProduct: true });
        };
        return (
            <ListView
                enableEmptySections
                dataSource={this.state.dataSource}
                renderRow={(item) => 
                    <ItemRow 
                        product={item} 
                        openProduct={openProduct}
                        addToCart={addToCart}
                    />
                }
            />
        );
    }

    renderSearchBar() {
        const closeButton = (
            <TouchableOpacity
                onPress={() => this.closeModal()}
                style={{ width: 50, justifyContent: 'center', alignItems: 'center' }}
            >
                <Icon name={Constants.Icon.Close} size={30} />
            </TouchableOpacity>
        );

        const separate = (
            <View 
                style={{
                    borderLeftWidth: 1, 
                    borderColor: Constants.Color.DirtyBackground
                }}
            />
        );

        const searchInput = (
            <TextInput
                ref="textInput"
                placeholder={Languages.SearchPlaceHolder}
                style={{ flex: 1, marginLeft: 10, fontSize: 18 }}
                value={this.state.query}
                onChangeText={(query) => this.setState({ query, search: false })}
                underlineColorAndroid='transparent'
                onSubmitEditing={() => this.startNewSearch()}
            />
        );

        return (
            <View 
                style={{
                    height: 50,
                    flexDirection: 'row',
                    marginBottom: 10,
                    borderBottomWidth: 1,
                    borderColor: Constants.Color.DirtyBackground,
                }}
            >
                {closeButton}
                {separate}
                {searchInput}
            </View>
        );
    }

    render() {
        const { query, data, search } = this.state;

        return (
            <View style={{ flex: 1 }}>
                {this.renderSearchBar()}
                <View style={{ flex: 1 }}>
                    {
                        (query.length > 0 && data.length === 0 && search) ?                            
                            <Text style={{ textAlign: 'center' }}>
                                {Languages.NoResultError}
                            </Text> :
                            this.renderResultList()
                    }
                </View>
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    let products = [];
    store.Store.categories.forEach((category) => {
        products = products.concat(category.products);
    });
    return {
        products
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectCategory: (selectedCategoryId) => {
            dispatch(selectCategory(selectedCategoryId));
        },
        selectProduct: (selectedProduct) => {
            dispatch(selectProduct(selectedProduct));
        },
        closeModal: () => {
            dispatch(closeModal());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPanel);
