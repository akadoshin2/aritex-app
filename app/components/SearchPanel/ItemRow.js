import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import { RaisedButton } from 'carbon-ui';
import Icon from 'react-native-vector-icons/Ionicons';

import Constants from './../../Constants';

export default class ItemRow extends Component {
    constructor(props) {
        super(props);
        this.imageWidth = Constants.Dimension.ScreenWidth(0.25);
        this.styles = {
            container: {
                height: undefined,
                width: undefined,
                borderColor: Constants.Color.ViewBorder,
                borderWidth: 1,
                flexDirection: 'row',
                margin: 10,
                marginTop: 0,
            },
            image: {
                width: this.imageWidth,
                height: this.imageWidth * 1.2,
            },
            product_name: {
                padding: 10,
                paddingTop: 5,
                paddingBottom: 5,
                height: undefined,
                justifyContent: 'flex-start',
            },
            product_price: {
                padding: 10,
                paddingTop: 0,
                paddingBottom: 0,
                flex: 1,
                justifyContent: 'flex-start',
            },
            buttons_wrapper: {
                padding: 10,
                paddingTop: 5,
                paddingBottom: 5,
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-start',
            },
            itemWrapper: {
                justifyContent: 'center',
                padding: 0,
                marginLeft: 10,
            },
        };
    }

    renderPriceGroup(_product) {
        const styles = {
            row: {
                flexDirection: 'row',
            },
            price: {
                color: Constants.Color.ProductPrice,
                fontSize: 14,
                fontWeight: 'bold',
                marginRight: 5,
            },
            sale_price: {
                textDecorationLine: 'line-through',
                color: Constants.Color.TextLight,
                fontWeight: 'normal',
            },
            sale_off: {
                color: Constants.Color.TextLight,
                fontWeight: 'normal',
            }
        };

        return (
            <View style={this.styles.product_price}>
                <View style={styles.row}>
                    <Text style={[styles.price]}>
                        {Constants.Formatter.currency(_product.price) }
                    </Text>
                </View>
            </View>
        );
    }

    renderButtonsGroup(_product) {
        return (
            <View style={this.styles.buttons_wrapper}>
                <RaisedButton 
                    onPress={() => this.props.openProduct(_product)}
                    style={{ 
                        backgroundColor: '#1976D2'
                    }}
                >
                    Detalles
                </RaisedButton><RaisedButton 
                    onPress={() => this.props.addToCart(_product)}
                    style={{ 
                        backgroundColor: '#1976D2'
                    }}
                >
                    <Icon name="md-cart" size={24} color="white" />
                </RaisedButton>
            </View>
        );
    }

    render() {
        const { product } = this.props;
        const productImage = (
            <Image
                source={{ uri: product.images[0].src }}
                style={this.styles.image}
                resizeMode="cover"
            />);

        const productName = (
            <Text
                ellipsizeMode={'tail'}
                numberOfLines={2}
                style={this.styles.product_name}
            >
                {product.name}
            </Text>);

        const productRef = (
            <Text
                ellipsizeMode={'tail'}
                numberOfLines={2}
                style={this.styles.product_name}
            >
                {`Ref. ${product.reference}`}
            </Text>);

        return (
            <View style={this.styles.container}>
                {productImage}
                <View style={{ flex: 1 }}>
                    {productName}
                    {productRef}
                    {this.renderPriceGroup(product)}
                    {this.renderButtonsGroup(product)}
                </View>
            </View>
        );
    }
}

