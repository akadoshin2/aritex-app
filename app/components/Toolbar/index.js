'use strict';

import React, { Component, PropTypes } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Title } from 'carbon-ui';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import TimerMixin from 'react-timer-mixin';
import DismissKeyboard from 'dismissKeyboard';

import Constants from './../../Constants';
import { openModal, closeModal } from '../../reducers/Modal/actions';

class Toolbar extends Component {
    static propTypes = {
        title: PropTypes.string,
        back: PropTypes.bool,
        cart: PropTypes.bool,
        check: PropTypes.bool,
        search: PropTypes.bool,
        elevation: PropTypes.bool,
        isModal: PropTypes.bool,
        rightCallback: PropTypes.func
    };

    static defaultProps = {
        title: 'Toolbar',
        back: false,
        cart: false,
        check: false,
        search: false,
        elevation: true,
        isModal: false,
        rightCallback: undefined
    };

    constructor(props) {
        super(props);

        this.styles = {
            toolbar: {
                height: 56,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: '#0D47A1',
                elevation: this.props.elevation ? 5 : 0
            },
            toolbarLeftGroup: {
                flexDirection: 'row',
                justifyContent: 'flex-start',
                flex: 1,
                paddingLeft: 10
            },
            toolbarCenterGroup: {
                flexDirection: 'row',
                justifyContent: 'center',
                flex: 4
            },            
            toolbarRightGroup: {
                flexDirection: 'row',
                justifyContent: 'flex-end',
                flex: 1,
                paddingRight: 10
            },
            itemWrapper: {
                padding: 10                
            }
        };
    }
    
    renderButton(icon, callback) {
        return (
            <TouchableOpacity 
                onPress={() => callback()} 
                style={this.styles.itemWrapper}
            >
                <Icon 
                    name={icon} 
                    size={25}
                    color="white"
                />
            </TouchableOpacity>
        );
    }

    renderBack(isModal) {
        const goBack = () => Actions.pop();

        const closeModal = () => {
            DismissKeyboard(); // There is a serious bug occurs whenever the keyboard close AFTER modal
            TimerMixin.setTimeout(
                () => this.props.closeModal(), 
                400
            );
        };

        if (!isModal) return this.renderButton(Constants.Icon.Back, goBack);
        return this.renderButton(Constants.Icon.Back, closeModal);
    }

    renderSearch() {
        const openSearchModal = () => {
            this.props.openModal('search-panel');
        };

        return this.renderButton(Constants.Icon.Search, openSearchModal);
    }

    renderCheck(callback) {
        return this.renderButton(Constants.Icon.Checkmark, callback);
    }

    render() {
        return (
            <View style={this.styles.toolbar}>
                <View style={this.styles.toolbarLeftGroup}>
                    {this.props.back ? this.renderBack(this.props.isModal) : undefined}
                </View>
                <View style={this.styles.toolbarCenterGroup}>                    
                    <Title style={{ color: 'white' }}>
                        {this.props.title}
                    </Title>
                </View>
                <View style={this.styles.toolbarRightGroup}>
                    {this.props.search ? this.renderSearch() : undefined}
                    {this.props.cart ? this.renderMyOrders(this.props.Cart.total) : undefined}
                    {this.props.check ? this.renderCheck(this.props.rightCallback) : undefined}
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        routes: state.routes
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        openModal: (modal) => {
            dispatch(openModal(modal));
        },
        closeModal: () => {
            dispatch(closeModal());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);
