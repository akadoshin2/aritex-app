'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import TimerMixin from 'react-timer-mixin';
import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import Languages from './../../Languages';

class BottomBar extends Component {
	render() {
		const onTabChange = (newTabIndex) => {
			switch (newTabIndex) {
				case 0:
					TimerMixin.setTimeout(() => Actions.home({ type: 'reset' }), 180);
					break;
				case 1:
					TimerMixin.setTimeout(() => Actions.catalog({ type: 'reset' }), 180);
					break;
				case 2:
					TimerMixin.setTimeout(() => Actions.myorders({ type: 'reset' }), 180);
					break;
				case 3:
					TimerMixin.setTimeout(() => Actions.settings({ type: 'reset' }), 180);
					break;
				default:
					break;
			}
		};

		const activeScene = () => {	
			switch (this.props.sceneKey) {
				case 'home':
					return 0;					
				case 'catalog':
					return 1;					
				case 'myorders':
					return 2;					
				case 'settings':
					return 3;					
				default:
					return 0;					
			}
		};
		
		return (			
			<BottomNavigation
				labelColor="white"
				rippleColor="white"
				style={{ 
					height: 56, 
					elevation: 8, 
					position: 'relative', 
					left: 0, 
					bottom: 0, 
					right: 0 
				}}
				onTabChange={(newTabIndex) => onTabChange(newTabIndex)}
				shifting={false}
				activeTab={activeScene()}
			>
				<Tab
					barBackgroundColor="black"
					label="Home"
					labelColor="#9B9B9B"
					activeLabelColor="white"
					icon={<Icon size={24} color="#9B9B9B" name="ios-home-outline" />}
					activeIcon={<Icon size={24} color="white" name="ios-home-outline" />}
				/>
				<Tab
					barBackgroundColor="black"
					label={Languages.Catalog}
					labelColor="#9B9B9B"
					activeLabelColor="white"
					icon={<Icon size={24} color="#9B9B9B" name="ios-book-outline" />}
					activeIcon={<Icon size={24} color="white" name="ios-book-outline" />}
				/>
				<Tab
					barBackgroundColor="black"
					label={Languages.MyOrders}
					labelColor="#9B9B9B"
					activeLabelColor="white"
					icon={<Icon size={24} color="#9B9B9B" name="ios-compass-outline" />}
					activeIcon={<Icon size={24} color="white" name="ios-compass-outline" />}
				/>
				<Tab
					barBackgroundColor="black"
					label={Languages.MyAccount}
					labelColor="#9B9B9B"
					activeLabelColor="white"
					icon={<Icon size={24} color="#9B9B9B" name="ios-heart-outline" />}
					activeIcon={<Icon size={24} color="white" name="ios-heart-outline" />}
				/>
			</BottomNavigation>
		);
    }
}

export default BottomBar;
