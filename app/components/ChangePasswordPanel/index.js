'use strict';

import React, { Component } from 'react';
import { View } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';
// import { Actions } from 'react-native-router-flux';

import Toolbar from './../../components/Toolbar';
import { changePassword } from '../../reducers/User/actions';
import { closeModal } from '../../reducers/Modal/actions';

class ChangePasswordPanel extends Component {
    constructor(props) {
        super(props);

        this.state = {            
            currentPassword: '',
            newPassword1: '',
            newPassword2: ''
        };              
    }

    componentDidMount() {        
        TimerMixin.setTimeout(
            () => this.refs.currentPasswordInput !== undefined && this.refs.currentPasswordInput.focus(), 
            400
        );        
    }

    renderContent() {
        const { 
            currentPassword, 
            newPassword1, 
            newPassword2
        } = this.state;

        const validateNewPasswordInputs = () => {
            if (newPassword1.length > 0 &&
                newPassword2.length > 0 &&
                newPassword1 !== newPassword2) return 'Contraseñas no coinciden';
            return '';
        };

        return (
            <View
                style={{
                    flex: 1
                }}
            >
                <View
                    style={{
                        height: 90,
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 10,
                        marginTop: 20,
                        borderBottomWidth: 0.5,
                        borderColor: 'black',
                    }}
                >
                    <Icon 
                        name="md-lock" 
                        size={30}
                        color="black"
                        style={{ marginRight: 10 }}
                    />
                    <View style={{ flex: 1 }}>
                        <TextField
                            ref="currentPasswordInput"
                            fontSize={15}
                            label="Contraseña actual"
                            secureTextEntry
                            value={currentPassword}
                            animationDuration={150} 
                            onChangeText={(value) => {
                                this.setState({ currentPassword: value });
                            }}            
                        />
                    </View>
                </View>


                <View
                    style={{
                        height: 90,
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 10,
                        marginTop: 30,
                        borderBottomWidth: 0.5,
                        borderColor: 'black',
                    }}
                >
                    <Icon 
                        name="md-lock" 
                        size={30}
                        color="black"
                        style={{ marginRight: 10 }}
                    />
                    <View style={{ flex: 1 }}>
                        <TextField
                            fontSize={15}
                            label="Contraseña nueva"
                            secureTextEntry
                            value={newPassword1}
                            animationDuration={150} 
                            onChangeText={(value) => {
                                this.setState({ newPassword1: value });
                            }}
                            error={validateNewPasswordInputs()}          
                        /> 
                    </View>
                </View>


                <View
                    style={{
                        height: 90,
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: 10,
                        borderBottomWidth: 0.5,
                        borderColor: 'black',
                    }}
                >
                    <Icon 
                        name="md-lock" 
                        size={30}
                        color="black"
                        style={{ marginRight: 10 }}
                    />
                    <View style={{ flex: 1 }}>
                        <TextField
                            fontSize={15}
                            label="Repetir contraseña"
                            secureTextEntry
                            value={newPassword2}
                            animationDuration={150} 
                            onChangeText={(value) => {
                                this.setState({ newPassword2: value });
                            }}
                            error={validateNewPasswordInputs()}            
                        /> 
                    </View>
                </View>
            </View>
        );
    }

    render() {
        const { changePassword, closeModal } = this.props;
        const { currentPassword, newPassword1, newPassword2 } = this.state;

        const showCheck = () => {
            if (currentPassword.length > 0 &&
                newPassword1.length > 0 &&
                newPassword2.length > 0 &&
                newPassword1 === newPassword2) return true;
            return false;
        };

        return (
            <View style={{ flex: 1 }}>
                <Toolbar 
                    title="Cambiar Contraseña" 
                    back 
                    isModal
                    check={showCheck()}
                    rightCallback={() => {
                        changePassword(currentPassword, newPassword1);
                        closeModal();
                    }}
                />
                {this.renderContent()} 
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changePassword: (password, newPassword) => {
            dispatch(changePassword(password, newPassword));
        },
        closeModal: () => {
            dispatch(closeModal());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePasswordPanel);
