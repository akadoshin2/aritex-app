'use strict';

import React, { Component } from 'react';
import { View, Picker, Alert, ScrollView } from 'react-native';
import { Body2, IconToggle, FloatingActionButton } from 'carbon-ui';
import { TextField } from 'react-native-material-textfield';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';
import TimerMixin from 'react-timer-mixin';
import DismissKeyboard from 'dismissKeyboard';

import Toolbar from './../../components/Toolbar';
// import Constants from './../../Constants';

import { addCartItem } from '../../reducers/Cart/actions';
import { closeModal } from '../../reducers/Modal/actions';

/*
    product -> reducer
    productCart
    isModal
*/

class VariationsPanel extends Component {
    constructor(props) {
        super(props);

        this.index = this.props.cartItems.findIndex((cartItem) => {
            return (this.props.isModal ? this.props.product.reference_id : this.props.productCart.reference_id) === cartItem.product.reference_id;
        });        

        this.state = {
            product: this.props.isModal ? this.props.product : this.props.productCart,
            selectedVariations: this.index === -1 ?
                [
                    {
                        color: 'Color',
                        size: 'Talla',
                        quantity: 1,
                        input: '',
                        inputWarning: false
                    }
                ] :
                this.props.cartItems[this.index].variations,
            time: moment()
        };                
    }

    componentDidUpdate() {
        TimerMixin.setTimeout(
            () => this.refs.scrollView.scrollToEnd({ animated: true }),
            400
        );
    }

    filterVariations(array) {
        return array.filter((variation) => {
            return variation.color !== 'Color' && variation.size !== 'Talla';
        }).map((variation) => {
            return Object.assign({}, variation, { 
                input: variation.quantity.toString(), 
                inputWarning: false 
            });
        }); 
    }

    addToCart(array) {
        const { product, time } = this.state;

        const productSelectedVariations = this.filterVariations(array);

        const amount = productSelectedVariations.reduce((total, variation) => {
            return total + variation.quantity;
        }, 0);

        const now = moment();

        this.props.addCartItem(
            product, 
            productSelectedVariations, 
            amount,
            now.diff(time, 'seconds')
        );
        if (this.props.isModal) {
            Actions.myorders({ type: 'reset' });
            DismissKeyboard();
            TimerMixin.setTimeout(
                () => this.props.closeModal(), 
                400
            );
        }
    }

    renderListItem(index) {
        const { color, size } = this.state.product.variations;
        const { selectedVariations } = this.state;

        const renderColorPickerItem = () => {
            let array = [];
            array = array.concat(
                <Picker.Item key="unselected" label="Color" value="Color" />
            );
            return array.concat(color.map((item) => {
                return (
                    <Picker.Item 
                        key={item}
                        label={item} 
                        value={item} 
                    />
                );
            }));            
        };

        const renderSizePickerItem = () => {
            let array = [];
            array = array.concat(
                <Picker.Item key="unselected" label="Talla" value="Talla" />
            );
            return array.concat(size.map((item) => {
                return (
                    <Picker.Item 
                        key={item}
                        label={item} 
                        value={item}
                    />
                );
            }));            
        };        

        const onColorValueChange = (itemValue) => {
            let array = selectedVariations.slice();
            let filter = [];
            if (array[index].size === 'Talla') {
                array[index].color = itemValue;
                this.setState({ selectedVariations: array });
            } else {
                filter = array.filter((variation, ind) => {
                    return variation.color === itemValue && 
                    variation.size === array[index].size && ind !== index;
                });
                if (filter.length > 0) {
                    Alert.alert(
                        'Alerta',
                        'Combinación de variaciones existente, por favor elija otra',                
                        [
                            {
                                text: 'OK', 
                                onPress: () => {}
                            },
                        ],
                        { cancelable: true }
                    );
                } else {
                    array[index].color = itemValue;
                    this.setState({ selectedVariations: array });
                    if (!this.props.isModal) this.addToCart(array);
                }
            }            
        };

        const onSizeValueChange = (itemValue) => {
            let array = selectedVariations.slice();
            let filter = [];
            if (array[index].color === 'Color') {
                array[index].size = itemValue;
                this.setState({ selectedVariations: array });
            } else {
                filter = array.filter((variation, ind) => {
                    return variation.size === itemValue && 
                    variation.color === array[index].color && 
                    ind !== index;
                });
                if (filter.length > 0) {
                    Alert.alert(
                        'Alerta',
                        'Combinación de variaciones existente, por favor elija otra',                
                        [
                            {
                                text: 'OK', 
                                onPress: () => {}
                            },
                        ],
                        { cancelable: true }
                    );
                } else {
                    array[index].size = itemValue;
                    this.setState({ selectedVariations: array });
                    if (!this.props.isModal) this.addToCart(array);
                }
            } 
        };

        const onQuantityValueChange = (quantity) => {
            const regex = /^[0-9]+$/;
            let array = selectedVariations.slice();

            const setWarning = () => {
                array[index].input = quantity;
                array[index].inputWarning = true;
                this.setState({ selectedVariations: array });
            };

            if (quantity.length === 0) {                
                setWarning();
            } else if (!regex.test(quantity)) {
                setWarning();
            } else if (parseInt(quantity, 10) === 0) {
                setWarning();
            } else {
                array[index].quantity = parseInt(quantity, 10);
                array[index].input = quantity;
                array[index].inputWarning = false;
                this.setState({ selectedVariations: array });
                if (!this.props.isModal) this.addToCart(array);
            }
        };
        
        return (
            <View
                key={index}
                style={{
                    flexDirection: 'row',
                    alignItems: 'center'
                }}
            >
                <View 
                    style={{ 
                        flex: 4,
                        paddingTop: 15
                    }}
                >
                    <Picker
                        selectedValue={selectedVariations[index].color}
                        onValueChange={(itemValue) => onColorValueChange(itemValue)}
                    >
                        {renderColorPickerItem()}
                    </Picker>
                </View>
                <View 
                    style={{ 
                        flex: 3,
                        paddingTop: 15
                    }}
                >
                    <Picker
                        selectedValue={selectedVariations[index].size}
                        onValueChange={(itemValue) => onSizeValueChange(itemValue)}
                    >
                        {renderSizePickerItem()}
                    </Picker>
                </View>
                <View style={{ flex: 2 }}>
                    <TextField
                        fontSize={14}
                        label="Min. 1"
                        value={selectedVariations[index].input}
                        error={selectedVariations[index].inputWarning ? 'Error' : ''}
                        animationDuration={150} 
                        keyboardType="numeric"
                        onChangeText={(quantity) => onQuantityValueChange(quantity)}
                    />
                </View>
                <View 
                    style={{ 
                        flex: 1,
                        alignItems: 'center',
                        paddingTop: 12,
                    }}
                >
                    {
                        selectedVariations.length > 1 ?
                            <IconToggle 
                                name="close"
                                onPress={() => {
                                    let array = selectedVariations.slice();
                                    array.splice(index, 1);
                                    this.setState({ selectedVariations: array });
                                }}
                                iconStyle={{
                                    color: 'black',
                                    fontSize: 20
                                }}
                            /> :
                            undefined
                    }                    
                </View>
            </View>
        );
    }

    renderList() {
        const { selectedVariations } = this.state;

        return (
            <ScrollView
                ref="scrollView"
            >                    
                {selectedVariations.map((item, index) => this.renderListItem(index))}  
            </ScrollView>
        );
    }

    renderFooter() {
        const { selectedVariations } = this.state;

        const onPress = () => {
            let array = selectedVariations.slice();
            array.push({
                color: 'Color',
                size: 'Talla',
                quantity: 1, 
                input: '',
                inputWarning: false
            });
            this.setState({ selectedVariations: array });
        };

        const totalizeVariations = () => {
            let total = 0;
            selectedVariations.forEach((item) => {
                if (item.color !== 'Color' && item.size !== 'Talla') {
                    total += item.quantity;
                }
            });
            return `TOTAL ${total}`;
        };

        if (!this.props.isModal) {
            return (
                <View 
                    style={{ 
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingLeft: 10,
                        marginBottom: 10
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row'
                        }}
                    >
                        <Body2>
                            {totalizeVariations()}
                        </Body2>
                    </View>
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'flex-end'
                        }}
                    >  
                        <IconToggle 
                            name="add"
                            onPress={() => onPress()}
                            iconStyle={{
                                color: 'black',
                                fontSize: 20
                            }}
                        />
                    </View>
                </View>
            );
        }

        return (
            <FloatingActionButton
                onPressIn={() => onPress()}
                style={{ 
                    backgroundColor: '#1976d2',
                    position: 'absolute',   
                    right: 24,                     
                    bottom: 24
                }}
            >
                <Icon 
                    name="md-add" 
                    size={25}
                    color="white" 
                />
            </FloatingActionButton>
        );
    }

    render() {
        const { selectedVariations } = this.state;

        const submitToCart = () => {
            Alert.alert(
                'Confirmar',
                'Presione OK para añadir al carrito',                
                [            
                    {
                        text: 'CANCEL', 
                        onPress: () => {}, 
                        style: 'cancel'
                    },
                    {
                        text: 'OK', 
                        onPress: () => this.addToCart(this.state.selectedVariations)
                    },
                ],
                { cancelable: true }
            );             
        };

        return (
            <View style={{ flex: 1 }}>
                {
                    this.props.isModal ?
                        <Toolbar 
                            title="Variaciones"  
                            back 
                            isModal     
                            check={this.filterVariations(selectedVariations).length > 0 ? 
                                true : false
                            }
                            rightCallback={() => submitToCart()}
                        /> :
                        undefined
                }                
                {this.renderList()}
                {this.renderFooter()}
            </View>
        );
    }
}

const mapStateToProps = (store) => {
    return { 
        product: store.Store.selectedProduct,
        cartItems: store.Cart.cartItems
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addCartItem: (product, variations, amount, time) => {
            dispatch(addCartItem(product, variations, amount, time));
        },
        closeModal: () => {
            dispatch(closeModal());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(VariationsPanel);
