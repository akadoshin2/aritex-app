'use strict';

import React, { Component } from 'react';
import {
    Text,
    View,
    ListView,
    TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

import Languages from './../../Languages';
import ProductCard from './ProductCard';
import { selectCategory, selectProduct } from '../../reducers/Store/actions';

class CarouselByCategory extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    renderProducts() {
        const { products } = this.props;
        const dataSource = new ListView.DataSource({ rowHasChanged: () => true });

        const goToProduct = (product) => {
            this.props.selectCategory(product.categories[0]);
            this.props.selectProduct(product);
            Actions.product({ product, addProduct: false }); 
        };

        const renderRow = (product) => (
            <ProductCard
                onPress={goToProduct}
                product={product}
            />
        );

        return (
            <ListView
                style={{
                    marginBottom: 8
                }}
                dataSource={dataSource.cloneWithRows(products)}
                renderRow={renderRow}
                showsHorizontalScrollIndicator={false}
                enableEmptySections
                horizontal
            />
        );
    }

    renderCarousel() {
        const { categoryId, categoryName } = this.props;

        const goToCategory = () => {
            this.props.selectCategory(categoryId);
            Actions.category({
                initCategoryId: categoryId,
                title: categoryName
            });
        };

        return (
            <View>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}
                >
                    <Text
                        style={{
                            fontSize: 12,
                            color: 'gray',
                            margin: 8
                        }}
                    >
                        {categoryName.toUpperCase()}
                    </Text>
                    <TouchableOpacity
                        onPress={() => goToCategory()}
                    >
                        <Text
                            style={{
                                fontSize: 12,
                                color: 'gray',
                                margin: 8
                            }}
                        >
                            {Languages.SeeMore}
                        </Text>
                    </TouchableOpacity>
                </View>
                {this.renderProducts()}
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>              
                {this.renderCarousel()}                
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        selectCategory: (selectedCategoryId) => {
            dispatch(selectCategory(selectedCategoryId));
        },
        selectProduct: (selectedProduct) => {
            dispatch(selectProduct(selectedProduct));
        }
    };
};

export default connect(null, mapDispatchToProps)(CarouselByCategory);
