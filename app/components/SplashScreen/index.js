'use strict';

import React, { Component } from 'react';
import { 
    View, 
    Image,
    StatusBar
} from 'react-native';

import Constants from './../../Constants';

class SplashScreen extends Component {
    render() {
        return ( 
            <View style={{ flex: 1 }}>
                <StatusBar hidden />
                <Image
                    style={{
                        height: Constants.Dimension.ScreenHeight(1),
                        width: Constants.Dimension.ScreenWidth(1)
                    }}
                    source={Constants.SplashScreen.Image}
                />
            </View>
        );
    }
}

export default SplashScreen;
