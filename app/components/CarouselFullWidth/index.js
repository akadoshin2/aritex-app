'use strict';

import React, { Component } from 'react';
import { View, Image, TouchableWithoutFeedback } from 'react-native';
import Swiper from 'react-native-swiper';

import Constants from './../../Constants';

/*
    data -> array
    isLocal -> bool
    onPressCard -> function
*/
class CarouselFullWidth extends Component {
    static defaultProps = {
        data: [],
        isLocal: false,
        onPressCard: () => {}
    };

    renderImages() {
        const { data, isLocal, onPressCard } = this.props;
        let _data = [];
        for(let key in data){
            _data.push(data[key]);
        }
        return (
            _data.map((item, index) =>
                <TouchableWithoutFeedback
                    key={index}
                    onPress={() => onPressCard(item)}
                >
                    <Image                        
                        source={
                            isLocal ?
                            item.url :
                            { uri: item.url }
                        }
                        style={{
                            height: Constants.Dimension.ScreenWidth(0.75),
                            width: Constants.Dimension.ScreenWidth(1)
                        }}
                    />
                </TouchableWithoutFeedback>,
                this
            )   
        );
    }

    render() {
        return (                 
            <Swiper
                height={Constants.Dimension.ScreenWidth(0.75)}
                dot={
                    <View 
                        style={{
                            backgroundColor: 'rgba(0,0,0,.2)',
                            width: 8,
                            height: 8,
                            borderRadius: 4,
                            marginLeft: 4,
                            marginRight: 4
                        }} 
                    />
                }
                activeDot={
                    <View 
                        style={{
                            backgroundColor: '#000',
                            width: 8,
                            height: 8,
                            borderRadius: 4,
                            marginLeft: 4,
                            marginRight: 4
                        }} 
                    />
                }
                paginationStyle={{ bottom: 20 }}
                loop={false} 
            >        
                {this.renderImages()}
            </Swiper>
           
        );
    }
}

export default CarouselFullWidth;
