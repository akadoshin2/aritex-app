
import { AsyncStorage } from 'react-native';
import { compose, createStore, applyMiddleware } from 'redux';
import { setupRNListener } from 'react-native-redux-listener';
import { persistStore, autoRehydrate } from 'redux-persist';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';
import reducers from './reducers';

// const middleWare = [thunk, logger]; // Development
const middleWare = [thunk]; // Production

const configureStore = (onComplete) => {
    const store = compose(
        setupRNListener({
        monitorAppState: false,
        monitorNetInfo: true,
        monitorKeyboard: false,
        monitorDeepLinks: false,
        monitorBackButton: false,
        }),
        applyMiddleware(...middleWare),
        autoRehydrate()
    )(createStore)(reducers);
        
    persistStore(
        store, 
        { 
            storage: AsyncStorage, 
            blacklist: [
                'routes', 
                'netinfo',
                'Modal',
                'form'
            ] 
        },
        onComplete
    );

    return store;
};

export default configureStore;
