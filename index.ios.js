/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

//  'use strict';
// import React, { Component } from 'react';
// import { AppRegistry, View } from 'react-native';

// import RootRouter from './app/RootRouter';

// class aritex extends Component {
//     render() {
//         return (
//             <View style={{ flex: 1, marginTop: 15 }}>
//                 <RootRouter />
//             </View>
//         );
//     }
// }

// AppRegistry.registerComponent('aritex', () => aritex);

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class aritex extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit index.ios.js
        </Text>
        <Text style={styles.instructions}>
          Press Cmd+R to reload,{'\n'}
          Cmd+D or shake for dev menu
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('aritex', () => aritex);

