/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

'use strict';
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import RootRouter from './app/RootRouter';

class aritex extends Component { 
    render() {
        return <RootRouter />;
    }
}

AppRegistry.registerComponent('aritex', () => aritex);

// import React, { Component } from 'react';
// import { AppRegistry } from 'react-native';
// import RootRouter from './app/RootRouter';

// export default class aritex extends Component {
//   render() {
//     return (
// 		<RootRouter />
//     );
//   }
// }

// AppRegistry.registerComponent('aritex', () => aritex);