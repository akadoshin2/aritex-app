package com.aritex;

// <react-native-localization
// import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
// // react-native-localization>

// // <react-native-restart
// import com.avishayil.rnrestart.ReactNativeRestartPackage; 
// // react-native-restart>

// // <react-native-vector-icons
// import com.oblador.vectoricons.VectorIconsPackage;
// // react-native-vector-icons>

// // <react-native-image-crop-picker
// import com.reactnative.ivpusic.imagepicker.PickerPackage;
// // react-native-image-crop-picker>

// // <react-native-fs
// import com.rnfs.RNFSPackage;
// // react-native-fs>

// // <react-native-fcm
// import com.evollu.react.fcm.FIRMessagingPackage;
// // react-native-fcm>

import android.app.Application;

import com.facebook.react.ReactApplication;
// import com.oblador.vectoricons.VectorIconsPackage;
// import com.avishayil.rnrestart.ReactNativeRestartPackage;
// import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
// import com.reactnative.ivpusic.imagepicker.PickerPackage;
// import com.rnfs.RNFSPackage;
// import com.evollu.react.fcm.FIRMessagingPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.avishayil.rnrestart.ReactNativeRestartPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import com.rnfs.RNFSPackage;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            // new VectorIconsPackage(),
            // new ReactNativeRestartPackage(),
            // new ReactNativeLocalizationPackage(),
            // new PickerPackage(),
            // new RNFSPackage(),
            // new FIRMessagingPackage(),
            // new VectorIconsPackage(),
            // new ReactNativeRestartPackage(),
            // new ReactNativeLocalizationPackage(),
            // new PickerPackage(),
            // new RNFSPackage(),
            // new FIRMessagingPackage(),

          // <react-native-localization
          new ReactNativeLocalizationPackage(),
          // react-native-localization>

          // <react-native-restart
          new ReactNativeRestartPackage(),
          // react-native-restart>

          // <react-native-vector-icons
          new VectorIconsPackage(),
          // react-native-vector-icons>

          // <react-native-image-crop-picker
          new PickerPackage(),
          // react-native-image-crop-picker>

          // <react-native-fs
          new RNFSPackage(),
          // react-native-fs>

          // <react-native-fcm
          new FIRMessagingPackage()
          // react-native-fcm>
      );
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
